<!DOCTYPE html>
<html>
    <body>
        <h3>Witaj ${user}!</h3>
        <p>Twoja rejestracja przebiegła pomyślnie. Jeśli chcesz aktywować swoje konto kliknij <a href="http://172.20.44.22:8280/subjects/#/registration/${unblockToken}">tutaj</a>.</p>
        <p>Jeśli nie rejestrowałeś się na żadnej stronie po prostu zignoruj ten email.</p>
    </body>
</html>
