<!DOCTYPE html>
<html>
    <body>
        <h3>Witaj ${user}!</h3>
        <#if chosenCourse?size != 0 >
            <p>Właśnie udało ci się zapisać na poniższe przedmioty:</p>
            <ol>
                <#list chosenCourse as course>
                    <li>
                        <b>${course}</b>
                    </li>
                </#list>
            </ol>
        </#if>
        <#if rejectedCourse?size != 0>
            <p>Wypisałeś się z następujących przedmiotów:</p>
            <ol>
                <#list rejectedCourse as course>
                    <li>
                        <b>${course}</b>
                    </li>
                </#list>
            </ol>
        </#if>
    </body>
</html>
