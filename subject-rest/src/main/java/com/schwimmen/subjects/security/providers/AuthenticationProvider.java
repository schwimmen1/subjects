package com.schwimmen.subjects.security.providers;

import com.schwimmen.subjects.LdapUserAttribute;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.service.interfaces.LdapService;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuthenticationProvider {

    private final Log logger = LogFactory.getLog(this.getClass());
    private final UserService userService;
    private final LdapService ldapService;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationProvider(AuthenticationManager authenticationManager, LdapService ldapService
            , UserService userService) {
        this.authenticationManager = authenticationManager;
        this.ldapService = ldapService;
        this.userService = userService;
    }

    public Authentication authenticate(Authentication authentication, Boolean isLdapAccount) throws AuthenticationException {
        if(!isLdapAccount) {
            return authenticationManager.authenticate(authentication);
        } else if(ldapService.authenticateLdapUser((String) authentication.getPrincipal(), (String) authentication.getCredentials())) {
            saveLdapUser(ldapService.getLdapUserAttribute((String) authentication.getPrincipal()));
            return authentication;
        }
        throw new BadCredentialsException("LDAP account: wrong username or password.");
    }

    private void saveLdapUser(Map<String, String> ldapUserAttributes) throws AuthenticationException {
        if(userService.checkIfUserIsSaved(ldapUserAttributes.get(LdapUserAttribute.LDAP_LOGIN.value()), Boolean.TRUE)){
            logger.info("Ldap user: " + ldapUserAttributes.get(LdapUserAttribute.LDAP_LOGIN.value()) + " exists in database.");
            return;
        }
        try {
            User user = new User();
            user.setUsername(ldapUserAttributes.get(LdapUserAttribute.LDAP_LOGIN.value()));
            user.setFirstName(ldapUserAttributes.get(LdapUserAttribute.NAME.value()));
            user.setLastName(ldapUserAttributes.get(LdapUserAttribute.SURNAME.value()));
            user.setEmail(ldapUserAttributes.get(LdapUserAttribute.EMAIL.value()));
            user.setLdap(Boolean.TRUE);
            userService.createUser(user, AuthorityName.getAuthorityByCode(ldapUserAttributes.get(LdapUserAttribute.ROLE.value())));
        } catch (ApplicationException e) {
            throw new BadCredentialsException("Authority: " + ldapUserAttributes.get(LdapUserAttribute.ROLE.value()) + " does not exist.", e);
        }
    }

}
