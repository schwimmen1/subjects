package com.schwimmen.subjects.security;

import java.io.Serializable;
import java.util.Date;

public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private final String firstName;
    private final String lastName;
    private final String role;
    private final Date startEnrolmentDay;
    private final Date endEnrolmentDay;

    public JwtAuthenticationResponse(String token, String firstName, String lastName, String role
            , Date startEnrolmentDay, Date endEnrolmentDay) {
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.startEnrolmentDay = startEnrolmentDay;
        this.endEnrolmentDay = endEnrolmentDay;
    }

    public String getToken() {
        return this.token;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRole() {
        return role;
    }

    public Date getStartEnrolmentDay() {
        return startEnrolmentDay;
    }

    public Date getEndEnrolmentDay() {
        return endEnrolmentDay;
    }

}