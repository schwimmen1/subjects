package com.schwimmen.subjects.security.service;

import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.security.JwtUser;
import com.schwimmen.subjects.security.JwtUserFactory;
import com.schwimmen.subjects.security.service.interfaces.UserDetailsService;
import com.schwimmen.subjects.service.interfaces.LdapService;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final Log logger = LogFactory.getLog(this.getClass());
    private final UserService userService;
    private final LdapService ldapService;

    @Autowired
    public JwtUserDetailsServiceImpl(UserService userService, LdapService ldapService) {
        this.userService = userService;
        this.ldapService = ldapService;
    }

    @Override
    public UserDetails loadUserByUsername(String username, boolean isLdapAccount) throws UsernameNotFoundException {
        return isLdapAccount ? loadUserByLdapLogin(username) : loadUserByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return Optional.ofNullable(userService.findUserByUsername(username)).map(JwtUserFactory::create)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("No user found with username '%s'.", username)));
    }

    private UserDetails loadUserByLdapLogin(String username) throws UsernameNotFoundException{
        Optional<JwtUser> userDetails = Optional.empty();
        try {
            userDetails = Optional.of(JwtUserFactory.create(ldapService.getLdapUserAttribute(username)));
        } catch(ApplicationException e) {
            logger.error("Ldap user has incorrect authority.");
        }
        return userDetails.orElseThrow(() -> new UsernameNotFoundException(String.format("No user found with username '%s'.", username)));
    }

}
