package com.schwimmen.subjects.security.controller;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.security.JwtAuthenticationRequest;
import com.schwimmen.subjects.security.JwtAuthenticationResponse;
import com.schwimmen.subjects.security.JwtTokenUtil;
import com.schwimmen.subjects.security.JwtUser;
import com.schwimmen.subjects.security.providers.AuthenticationProvider;
import com.schwimmen.subjects.security.service.interfaces.UserDetailsService;
import com.schwimmen.subjects.service.interfaces.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthenticationRestController {

    @Value("${jwt.header}")
    private String tokenHeader;
    private final AuthenticationProvider authenticationProvider;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserDetailsService userDetailsService;
    private final ParameterService parameterService;

    @Autowired
    public AuthenticationRestController(AuthenticationProvider authenticationManager, JwtTokenUtil jwtTokenUtil
            , UserDetailsService userDetailsService, ParameterService parameterService) {
        this.authenticationProvider = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.parameterService = parameterService;
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest
            , Device device) throws AuthenticationException {
        final Authentication authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getUsername(), authenticationRequest.getPassword()), authenticationRequest.getIsLdap());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        if(parameterService.getParameterValue(Parameters.ONLY_LDAP_LOGIN).equals("1") && !authenticationRequest.getIsLdap()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Only LDAP user can log in to application.");
        }

        final JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(authenticationRequest.getUsername(), authenticationRequest.getIsLdap());
        final String token = jwtTokenUtil.generateToken(user, device);
        return ResponseEntity.ok(new JwtAuthenticationResponse(token, user.getFirstName(), user.getLastName()
                , user.getAuthorities().stream().findFirst().map(GrantedAuthority::getAuthority).orElse("")
                , parameterService.getDate(Parameters.BEGIN_ENROLMENT_DAY), parameterService.getDate(Parameters.END_ENROLMENT_DAY)));
    }

    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        Boolean isLdapAccount = jwtTokenUtil.isLdapFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username, isLdapAccount);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken, user.getFirstName(), user.getLastName()
                    , user.getAuthorities().stream().findFirst().map(GrantedAuthority::getAuthority).orElse("")
                    , parameterService.getDate(Parameters.BEGIN_ENROLMENT_DAY), parameterService.getDate(Parameters.END_ENROLMENT_DAY)));
        }
        return ResponseEntity.badRequest().body(null);
    }

}