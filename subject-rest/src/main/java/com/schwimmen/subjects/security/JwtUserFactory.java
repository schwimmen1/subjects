package com.schwimmen.subjects.security;

import com.schwimmen.subjects.LdapUserAttribute;
import com.schwimmen.subjects.entity.security.ApplicationUser;
import com.schwimmen.subjects.entity.security.Authority;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.exception.ApplicationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class JwtUserFactory {

    private JwtUserFactory() {
        super();
    }

    public static JwtUser create(ApplicationUser user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getAuthorities()),
                user.getEnabled(),
                user.getLastPasswordResetDate(),
                user.getLdap()
        );
    }

    /** Only for LDAP users */
    public static JwtUser create(Map<String, String> attributes) throws ApplicationException {
        return new JwtUser(
                attributes.get(LdapUserAttribute.LDAP_LOGIN.value()),
                attributes.get(LdapUserAttribute.NAME.value()),
                attributes.get(LdapUserAttribute.SURNAME.value()),
                attributes.get(LdapUserAttribute.EMAIL.value()),
                Boolean.TRUE,
                mapToGrantedAuthorities(AuthorityName.getAuthorityByCode(attributes.get(LdapUserAttribute.ROLE.value())))
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName().name()))
                .collect(Collectors.toList());
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(AuthorityName authority) {
        return Arrays.asList(new SimpleGrantedAuthority(authority.name()));
    }

}
