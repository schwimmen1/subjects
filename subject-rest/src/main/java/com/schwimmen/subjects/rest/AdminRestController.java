package com.schwimmen.subjects.rest;

import com.schwimmen.subjects.entity.Parameter;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.rest.mappers.ParameterMapper;
import com.schwimmen.subjects.rest.request.ParameterModifyRequest;
import com.schwimmen.subjects.service.interfaces.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class AdminRestController {

    private final ParameterService parameterService;

    @Autowired
    public AdminRestController(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    @RequestMapping(value = "/parameters", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getParameters() {
        return ResponseEntity.ok().body(parameterService.getAllParameters().stream()
                .map(this::mapParameter).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/parameters", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> modifyParameters(@RequestBody ParameterModifyRequest parameterRequest) {
        ResponseEntity<String> response = ResponseEntity.ok().build();
        try {
            parameterService.saveParameter(parameterRequest.getParameterName(), parameterRequest.getParameterValue());
        } catch(ApplicationException e) {
            response =  ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    private ParameterMapper mapParameter(Parameter parameter) {
        ParameterMapper mapper = new ParameterMapper();
        try {
            mapper.setValue(parameterService.castValueToObject(parameter));
        } catch(ApplicationException e) {
            mapper.setValue(null);
        }
        mapper.setId(parameter.getId());
        mapper.setName(parameter.getName());
        mapper.setDescription(parameter.getDescription());
        mapper.setType(parameter.getType());
        return mapper;
    }

}
