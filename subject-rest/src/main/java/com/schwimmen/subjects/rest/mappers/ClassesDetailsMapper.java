package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.ClassesDetails;

import java.util.Date;

public class ClassesDetailsMapper {

    private Long id;
    private String day;
    private Date hoursFrom;
    private Date hoursTo;
    private String building;
    private String roomNumber;

    public static ClassesDetailsMapper mapClassesDetail(ClassesDetails classesDetails) {
        ClassesDetailsMapper mapper = new ClassesDetailsMapper();
        mapper.setId(classesDetails.getId());
        mapper.setDay(classesDetails.getDay().getName().value());
        mapper.setHoursFrom(classesDetails.getHoursFrom());
        mapper.setHoursTo(classesDetails.getHoursTo());
        mapper.setBuilding(classesDetails.getBuilding());
        mapper.setRoomNumber(classesDetails.getRoomNumber());
        return mapper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Date getHoursFrom() {
        return hoursFrom;
    }

    public void setHoursFrom(Date hoursFrom) {
        this.hoursFrom = hoursFrom;
    }

    public Date getHoursTo() {
        return hoursTo;
    }

    public void setHoursTo(Date hoursTo) {
        this.hoursTo = hoursTo;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

}
