package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.TypeOfCourse;

public class TypeOfCoursesMapper {

    private Long id;
    private String name;
    private String description;

    public static TypeOfCoursesMapper mapTypeOfCourse(TypeOfCourse typeOfCourse) {
        TypeOfCoursesMapper mapper = new TypeOfCoursesMapper();
        mapper.setId(typeOfCourse.getId());
        mapper.setName(typeOfCourse.getName());
        mapper.setDescription(typeOfCourse.getDescription());
        return mapper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
