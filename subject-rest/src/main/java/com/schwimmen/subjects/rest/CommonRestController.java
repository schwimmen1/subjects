package com.schwimmen.subjects.rest;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.ClassesDetails;
import com.schwimmen.subjects.entity.Course;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.rest.mappers.ClassesWithStudentsMapper;
import com.schwimmen.subjects.rest.mappers.CourseMapper;
import com.schwimmen.subjects.rest.mappers.TypeOfCoursesMapper;
import com.schwimmen.subjects.rest.mappers.UserMapper;
import com.schwimmen.subjects.rest.response.ListContainerResponse;
import com.schwimmen.subjects.rest.response.SizeClassesResponse;
import com.schwimmen.subjects.service.interfaces.*;
import com.schwimmen.subjects.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.applet.AppletContext;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequestMapping("/common")
public class CommonRestController {

    private final UserRestService userRestService;
    private final CourseService courseService;
    private final ClassesService classesService;
    private final UserService userService;
    private final ParameterService parameterService;

    @Autowired
    public CommonRestController(CourseService courseService, ClassesService classesService, UserService userService
            , UserRestService userRestService, ParameterService parameterService) {
        this.courseService = courseService;
        this.classesService = classesService;
        this.userService = userService;
        this.userRestService = userRestService;
        this.parameterService = parameterService;
    }

    @RequestMapping(value = "/parameter/onlyLdapLogin", method = RequestMethod.GET)
    public ResponseEntity<?> isOnlyLdapLogin() {
        return ResponseEntity.ok(parameterService.getParameterValue(Parameters.ONLY_LDAP_LOGIN));
    }

    @RequestMapping(value = "/parameter/registrationAllow", method = RequestMethod.GET)
    public ResponseEntity<?> isRegistrationAllow() {
        return ResponseEntity.ok(parameterService.getParameterValue(Parameters.REGISTRATION_AVAILABLE));
    }

    @RequestMapping(value = "/loggedUser", method = RequestMethod.GET)
    public ResponseEntity<?> getULoggedUser(@RequestHeader(value="${jwt.header}") String token) {
        return ResponseEntity.ok(UserMapper.mapUser(userRestService.findUserFromToken(token)));
    }

    @RequestMapping(value = "/usersByRole/{roleName}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getUsersByRole(@PathVariable String roleName, @RequestParam Integer size
            , @RequestParam Integer from, @RequestParam String search, @RequestParam List<Long> exclude) {
        if(Objects.isNull(size) || Objects.isNull(from) || size < 0 || from <= 0) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Size and from parameter cannot be null or negative");
        }
        ResponseEntity response;
        try {
            List<User> userByRole = userService.findUserByRole(AuthorityName.getAuthorityByCode(roleName), exclude)
                    .stream().filter(user -> Pattern.compile(Pattern.quote(search), Pattern.CASE_INSENSITIVE)
                            .matcher(user.getFirstName() + " " + user.getLastName()).find())
                    .collect(Collectors.toList());
            response = ResponseEntity.ok(new ListContainerResponse(userByRole.size(), userByRole.stream()
                    .limit(size * from)
                    .skip((from - 1) * size)
                    .map(UserMapper::mapUser)
                    .collect(Collectors.toList())));
        } catch (ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/typeOfCourses", method = RequestMethod.GET)
    public ResponseEntity<?> getAllTypeOfCourses() {
        return ResponseEntity.ok(courseService.getAllTypeOfCourse().stream()
                .map(TypeOfCoursesMapper::mapTypeOfCourse)
                .collect(Collectors.toList()));
    }

    @RequestMapping(value = "/typeOfCourses/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<?> getAllTypeOfCourses(@PathVariable Long courseId) {
        return ResponseEntity.ok(courseService.getAllTypeOfCourse(courseId).stream()
                .map(TypeOfCoursesMapper::mapTypeOfCourse)
                .collect(Collectors.toList()));
    }

    @RequestMapping(value = "/selectedClasses/{courseId}/{typeOfClasses}", method = RequestMethod.GET)
    public ResponseEntity<?> getStudentClasses(@PathVariable Long courseId, @PathVariable Long typeOfClasses) {
        return ResponseEntity.ok(classesService.getClassesWithTypes(courseId, typeOfClasses).stream().sorted()
                .map(ClassesWithStudentsMapper::mapClasses).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/course/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<?> getCourse(@PathVariable Long courseId) {
        return ResponseEntity.ok(Optional.ofNullable(courseService.getCourse(courseId)).map(CourseMapper::mapCourse)
                .orElse(new CourseMapper()));
    }

    @RequestMapping(value = "/downloadListFile/{courseId}", produces = "text/csv; charset=UTF-8", method = RequestMethod.GET)
    public void downloadFile(@PathVariable Long courseId, HttpServletResponse response) throws ApplicationException
            , IOException, ParseException {
        Optional<Course> course = Optional.ofNullable(courseService.getCourse(courseId));
        if(!course.isPresent()) {
            throw new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST);
        }

        String csvFileName = "List.csv";
        response.setContentType("text/csv");
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
        response.setHeader(headerKey, headerValue);
        response.setCharacterEncoding(String.valueOf(Charset.forName("UTF-8")));

        for(Classes classes : course.get().getClasses()) {
            response.getWriter().print(course.get().getName() + ";" + classes.getTypeOfCourse().getName() + ";" + classes.getLecturer() + "\n");
            for(ClassesDetails details : classes.getClassesDetails()) {
                response.getWriter().print(details.getDay().getName().value() + ";" + DateUtils.parseToString(DateUtils.PATTERN_HOUR, details.getHoursFrom()) + "-" +
                        DateUtils.parseToString(DateUtils.PATTERN_HOUR, details.getHoursTo()) + ";" + Optional.ofNullable(details.getBuilding()).orElse("") +
                        ", " + Optional.ofNullable(details.getRoomNumber()).orElse("") + "\n");
            }
            Integer position = 1;
            for(User user : classes.getUsers()) {
                response.getWriter().print(position++ + ";" + user.getFirstName() + ";" + user.getLastName() + "\n");
            }
            response.getWriter().print("\n");
        }
        response.getWriter().close();
    }

}
