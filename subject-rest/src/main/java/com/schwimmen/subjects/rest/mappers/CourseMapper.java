package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.Course;

import java.util.Date;
import java.util.Optional;

public class CourseMapper {

    private Long id;
    private String name;
    private String description;
    private Date startEnrolmentDate;
    private Date endEnrolmentDate;

    public static CourseMapper mapCourse(Course course) {
        CourseMapper mapper = new CourseMapper();
        mapper.setId(course.getId());
        mapper.setName(course.getName());
        mapper.setDescription(Optional.ofNullable(course.getDescription()).orElse(""));
        mapper.setStartEnrolmentDate(course.getStartEnrolmentDate());
        mapper.setEndEnrolmentDate(course.getEndEnrolmentDate());
        return mapper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Optional.ofNullable(description).orElse("");
    }

    public Date getStartEnrolmentDate() {
        return startEnrolmentDate;
    }

    public void setStartEnrolmentDate(Date startEnrolmentDate) {
        this.startEnrolmentDate = startEnrolmentDate;
    }

    public Date getEndEnrolmentDate() {
        return endEnrolmentDate;
    }

    public void setEndEnrolmentDate(Date endEnrolmentDate) {
        this.endEnrolmentDate = endEnrolmentDate;
    }

}
