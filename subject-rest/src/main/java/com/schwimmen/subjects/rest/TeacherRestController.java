package com.schwimmen.subjects.rest;

import com.schwimmen.subjects.entity.*;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.rest.mappers.CourseMapperWithNumberOfPeople;
import com.schwimmen.subjects.rest.mappers.CourseWithClassesMapper;
import com.schwimmen.subjects.rest.mappers.UserMapper;
import com.schwimmen.subjects.rest.request.ClassesDetailsRequest;
import com.schwimmen.subjects.rest.request.ClassesRequest;
import com.schwimmen.subjects.rest.request.CourseRequest;
import com.schwimmen.subjects.rest.response.SizeClassesResponse;
import com.schwimmen.subjects.service.interfaces.ClassesService;
import com.schwimmen.subjects.service.interfaces.CourseService;
import com.schwimmen.subjects.service.interfaces.UserRestService;
import com.schwimmen.subjects.service.interfaces.UserService;
import com.schwimmen.subjects.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequestMapping("/teacher")
public class TeacherRestController {

    private final UserService userService;
    private final UserRestService userRestService;
    private final CourseService courseService;
    private final ClassesService classesService;

    @Autowired
    public TeacherRestController(UserRestService userRestService, CourseService courseService
            , ClassesService classesService, UserService userService) {
        this.userRestService = userRestService;
        this.courseService = courseService;
        this.classesService = classesService;
        this.userService = userService;
    }

    @RequestMapping(value = "/sizeAllClasses", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getSizeTeacherClasses(@RequestHeader(value="${jwt.header}") String token) {
        return ResponseEntity.ok(Optional.ofNullable(userRestService.findUserFromToken(token))
                .map(user -> new SizeClassesResponse(user.getTeacherCourses().size()))
                .orElse(new SizeClassesResponse(0)));
    }

    @RequestMapping(value = "/sizeClasses", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getTeacherSizeClasses(@RequestHeader(value="${jwt.header}") String token
            , @RequestParam Integer size, @RequestParam Integer from, @RequestParam String search) {
        if(Objects.isNull(size) || Objects.isNull(from) || size < 0 || from <= 0) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Size and from parameter cannot be null or negative");
        }
        User user = userRestService.findUserFromToken(token);
        return ResponseEntity.ok(new SizeClassesResponse(user.getTeacherCourses().stream()
                .filter(course -> Pattern.compile(Pattern.quote(search), Pattern.CASE_INSENSITIVE)
                        .matcher(course.getName()).find())
                .collect(Collectors.toList()).size()));
    }

    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getTeacherClasses(@RequestHeader(value="${jwt.header}") String token
            , @RequestParam Integer size, @RequestParam Integer from, @RequestParam String search) {
        if(Objects.isNull(size) || Objects.isNull(from) || size < 0 || from <= 0) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Size and from parameter cannot be null or negative");
        }
        User user = userRestService.findUserFromToken(token);
        return ResponseEntity.ok(user.getTeacherCourses().stream()
                .filter(course -> Pattern.compile(Pattern.quote(search), Pattern.CASE_INSENSITIVE)
                        .matcher(course.getName()).find())
                .limit(size * from)
                .skip((from - 1) * size)
                .map(CourseMapperWithNumberOfPeople::mapCourse)
                .collect(Collectors.toList()));
    }

    @RequestMapping(value = "/teachers/{courseId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getTeachers(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long courseId) {
        ResponseEntity response;
        try {
            response = ResponseEntity.ok().body(Optional.ofNullable(courseService.getCourse(courseId))
                    .orElseThrow(() -> new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST))
                    .getTeacherUsers().stream().map(UserMapper::mapUser).collect(Collectors.toList()));
        } catch (ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/teacherClasses/{courseId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getTeacherClasses(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long courseId) {
        ResponseEntity response;
        try {
            User user = userRestService.findUserFromToken(token);
            response = ResponseEntity.ok().body(user.getTeacherCourses().stream()
                    .filter(course -> course.getId().equals(courseId))
                    .findFirst().map(CourseWithClassesMapper::mapCourse)
                    .orElseThrow(() -> new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST)));
        } catch(ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/course/{courseId}", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> clearCourseList(@PathVariable Long courseId) {
        ResponseEntity response = ResponseEntity.ok("Course was cleared");
        try {
            courseService.clearStudentCourse(courseId);
        } catch (ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/course/{courseId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> removeCourse(@PathVariable Long courseId) {
        ResponseEntity response = ResponseEntity.ok("Course was cleared");
        try {
            courseService.removeCourse(courseId);
        } catch (ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/course", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> createCourse(@RequestBody CourseRequest course) {
        if(Objects.isNull(course.getTeacherId()) || course.getTeacherId().isEmpty()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Teacher id list cannot be empty.");
        }
        List<User> teachers = userService.findUserByRoleAndId(AuthorityName.ROLE_TEACHER, course.getTeacherId());
        if(teachers.size() != course.getTeacherId().size()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("There are no users with id: " +
                    usersNoExists(course.getTeacherId(), teachers.stream().map(User::getId).collect(Collectors.toList()))
                            .stream().map(Object::toString).collect(Collectors.joining(", ")));
        }
        ResponseEntity response = ResponseEntity.ok("Course was created.");
        try {
            Course courseToCreate = processCourseCreation(course);
            courseToCreate.setTeacherUsers(teachers);
            courseService.createCourseWithClasses(courseToCreate);
        } catch (ParseException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body("Wrong pattern date. Mandatory pattern: " +
                    DateUtils.PATTERN_DATE);
        } catch (RuntimeException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/course", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> updateCourse(@RequestBody CourseRequest course) {
        if(Objects.isNull(course.getTeacherId()) || course.getTeacherId().isEmpty()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Teacher id list cannot be empty.");
        }
        List<User> teachers = userService.findUserByRoleAndId(AuthorityName.ROLE_TEACHER, course.getTeacherId());
        if(teachers.size() != course.getTeacherId().size()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("There are no users with id: " +
                    usersNoExists(course.getTeacherId(), teachers.stream().map(User::getId).collect(Collectors.toList()))
                            .stream().map(Object::toString).collect(Collectors.joining(", ")));
        }
        ResponseEntity response = ResponseEntity.ok("Course was created.");
        try {
            Course courseToCreate = processCourseCreation(course);
            courseToCreate.setTeacherUsers(teachers);
            courseService.updateCourseWithClasses(courseToCreate);
        } catch (ParseException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body("Wrong pattern date. Mandatory pattern: " +
                    DateUtils.PATTERN_DATE);
        } catch (RuntimeException | ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    private List<Long> usersNoExists(List<Long> allToFind, List<Long> exists) {
        List<Long> userIdNoExist = new ArrayList<>();
        allToFind.forEach(id -> {
            if(!exists.contains(id)) {
                userIdNoExist.add(id);
            }
        });
        return userIdNoExist;
    }

    private Course processCourseCreation(CourseRequest course) throws ParseException {
        Course newCourse = new Course();
        newCourse.setId(course.getId());
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setStartEnrolmentDate(DateUtils.parseDate(DateUtils.PATTERN_DATE, course.getStartEnrolmentDate()));
        newCourse.setEndEnrolmentDate(DateUtils.parseDate(DateUtils.PATTERN_DATE, course.getEndEnrolmentDate()));
        course.getClasses().forEach((key, value) -> newCourse.getClasses().addAll(processClassesList(key, value)));
        return newCourse;
    }

    private List<Classes> processClassesList(Long typeClassId, List<ClassesRequest> classes) {
        try {
            TypeOfCourse typeOfCourse = courseService.getTypeOfCourse(typeClassId);
            return classes.stream().map(item -> {
                Classes clazz = processClasses(item);
                clazz.setTypeOfCourse(typeOfCourse);
                return clazz;
            }).collect(Collectors.toList());
        } catch(ApplicationException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private Classes processClasses(ClassesRequest classes) {
        Classes newClasses = new Classes();
        newClasses.setId(classes.getId());
        newClasses.setMaxGroupNumber(classes.getMaxGroupNumber());
        newClasses.setDescription(classes.getDescription());
        newClasses.setLecturer(classes.getLecturer());
        newClasses.setClassesDetails(classes.getClassesDetails().stream().map(this::processClassesDetails)
                .collect(Collectors.toList()));
        return newClasses;
    }

    private ClassesDetails processClassesDetails(ClassesDetailsRequest classesDetails) {
        try {
            ClassesDetails newDetails = new ClassesDetails();
            newDetails.setId(classesDetails.getId());
            newDetails.setBuilding(classesDetails.getBuilding());
            newDetails.setRoomNumber(classesDetails.getRoomNumber());
            newDetails.setHoursFrom(DateUtils.parseDate(DateUtils.PATTERN_HOUR, classesDetails.getHourFrom()));
            newDetails.setHoursTo(DateUtils.parseDate(DateUtils.PATTERN_HOUR, classesDetails.getHourTo()));
            newDetails.setDay(classesService.getDayByName(Days.getDayByCode(classesDetails.getDay())));
            return newDetails;
        } catch (ParseException e) {
            throw new RuntimeException("Wrong pattern hours. Mandatory pattern: " + DateUtils.PATTERN_HOUR);
        } catch (ApplicationException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
