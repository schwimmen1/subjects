package com.schwimmen.subjects.rest;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.entity.security.ApplicationUser;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.rest.request.RegistrationRequest;
import com.schwimmen.subjects.rest.request.UnblockUser;
import com.schwimmen.subjects.security.JwtTokenUtil;
import com.schwimmen.subjects.service.interfaces.MailService;
import com.schwimmen.subjects.service.interfaces.ParameterService;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class RegistrationRestController {

    private final JwtTokenUtil jwtTokenUtil;
    private final MailService mailService;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final ParameterService parameterService;

    @Autowired
    public RegistrationRestController(UserService userService, PasswordEncoder passwordEncoder, MailService mailService
            , JwtTokenUtil jwtTokenUtil, ParameterService parameterService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.mailService = mailService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.parameterService = parameterService;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ResponseEntity<?> registration(@RequestBody RegistrationRequest user) {
        ResponseEntity<?> response = ResponseEntity.status(HttpStatus.OK).body("User was registered.");
        if(parameterService.getParameterValue(Parameters.REGISTRATION_AVAILABLE).equals("0")) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Registration is not allow.");
        }
        try {
            ApplicationUser mappedUser = mapRequestToUser(user);
            mappedUser.setUnblockToken(jwtTokenUtil.generateUnblockRegistrationToken(user.getUsername()));
            userService.createUser(mappedUser, user.getAuthority());
            sendUnblockMail(mappedUser);
        } catch (ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        } catch (Exception e) {
            response = ResponseEntity.status(HttpStatus.NOT_MODIFIED).body("Internal Error");
        }
        return response;
    }

    private ApplicationUser mapRequestToUser(RegistrationRequest user) {
        ApplicationUser newUser = new ApplicationUser();
        newUser.setFirstName(eliminateNullString(user.getFirstname()));
        newUser.setLastName(eliminateNullString(user.getLastname()));
        newUser.setUsername(eliminateNullString(user.getUsername()));
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setPassword("");
        newUser.setEmail(eliminateNullString(user.getEmail()));
        newUser.setLdap(Boolean.FALSE);
        newUser.setEnabled(Boolean.FALSE);
        newUser.setLastPasswordResetDate(new Date());
        return newUser;
    }

    private String eliminateNullString(String string) {
        return Optional.ofNullable(string).map(String::trim).orElse("");
    }

    private void sendUnblockMail(ApplicationUser user) {
        Map<String, Object> model = new HashMap<>();
        model.put("user", user.getFirstName());
        model.put("unblockToken", user.getUnblockToken());
        mailService.sendMail(user.getEmail(), "Rejestracja", "registration.ftl", model);
    }

    @RequestMapping(value = "/registration/sendActivationEmailAgain", method = RequestMethod.POST)
    public ResponseEntity<?> sendActivationEmailAgain(@RequestBody UnblockUser unblockUser) {
        Optional.ofNullable(userService.findUserByUsername(unblockUser.getUnblockUsername())).ifPresent(this::sendUnblockMail);
        return ResponseEntity.ok("Mail was send.");
    }

    @RequestMapping(value = "/registration/unblock", method = RequestMethod.POST)
    public ResponseEntity<?> unblockUser(@RequestBody UnblockUser unblockToken) {
        return Optional.ofNullable(jwtTokenUtil.getUsernameFromToken(unblockToken.getUnblockToken()))
                .map(login -> {
                    userService.unblockUser(login, unblockToken.getUnblockToken());
                    return ResponseEntity.ok().body("Account was unlocked successfully.");
                }).orElse(ResponseEntity.status(HttpStatus.FORBIDDEN).body("Username does not exist!"));
    }

    @RequestMapping(value = "/registration/checkLogin", method = RequestMethod.POST)
    public ResponseEntity<Boolean> checkLoginAvailable(@RequestBody String login) {
        return ResponseEntity.ok(!Optional.ofNullable(userService.findUserByUsername(login)).isPresent());
    }

    @RequestMapping(value = "/registration/checkEmail", method = RequestMethod.POST)
    public ResponseEntity<Boolean> checkEmailAvailable(@RequestBody String email) {
        return ResponseEntity.ok(!Optional.ofNullable(userService.findUserByEmail(email)).isPresent());
    }

}
