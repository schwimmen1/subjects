package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.Classes;

import java.util.List;
import java.util.stream.Collectors;

public class ClassesWithStudentsMapper {

    private Long id;
    private String typeOfCourse;
    private Integer actualGroupNumber;
    private Integer maxGroupNumber;
    private String lecturer;
    private String description;
    private List<ClassesDetailsMapper> classesDetails;
    private List<UserMapper> students;

    public static ClassesWithStudentsMapper mapClasses(Classes classes) {
        ClassesWithStudentsMapper mapper = new ClassesWithStudentsMapper();
        mapper.setId(classes.getId());
        mapper.setTypeOfCourse(classes.getTypeOfCourse().getName());
        mapper.setActualGroupNumber(classes.getActualGroupNumber());
        mapper.setMaxGroupNumber(classes.getMaxGroupNumber());
        mapper.setLecturer(classes.getLecturer());
        mapper.setDescription(classes.getDescription());
        mapper.setClassesDetails(classes.getClassesDetails().stream().map(ClassesDetailsMapper::mapClassesDetail)
                .collect(Collectors.toList()));
        mapper.setStudents(classes.getUsers().stream().map(UserMapper::mapUser).collect(Collectors.toList()));
        return mapper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeOfCourse() {
        return typeOfCourse;
    }

    public void setTypeOfCourse(String typeOfCourse) {
        this.typeOfCourse = typeOfCourse;
    }

    public Integer getActualGroupNumber() {
        return actualGroupNumber;
    }

    public void setActualGroupNumber(Integer actualGroupNumber) {
        this.actualGroupNumber = actualGroupNumber;
    }

    public Integer getMaxGroupNumber() {
        return maxGroupNumber;
    }

    public void setMaxGroupNumber(Integer maxGroupNumber) {
        this.maxGroupNumber = maxGroupNumber;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ClassesDetailsMapper> getClassesDetails() {
        return classesDetails;
    }

    public void setClassesDetails(List<ClassesDetailsMapper> classesDetails) {
        this.classesDetails = classesDetails;
    }

    public List<UserMapper> getStudents() {
        return students;
    }

    public void setStudents(List<UserMapper> students) {
        this.students = students;
    }

}
