package com.schwimmen.subjects.rest.response;

import com.schwimmen.subjects.rest.mappers.CourseMapper;

import java.util.List;

public class StudentCoursesResponse {

    private List<CourseMapper> selectedStudentCourses;
    private List<CourseMapper> unselectedStudentCourses;

    public List<CourseMapper> getSelectedStudentCourses() {
        return selectedStudentCourses;
    }

    public void setSelectedStudentCourses(List<CourseMapper> selectedStudentCourses) {
        this.selectedStudentCourses = selectedStudentCourses;
    }

    public List<CourseMapper> getUnselectedStudentCourses() {
        return unselectedStudentCourses;
    }

    public void setUnselectedStudentCourses(List<CourseMapper> unselectedStudentCourses) {
        this.unselectedStudentCourses = unselectedStudentCourses;
    }

}
