package com.schwimmen.subjects.rest.request;

import java.util.List;

public class ClassesRequest {

    private Long id;
    private Integer maxGroupNumber;
    private String lecturer;
    private String description;
    private List<ClassesDetailsRequest> classesDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaxGroupNumber() {
        return maxGroupNumber;
    }

    public void setMaxGroupNumber(Integer maxGroupNumber) {
        this.maxGroupNumber = maxGroupNumber;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ClassesDetailsRequest> getClassesDetails() {
        return classesDetails;
    }

    public void setClassesDetails(List<ClassesDetailsRequest> classesDetails) {
        this.classesDetails = classesDetails;
    }

}
