package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.Course;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClassesMapper {

    private Long id;
    private Long courseId;
    private String typeOfCourse;
    private Integer actualGroupNumber;
    private Integer maxGroupNumber;
    private String lecturer;
    private String description;
    private boolean isChosen;
    private List<ClassesDetailsMapper> classesDetails;

    public static ClassesMapper mapClasses(Classes classes) {
        return mapClasses(classes, new ArrayList<>());
    }

    public static ClassesMapper mapClasses(Classes classes, List<Classes> selectedClasses) {
        ClassesMapper mapper = new ClassesMapper();
        mapper.setId(classes.getId());
        mapper.setCourseId(Optional.ofNullable(classes.getCourse()).map(Course::getId).orElse(0L));
        mapper.setTypeOfCourse(classes.getTypeOfCourse().getName());
        mapper.setActualGroupNumber(classes.getActualGroupNumber());
        mapper.setMaxGroupNumber(classes.getMaxGroupNumber());
        mapper.setLecturer(classes.getLecturer());
        mapper.setDescription(classes.getDescription());
        mapper.setChosen(selectedClasses.stream().anyMatch(selected -> selected.getId().equals(classes.getId())));
        mapper.setClassesDetails(classes.getClassesDetails().stream().map(ClassesDetailsMapper::mapClassesDetail)
                .collect(Collectors.toList()));
        return mapper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getTypeOfCourse() {
        return typeOfCourse;
    }

    public void setTypeOfCourse(String typeOfCourse) {
        this.typeOfCourse = typeOfCourse;
    }

    public Integer getActualGroupNumber() {
        return actualGroupNumber;
    }

    public void setActualGroupNumber(Integer actualGroupNumber) {
        this.actualGroupNumber = actualGroupNumber;
    }

    public Integer getMaxGroupNumber() {
        return maxGroupNumber;
    }

    public void setMaxGroupNumber(Integer maxGroupNumber) {
        this.maxGroupNumber = maxGroupNumber;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isChosen() {
        return isChosen;
    }

    public void setChosen(boolean chosen) {
        isChosen = chosen;
    }

    public List<ClassesDetailsMapper> getClassesDetails() {
        return classesDetails;
    }

    public void setClassesDetails(List<ClassesDetailsMapper> classesDetails) {
        this.classesDetails = classesDetails;
    }

}
