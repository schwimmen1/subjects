package com.schwimmen.subjects.rest.response;

public class SizeClassesResponse {

    private Integer size;

    public SizeClassesResponse(Integer size) {
        this.size = size;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
