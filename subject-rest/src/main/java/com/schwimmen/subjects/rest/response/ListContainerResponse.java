package com.schwimmen.subjects.rest.response;

import java.util.List;

public class ListContainerResponse {

    private Integer maxSizeList;
    private List<Object> result;

    public ListContainerResponse(Integer maxSizeList, List<Object> result) {
        this.maxSizeList = maxSizeList;
        this.result = result;
    }

    public Integer getMaxSizeList() {
        return maxSizeList;
    }

    public void setMaxSizeList(Integer maxSizeList) {
        this.maxSizeList = maxSizeList;
    }

    public List<Object> getResult() {
        return result;
    }

    public void setResult(List<Object> result) {
        this.result = result;
    }

}
