package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.security.User;

public class UserMapper {

    private Long id;
    private String firstName;
    private String lastName;

    public static UserMapper mapUser(User user) {
        UserMapper mapper = new UserMapper();
        mapper.setId(user.getId());
        mapper.setFirstName(user.getFirstName());
        mapper.setLastName(user.getLastName());
        return mapper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}

