package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.Course;

import java.util.Optional;

public class CourseMapperWithNumberOfPeople extends CourseMapper {

    private Integer numberOfPeople;

    public static CourseMapper mapCourse(Course course) {
        CourseMapperWithNumberOfPeople mapper = new CourseMapperWithNumberOfPeople();
        mapper.setId(course.getId());
        mapper.setName(course.getName());
        mapper.setDescription(Optional.ofNullable(course.getDescription()).orElse(""));
        mapper.setStartEnrolmentDate(course.getStartEnrolmentDate());
        mapper.setEndEnrolmentDate(course.getEndEnrolmentDate());
        mapper.setNumberOfPeople(course.getStudentUsers().size());
        return mapper;
    }

    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

}
