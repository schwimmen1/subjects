package com.schwimmen.subjects.rest.mappers;

import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.Course;

import java.util.*;

public class CourseWithClassesMapper extends CourseMapper {

    private Map<String, List<ClassesMapper>> classesWithTypes;

    public static CourseWithClassesMapper mapCourse(Course course, List<Classes> selectedClasses) {
        CourseWithClassesMapper mapper = getCourseWithClassesMapper(course);
        mapper.setClassesWithTypes(getClassesDependingOnType(course.getClasses(), selectedClasses));
        return mapper;
    }

    public static CourseWithClassesMapper mapCourse(Course course) {
        CourseWithClassesMapper mapper = getCourseWithClassesMapper(course);
        mapper.setClassesWithTypes(getClassesDependingOnType(course.getClasses(), course.getClasses()));
        return mapper;
    }

    private static CourseWithClassesMapper getCourseWithClassesMapper(Course course) {
        CourseWithClassesMapper mapper = new CourseWithClassesMapper();
        mapper.setId(course.getId());
        mapper.setName(course.getName());
        mapper.setDescription(Optional.ofNullable(course.getDescription()).orElse(""));
        mapper.setStartEnrolmentDate(course.getStartEnrolmentDate());
        mapper.setEndEnrolmentDate(course.getEndEnrolmentDate());
        return mapper;
    }

    private static HashMap<String, List<ClassesMapper>> getClassesDependingOnType(List<Classes> classes
            , List<Classes> selectedClasses) {
        Collections.sort(classes);
        HashMap<String, List<ClassesMapper>> classesWithTypes = new HashMap<>();
        classes.forEach(item -> {
            List<ClassesMapper> orDefault = classesWithTypes.getOrDefault(item.getTypeOfCourse().getName(), new ArrayList<>());
            orDefault.add(ClassesMapper.mapClasses(item, selectedClasses));
            classesWithTypes.put(item.getTypeOfCourse().getName(), orDefault);
        });
        return classesWithTypes;
    }

    public Map<String, List<ClassesMapper>> getClassesWithTypes() {
        return classesWithTypes;
    }

    public void setClassesWithTypes(Map<String, List<ClassesMapper>> classesWithTypes) {
        this.classesWithTypes = classesWithTypes;
    }

}
