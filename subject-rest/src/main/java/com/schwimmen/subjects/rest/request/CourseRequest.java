package com.schwimmen.subjects.rest.request;

import java.util.List;
import java.util.Map;

public class CourseRequest {

    private Long id;
    private String name;
    private String description;
    private String startEnrolmentDate;
    private String endEnrolmentDate;
    private Map<Long, List<ClassesRequest>> classes;
    private List<Long> teacherId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartEnrolmentDate() {
        return startEnrolmentDate;
    }

    public void setStartEnrolmentDate(String startEnrolmentDate) {
        this.startEnrolmentDate = startEnrolmentDate;
    }

    public String getEndEnrolmentDate() {
        return endEnrolmentDate;
    }

    public void setEndEnrolmentDate(String endEnrolmentDate) {
        this.endEnrolmentDate = endEnrolmentDate;
    }

    public Map<Long, List<ClassesRequest>> getClasses() {
        return classes;
    }

    public void setClasses(Map<Long, List<ClassesRequest>> classes) {
        this.classes = classes;
    }

    public List<Long> getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(List<Long> teacherId) {
        this.teacherId = teacherId;
    }

}
