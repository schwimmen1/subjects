package com.schwimmen.subjects.rest.request;

public class UnblockUser {

    private String unblockToken;
    private String unblockUsername;

    public String getUnblockToken() {
        return unblockToken;
    }

    public void setUnblockToken(String unblockToken) {
        this.unblockToken = unblockToken;
    }

    public String getUnblockUsername() {
        return unblockUsername;
    }

    public void setUnblockUsername(String unblockUsername) {
        this.unblockUsername = unblockUsername;
    }

}
