package com.schwimmen.subjects.rest.request;

import java.util.List;

public class ListIdRequest {

    private List<Long> id;

    public List<Long> getId() {
        return id;
    }

    public void setId(List<Long> id) {
        this.id = id;
    }

}
