package com.schwimmen.subjects.rest;

import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.Course;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.rest.mappers.ClassesMapper;
import com.schwimmen.subjects.rest.mappers.CourseMapper;
import com.schwimmen.subjects.rest.mappers.CourseWithClassesMapper;
import com.schwimmen.subjects.rest.response.StudentCoursesResponse;
import com.schwimmen.subjects.service.interfaces.ClassesService;
import com.schwimmen.subjects.service.interfaces.CourseService;
import com.schwimmen.subjects.service.interfaces.UserRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/student")
public class StudentRestController {

    private final UserRestService userService;
    private final CourseService courseService;
    private final ClassesService classesService;

    @Autowired
    public StudentRestController(CourseService courseService, ClassesService classesService, UserRestService userService) {
        this.courseService = courseService;
        this.classesService = classesService;
        this.userService = userService;
    }

    @RequestMapping(value = "/getStudentCourses", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getStudentCourses(@RequestHeader(value="${jwt.header}") String token) {
        User user = userService.findUserFromToken(token);
        List<Course> unselectedStudentCourses = courseService.getUnselectedStudentCourses(user);
        StudentCoursesResponse response = new StudentCoursesResponse();
        response.setSelectedStudentCourses(mapCourses(courseService.getSelectedStudentCoursesBetweenDate(user)));
        response.setUnselectedStudentCourses(mapCourses(unselectedStudentCourses));
        return ResponseEntity.ok(response);
    }

    private List<CourseMapper> mapCourses(List<Course> courses) {
        return courses.stream().map(CourseMapper::mapCourse).collect(Collectors.toList());
    }

    @RequestMapping(value = "/studentCourses/{courseId}", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> addStudentCourse(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long courseId) {
        ResponseEntity<String> response = ResponseEntity.ok().body("Courses was added.");
        try {
            User user = userService.findUserFromToken(token);
            courseService.addStudentCourse(user, courseId);
        } catch(ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/studentCourses/{courseId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> removeStudentCourse(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long courseId) {
        ResponseEntity<String> response = ResponseEntity.ok().body("Courses was removed.");
        try {
            User user = userService.findUserFromToken(token);
            courseService.removeStudentCourse(user, courseId);
        } catch(ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/selectedCourses", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getSelectedStudentCourses(@RequestHeader(value="${jwt.header}") String token) {
        User user = userService.findUserFromToken(token);
        return ResponseEntity.ok(courseService.getSelectedStudentCoursesBetweenDate(user).stream()
                .map(CourseMapper::mapCourse).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/studentClasses", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getStudentClasses(@RequestHeader(value="${jwt.header}") String token) {
        User user = userService.findUserFromToken(token);
        List<Classes> selectedClasses = user.getSelectedStudentClasses();
        return ResponseEntity.ok().body(user.getStudentCourses().stream()
                .map(course -> CourseWithClassesMapper.mapCourse(course, selectedClasses))
                .collect(Collectors.toList()));
    }

    @RequestMapping(value = "/studentClasses/{courseId}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getChosenStudentClasses(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long courseId) {
        User user = userService.findUserFromToken(token);
        List<Classes> selectedClasses = user.getSelectedStudentClasses();
        return ResponseEntity.ok().body(user.getStudentCourses().stream().filter(item -> item.getId().equals(courseId))
                .findFirst().map(course -> CourseWithClassesMapper.mapCourse(course, selectedClasses))
                .orElse(null));
    }

    @RequestMapping(value = "/classes/{classesId}", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<String> addStudentClasses(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long classesId) {
        ResponseEntity<String> response = ResponseEntity.ok().body("Student was assigned to this classes.");
        try {
            classesService.addStudentClasses(classesId, userService.findUserFromToken(token));
        } catch(ApplicationException e) {
            response = ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/classes/{classesId}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<String> deleteStudentClasses(@RequestHeader(value="${jwt.header}") String token
            , @PathVariable Long classesId) {
        classesService.deleteStudentClasses(classesId, userService.findUserFromToken(token));
        return ResponseEntity.ok().body("Student resigned from course.");
    }

    @RequestMapping(value = "/selectedClasses", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getSelectedStudentClasses(@RequestHeader(value="${jwt.header}") String token) {
        User user = userService.findUserFromToken(token);
        Map<String, List<ClassesMapper>> mapper = new HashMap<>();
        user.getSelectedStudentClasses().forEach(item -> {
            List<ClassesMapper> orDefault = mapper.getOrDefault(item.getCourse().getName(), new ArrayList<>());
            orDefault.add(ClassesMapper.mapClasses(item));
            mapper.put(item.getCourse().getName(), orDefault);
        });
        return ResponseEntity.ok(mapper);
    }

}