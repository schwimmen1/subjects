package com.schwimmen.subjects.configuration.handlers;

import com.schwimmen.subjects.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.lang.reflect.UndeclaredThrowableException;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ ApplicationException.class })
    public ResponseEntity<?> handleAccessDeniedException(UndeclaredThrowableException exception) {
        if(exception.getUndeclaredThrowable() instanceof ApplicationException) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(exception.getUndeclaredThrowable().getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unexpected error!");
    }

}