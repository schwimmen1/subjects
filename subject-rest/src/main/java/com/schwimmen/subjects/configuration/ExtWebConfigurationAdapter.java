package com.schwimmen.subjects.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ExtWebConfigurationAdapter extends WebMvcConfigurerAdapter {

    private final Environment environment;

    @Autowired
    public ExtWebConfigurationAdapter(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(environment.getProperty("cors.mapping"))
                .allowedOrigins(environment.getProperty("cors.mapping.allowedOrigins"))
                .allowedMethods(environment.getProperty("cors.mapping,allowedMethods"));
    }

}
