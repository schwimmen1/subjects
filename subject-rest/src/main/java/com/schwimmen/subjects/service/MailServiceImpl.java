package com.schwimmen.subjects.service;

import com.schwimmen.subjects.service.interfaces.MailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;
    private final Configuration freemarkerConfig;

    @Autowired
    public MailServiceImpl(JavaMailSender javaMailSender, Configuration freemarkerConfig) {
        this.javaMailSender = javaMailSender;
        this.freemarkerConfig = freemarkerConfig;
    }

    public void sendMail(String sendTo, String mailSubjects, String templateName, Map<String, Object> model) {
        MimeMessage mail = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(sendTo);
            helper.setSubject(mailSubjects);
            helper.setText(getTextMessage(templateName, model), true);
            javaMailSender.send(mail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getTextMessage(String templateName, Map<String, Object> model) throws IOException, TemplateException {
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");
        Template template = freemarkerConfig.getTemplate(templateName);
        return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
    }

}
