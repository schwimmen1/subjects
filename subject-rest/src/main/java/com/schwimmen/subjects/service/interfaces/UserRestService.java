package com.schwimmen.subjects.service.interfaces;

import com.schwimmen.subjects.entity.security.User;

public interface UserRestService {

    User findUserFromToken(String token);

}
