package com.schwimmen.subjects.service.interfaces;

import java.util.Map;

public interface MailService {

    void sendMail(String sendTo, String mailSubjects, String templateName, Map<String, Object> model);

}
