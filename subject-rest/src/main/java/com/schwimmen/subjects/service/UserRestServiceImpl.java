package com.schwimmen.subjects.service;

import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.security.JwtTokenUtil;
import com.schwimmen.subjects.service.interfaces.UserRestService;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRestServiceImpl implements UserRestService {

    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;

    @Autowired
    public UserRestServiceImpl(JwtTokenUtil jwtTokenUtil, UserService userService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }

    @Override
    public User findUserFromToken(String token) {
        String username = jwtTokenUtil.getUsernameFromToken(token);
        Boolean isLdap = jwtTokenUtil.isLdapFromToken(token);
        return userService.findUserByUsername(username, isLdap);
    }

}
