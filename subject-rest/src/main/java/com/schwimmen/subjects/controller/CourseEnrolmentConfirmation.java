package com.schwimmen.subjects.controller;

import com.schwimmen.subjects.entity.CourseEnrolmentAction;
import com.schwimmen.subjects.entity.CourseEnrolmentHistory;
import com.schwimmen.subjects.service.interfaces.CourseService;
import com.schwimmen.subjects.service.interfaces.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.*;

@Controller
@EnableScheduling
public class CourseEnrolmentConfirmation {

    private final CourseService courseService;
    private final MailService mailService;

    @Autowired
    public CourseEnrolmentConfirmation(CourseService courseService, MailService mailService) {
        this.courseService = courseService;
        this.mailService = mailService;
    }

    @Scheduled(fixedDelay = 360000)
    public void sendMailCourseEnrolConfirm() {
        List<CourseEnrolmentHistory> allCourseHistory = courseService.getAllCourseHistory();
        if(Objects.isNull(allCourseHistory) || allCourseHistory.isEmpty()) {
            return;
        }
        mapCourseHistoryByEmailUser(allCourseHistory).forEach(this::processUserCourseHistory);
        courseService.removeAllCourseHistory();
    }

    private Map<String, List<CourseEnrolmentHistory>> mapCourseHistoryByEmailUser(List<CourseEnrolmentHistory> courseHistory) {
        Map<String, List<CourseEnrolmentHistory>> users = new HashMap<>();
        courseHistory.forEach(history -> {
            List<CourseEnrolmentHistory> orDefault = users.getOrDefault(history.getUser().getEmail(), new ArrayList<>());
            orDefault.add(history);
            users.put(history.getUser().getEmail(), orDefault);
        });
        return users;
    }

    private void processUserCourseHistory(String mail, List<CourseEnrolmentHistory> courseHistory) {
        List<String> chosenCourse = new ArrayList<>();
        List<String> rejectedCourse = new ArrayList<>();
        courseHistory.forEach(history -> {
            String courseName = history.getCourse().getName();
            if(history.getAction() == CourseEnrolmentAction.ADD) {
                chosenCourse.add(courseName);
            } else {
                rejectedCourse.add(courseName);
            }
        });
        Map<String, Object> model = new HashMap<>();
        model.put("user", courseHistory.get(0).getUser().getFirstName());
        model.put("chosenCourse", chosenCourse);
        model.put("rejectedCourse", rejectedCourse);
        mailService.sendMail(mail, "Potwierdzenie zapisu na przedmioty", "courseConfirmation.ftl", model);
    }

}
