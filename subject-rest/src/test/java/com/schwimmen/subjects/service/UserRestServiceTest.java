package com.schwimmen.subjects.service;

import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.security.JwtTokenUtil;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserRestServiceTest {

    @Mock
    private JwtTokenUtil jwtTokenUtil;
    @Mock
    private UserService userService;
    @InjectMocks
    private UserRestServiceImpl userRestService;

    private User user;

    @Before
    public void before() {
        user = new User();
    }

    @Test
    public void findUserFromTokenUserNotExistTest() {
        String token = "token";
        String username = "user";
        Boolean isLdap = true;
        user.setLdap(isLdap);
        user.setUsername(username);
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn(username);
        when(jwtTokenUtil.isLdapFromToken(token)).thenReturn(isLdap);
        when(userService.findUserByUsername(username, isLdap)).thenReturn(null);
        User userFromToken = userRestService.findUserFromToken(token);
        assertNull(userFromToken);
    }

    @Test
    public void findUserFromTokenWrongTokenTest() {
        String token = "token";
        String username = "user";
        Boolean isLdap = true;
        user.setLdap(isLdap);
        user.setUsername(username);
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn(null);
        when(jwtTokenUtil.isLdapFromToken(token)).thenReturn(false);
        when(userService.findUserByUsername(username, isLdap)).thenReturn(user);
        User userFromToken = userRestService.findUserFromToken(token);
        assertNull(userFromToken);
    }

    @Test
    public void findUserFromTokenUserIsNotLDAPest() {
        String token = "token";
        String username = "user";
        Boolean isLdap = true;
        user.setLdap(isLdap);
        user.setUsername(username);
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn(username);
        when(jwtTokenUtil.isLdapFromToken(token)).thenReturn(false);
        when(userService.findUserByUsername(username, isLdap)).thenReturn(user);
        User userFromToken = userRestService.findUserFromToken(token);
        assertNull(userFromToken);
    }

    @Test
    public void findUserFromTokenTest() {
        String token = "token";
        String username = "user";
        Boolean isLdap = true;
        user.setLdap(isLdap);
        user.setUsername(username);
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn(username);
        when(jwtTokenUtil.isLdapFromToken(token)).thenReturn(isLdap);
        when(userService.findUserByUsername(username, isLdap)).thenReturn(user);
        User userFromToken = userRestService.findUserFromToken(token);
        assertNotNull(userFromToken);
        assertEquals(userFromToken.getUsername(), username);
        assertEquals(userFromToken.getLdap(), isLdap);
    }

}
