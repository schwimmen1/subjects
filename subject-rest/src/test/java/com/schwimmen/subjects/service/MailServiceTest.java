package com.schwimmen.subjects.service;

import freemarker.template.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
public class MailServiceTest {

    @Mock
    private JavaMailSender javaMailSender;
    @Mock
    private Configuration freemarkerConfig;
    @InjectMocks
    private MailServiceImpl mailService;

    @Test
    public void sendMailTest() {
        String sendTo = "op@op.pl";
        String mailSubjects = "test";
        String templateName = "templateName";
        Map<String, Object> model = new HashMap<>();
        mailService.sendMail(sendTo, mailSubjects, templateName, model);
    }

}
