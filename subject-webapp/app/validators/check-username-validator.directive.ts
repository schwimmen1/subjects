import { Directive, forwardRef } from "@angular/core";
import { NG_ASYNC_VALIDATORS, Validator, AbstractControl} from "@angular/forms";
import { Observable } from "rxjs";

import { HttpRequestService } from '../service/http-request.service';

@Directive({
  selector: "[checkUsername][formControlName], [checkUsername][ngModel]",
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => CheckUsernameValidator), multi: true
    }
  ]
})
export class CheckUsernameValidator implements Validator {

    constructor(private httpRequestService: HttpRequestService) {}

    validate( c : AbstractControl ) : Promise<{[key : string] : any}>|Observable<{[key : string] : any}> {
        return c.valueChanges
            .debounceTime(500)
            .mergeMap(value => this.httpRequestService.postObservable(`registration/checkLogin`, value, false))
            .mergeMap(stat => {
                if(!stat) {
                    c.setErrors({checkUsername: true});
                    return Observable.of({checkUsername: true});
                }
                c.setErrors(null);
                return Observable.of({checkUsername: false});
            });       
    }

}