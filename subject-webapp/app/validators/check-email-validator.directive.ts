import { Directive, forwardRef } from "@angular/core";
import { NG_ASYNC_VALIDATORS, Validator, AbstractControl} from "@angular/forms";
import { Observable } from "rxjs";

import { HttpRequestService } from '../service/http-request.service';

@Directive({
  selector: "[checkEmail][formControlName], [checkEmail][ngModel]",
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => CheckEmailValidator), multi: true
    }
  ]
})
export class CheckEmailValidator implements Validator {

    constructor(private httpRequestService: HttpRequestService) {}

    validate( c : AbstractControl ) : Promise<{[key : string] : any}>|Observable<{[key : string] : any}> {
        return c.valueChanges
            .debounceTime(500)
            .mergeMap(value => this.httpRequestService.postObservable(`registration/checkEmail`, value, false))
            .mergeMap(stat => {
                if(!stat) {
                    c.setErrors({checkEmail: true});
                    return Observable.of({checkEmail: true});
                }
                c.setErrors(null);
                return Observable.of({checkEmail: false});
            });       
    }

}