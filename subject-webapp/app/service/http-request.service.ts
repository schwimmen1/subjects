import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AuthorizationService } from './authorization.service';

@Injectable()
export class HttpRequestService {
    
    //mainUrl: string = 'http://localhost:8080';
    mainUrl: string = 'http://172.20.44.22:8080/';

    constructor(private http: Http) {}

    getHeader(): Headers {
        return new Headers({
            'Content-Type' : 'application/json'
        });
    }

    getHeadersWithAuthorization(): Headers {
        return new Headers({
            'Content-Type' : 'application/json',
            'Authorization' : JSON.parse(localStorage.getItem('subjects')).token
        });
    }
    
    /** Get method without authentication */
    get(url: string): Promise<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return new Promise<any>((resolve, reject) => {
            this.http.get(fullUrl, {headers : this.getHeader()})
            .toPromise()
            .then(response => {
                try {
                    resolve(response.json());
                } catch(e) {
                    resolve(response);
                }
            })
            .catch(e => reject(e));
        });
    }

    /** Get method without authentication */
    getObservable(url: string): Observable<any> {
        return this.http
            .get(`${this.mainUrl}/${url}`, {headers: this.getHeadersWithAuthorization()})
            .map(response => response.json());
    }

    /** Get method wit authentication */
    authenticateGet(url: string): Promise<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return new Promise<any>((resolve, reject) => {
            this.http.get(fullUrl, {headers : this.getHeadersWithAuthorization()})
            .toPromise()
            .then(response => {
                try {
                    resolve(response.json());
                } catch(e) {
                    resolve(response);
                }
            })
            .catch(e => reject(e));
        });
    }

    /** Post method without authentication */
    postObservable(url: string, text: any, json: boolean): Observable<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return this.http.post(fullUrl, json ? JSON.stringify(text) : text, {headers : this.getHeader()}).map(response => response.json());;
    }

    /** Post method without authentication */
    post(url: string, json: any): Promise<any>  {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return this.http.post(fullUrl, JSON.stringify(json), {headers : this.getHeader()})
            .toPromise()
            .then(response => {
                try {
                    Promise.resolve(response.json());
                } catch(e) {
                    Promise.resolve(response);
                }
            })
            .catch(this.handleError);
    }

    /** Put method wit authentication */
    authenticatePost(url: string, json: any): Promise<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return new Promise<any>((resolve, reject) => {
            this.http.post(fullUrl, JSON.stringify(json), {headers : this.getHeadersWithAuthorization()})
            .toPromise()
            .then(response => {
                try {
                    resolve(response.json());
                } catch(e) {
                    resolve(response);
                }
            })
            .catch(e => reject(e));
        });
    }

    /** Put method wit authentication */
    authenticatePut(url: string, json: any): Promise<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return new Promise<any>((resolve, reject) => {
            this.http.put(fullUrl, json, {headers : this.getHeadersWithAuthorization()})
            .toPromise()
            .then(response => {
                try {
                    resolve(response.json());
                } catch(e) {
                    resolve(response);
                }
            })
            .catch(e => reject(e));
        });
    }

    /** Put method wit authentication */
    authenticatePutEmpty(url: string): Promise<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return new Promise<any>((resolve, reject) => {
            this.http.put(fullUrl, null, {headers : this.getHeadersWithAuthorization()})
            .toPromise()
            .then(response => {
                try {
                    resolve(response.json());
                } catch(e) {
                    resolve(response);
                }
            })
            .catch(e => reject(e));
        });
    }

    /** Delete method wit authentication */
    authenticateDelete(url: string): Promise<any> {
        const fullUrl: string = `${this.mainUrl}/${url}`;
        return new Promise<any>((resolve, reject) => {
            this.http.delete(fullUrl, {headers : this.getHeadersWithAuthorization()})
            .toPromise()
            .then(response => {
                try {
                    resolve(response.json());
                } catch(e) {
                    resolve(response);
                }
            })
            .catch(e => reject(e));
        });
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); 
        return Promise.reject(error.message || error);
    }

}