import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router } from '@angular/router';

import { HttpRequestService } from './http-request.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthorizationService {

  localStorage: string = 'subjects';
  //host: string = 'http://localhost:8080/';
  host: string = 'http://172.20.44.22:8080/';
  
  private normalHeader = new Headers({
    'Content-Type' : 'application/json'
  });

  constructor(private http: Http, private router: Router) {}

  getHeadersWithAuthorization(): Headers {
    return new Headers({
        'Content-Type' : 'application/json',
        'Authorization' : JSON.parse(localStorage.getItem('subjects')).token
    });
  }

  login(login: string, password: string, isLdap: boolean): Promise<any> {
    console.log('Logowanie użytkownika: ' + login);
    return new Promise<any>((resolve, reject) => {
    this.http
      .post(`${this.host}auth`, {username : login, password : password, isLdap : isLdap}, {headers: this.normalHeader})
      .toPromise()
      .then(response => {
        localStorage.setItem(this.localStorage, JSON.stringify({
        'token' : response.json().token,
        'firstName' : response.json().firstName,
        'lastName' : response.json().lastName,
        'role' : response.json().role,
        'startEnrolmentDay' : response.json().startEnrolmentDay,
        'endEnrolmentDay' : response.json().endEnrolmentDay,
        }));
        resolve(true);
      })
      .catch(e => reject(e.status));
      });         
    }

    checkToken(): boolean {
      if(!localStorage.getItem(this.localStorage)) {
        return false;
      }
      return true;
    }

    checkPrivilege(role: string): boolean {
      return JSON.parse(localStorage.getItem(this.localStorage)).role === role;
    }

    refreshToken() {
      return new Promise<any>((resolve, reject) => {this.http
        .get(`${this.host}refresh`, {headers: this.getHeadersWithAuthorization()})
        .toPromise()
        .then(response => {
          localStorage.setItem(this.localStorage, JSON.stringify({
          'token' : response.json().token,
          'firstName' : response.json().firstName,
          'lastName' : response.json().lastName,
          'role' : response.json().role,
          'startEnrolmentDay' : response.json().startEnrolmentDay,
          'endEnrolmentDay' : response.json().endEnrolmentDay,
          }));
          resolve(true);
        })
        .catch(e => {
          localStorage.removeItem(this.localStorage);
          reject(e.status);
        });
      });
    }

    logout(): void {
      localStorage.removeItem(this.localStorage);
      this.router.navigate(['login']);
    }

}