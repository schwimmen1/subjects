import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthorizationService } from '../service/authorization.service';

@Injectable()
export class CanActivateViaStudentUser implements CanActivate {

    constructor(private router: Router, private authService: AuthorizationService) {}

    canActivate() {
        if(!this.authService.checkToken()) {
            this.router.navigate(['/login']);
            return false;
        }
        if(!this.authService.checkPrivilege('ROLE_STUDENT')) {
            this.router.navigate(['/login']);
            return false;
        }
        this.authService.refreshToken()
            .then(result => {})
            .catch(e => this.router.navigate(['/login']));
        return true;
    }

}