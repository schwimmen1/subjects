export class User {
  username: string;
  password: string;
  secondPassword: string;
  firstname:string;
  lastname: string;
  email: string;
  authority: string;
  ldap: boolean;
}