export class Parameter {
    id: number;
    name: string;
    description: string;
    value: any;
    type: string;
}