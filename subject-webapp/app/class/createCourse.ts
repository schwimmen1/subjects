import { CreateUser } from './createUsers';

export class CreateCourse {
    id : number;   
    name : string; 
    description : string;
    startEnrolmentDate: Date;
    endEnrolmentDate: Date;
    classes: any;
    teacherId: CreateUser[];
    numberOfPeople: number;
}