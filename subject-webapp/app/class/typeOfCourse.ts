export class TypeOfCourses {
    id: number;
    name: string;
    description: string;
}