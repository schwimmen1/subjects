import { CreateClassesDetails } from './createClassesDetails';

export class CreateClasses {
    id: number;
    maxGroupNumber: number;
    lecturer: string;
    description: string;
    classesDetails: CreateClassesDetails[];
}