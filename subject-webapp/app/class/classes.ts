export class Classes {
    id: number;
    actualGroupNumber: number;
    maxGroupNumber: number;
    day: string;
    typeOfCourse: string;
    roomNumber: string;
    building: string;
    lecturer: string;
    hoursFrom: string;
    hoursTo: string;
    description: string;
    chosen: boolean;
}