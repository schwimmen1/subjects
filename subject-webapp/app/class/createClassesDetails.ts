export class CreateClassesDetails {
    id: number;
    day: string;
    hourFrom: Date;
    hourTo: Date;
    building: string;
    roomNumber: string;
}