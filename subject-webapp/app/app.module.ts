import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login.component';
import { UnblockUserComponent } from './component/unblock-user.component';
import { SubjectsComponet } from './component/subjects.component';
import { HomeSubjectsComponet } from './component/subjects_component/home.component';
import { CoursesSubjectsComponet } from './component/subjects_component/courses.component';
import { ParameterSubjectsComponent } from './component/subjects_component/parameter.component';
import { ClassesStudentSubjectsComponet } from './component/subjects_component/classes-student.component';
import { ClassesStudentResultsSubjectsComponet } from './component/subjects_component/classes-student-result.component';
import { ClassesStudentResultsListSubjectsComponet } from './component/subjects_component/classes-student-result-list.component';
import { TableContentComponent } from './component/subjects_component/table-content.component';
import { TeacherCoursesSubjectsComponet } from './component/subjects_component/teacher-courses-component';
import { InputDebounceComponent } from './component/input-debounce.component';
import { CreateCourseSubjectsComponet } from './component/subjects_component/create-course.component';
import { UpdateCourseSubjectsComponet } from './component/subjects_component/update-course.component';
import { CreateCourseDetailsSubjectsComponet } from './component/subjects_component/create-course-details.component';
import { CreateTypeOfCourseDetailsSubjectsComponet } from './component/subjects_component/create-type-of-course.componnet';
import { CreateCourseTeacherSubjectsComponet } from './component/subjects_component/create-course-teachers.component';
import { ClassesStudentTeacherListSubjectsComponet } from './component/subjects_component/classes-student-list.component';
import { EqualValidator } from './validators/equal-validator.directive';
import { CheckUsernameValidator } from './validators/check-username-validator.directive';
import { CheckEmailValidator } from './validators/check-email-validator.directive';
import { RegistrationComponent } from './component/registration.component';
import { AuthorizationService } from './service/authorization.service';
import { HttpRequestService } from './service/http-request.service';
import { DialogComponent } from './dialog/dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { KeysPipe } from './pipe/keys-pipe';
import { MapToIterable } from './pipe/map-iterable-pipe';
import { CanActivateViaStudentUser } from './activator/can-activate-student';
import { CanActivateViaAdminUser } from './activator/can-activate-admin';
import { CanActivateViaTeacherUser } from './activator/can-activate-teacher';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    TableContentComponent,
    LoginComponent,
    RegistrationComponent,
    UnblockUserComponent,
    SubjectsComponet,
    HomeSubjectsComponet,
    CoursesSubjectsComponet,
    ParameterSubjectsComponent,
    ClassesStudentSubjectsComponet,
    ClassesStudentResultsSubjectsComponet,
    ClassesStudentResultsListSubjectsComponet,
    TeacherCoursesSubjectsComponet,
    EqualValidator,
    CheckUsernameValidator,
    CheckEmailValidator,
    DialogComponent,
    InputDebounceComponent,
    CreateCourseSubjectsComponet,
    UpdateCourseSubjectsComponet,
    CreateCourseDetailsSubjectsComponet,
    CreateTypeOfCourseDetailsSubjectsComponet,
    CreateCourseTeacherSubjectsComponet,
    ClassesStudentTeacherListSubjectsComponet,
    /*Pipe*/
    KeysPipe,
    MapToIterable
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    },
    CanActivateViaStudentUser,
    CanActivateViaAdminUser,
    CanActivateViaTeacherUser,
    AuthorizationService,
    HttpRequestService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }