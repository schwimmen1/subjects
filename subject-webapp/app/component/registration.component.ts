import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../class/user';
import { HttpRequestService } from '../service/http-request.service';

@Component({
  selector: 'registration',
  templateUrl: './html/registration.component.html',
  styleUrls: ['./css/registration.component.css']
  })
  export class RegistrationComponent implements OnInit {

    roles = ['ROLE_STUDENT', 'ROLE_TEACHER'];
    rolesNames = {'ROLE_STUDENT' : 'Student', 'ROLE_TEACHER' : 'Prowadzący'};
    message: string;
    registrationUser: User;

    constructor(private router: Router, private httpServise: HttpRequestService) {
        this.registrationUser = new User();
        this.registrationUser.authority = this.roles[0];
        this.registrationUser.username = '';
        this.registrationUser.password = '';
        this.registrationUser.secondPassword = '';
        this.registrationUser.firstname = '';
        this.registrationUser.lastname = '';
        this.registrationUser.email = '';
        this.registrationUser.ldap = false;
    }

    ngOnInit(): void {
        this.httpServise.get('common/parameter/registrationAllow')
        .then(result => {
            if(result === 0) {
                this.router.navigate(['/login']);
            }
        }).catch(e => {});
    }

    registration(): void {
        this.httpServise.post('registration', this.registrationUser)
            .then(result => {
                this.registrationUser.authority = this.roles[0];
                this.registrationUser.username = '';
                this.registrationUser.password = '';
                this.registrationUser.secondPassword = '';
                this.registrationUser.firstname = '';
                this.registrationUser.lastname = '';
                this.registrationUser.email = '';
                this.registrationUser.ldap = false;
                this.message = 'Rejestracja przebiegła pomyślnie. Na podanego maila został wysłany link aktywacyjny. Po aktywacji konta będzie możliwośc zalogowania się w aplikacji.';
            })
            .catch(e => {
                this.registrationUser.authority = this.roles[0];
                this.registrationUser.password = '';
                this.registrationUser.secondPassword = '';
                if(e === 403) {
                    this.message = 'Użytkownik o wybranym loginie lub mailu istnieje już w bazie.';
                } else {
                    this.message = 'Błąd wewnętrzny. Nie można dokończyć rejestracji. Spróbuj ponownie za chwilę.';
                }
            });  
    }

    checkPasswords(): boolean {
        return ((!this.registrationUser.password || !this.registrationUser.secondPassword)
                 || this.registrationUser.secondPassword === this.registrationUser.password);
    }

}