import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthorizationService } from '../service/authorization.service';
import { HttpRequestService } from '../service/http-request.service';
import { User } from '../class/user';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'login',
  templateUrl: './html/login.component.html',
  styleUrls: ['./css/login.component.css']
  })
  export class LoginComponent implements OnInit {
    
    user: User;
    error: string;
    inactiveAccount: boolean;
    accountTypes = [true, false];

    registrationAllow: boolean;
    loginDBAllow: boolean;

    ngOnInit(): void {
      this.httpService.get('common/parameter/onlyLdapLogin')
        .then(result => {
          this.loginDBAllow = result === 0 ? true : false;
          if(!this.loginDBAllow) {
            this.accountTypes = [true];
          }
        })
        .catch(e => {});
      this.httpService.get('common/parameter/registrationAllow')
      .then(result => this.registrationAllow = result === 0 ? false : true)
      .catch(e => {});
    }

    constructor(private authorizationService: AuthorizationService, private httpService: HttpRequestService, private router: Router) {
      this.user = new User();
      this.user.ldap = true;
      this.inactiveAccount = false;
    }

    logIn(): void {
      this.inactiveAccount = false;
      this.authorizationService.login(this.user.username, this.user.password, this.user.ldap).then(result => {
        this.clearPassword();
        if(result) {
          this.router.navigate(['/subjects/home']);
        } else {
          this.error = 'Nieprawidłowy login i/lub hasło. Spróbuj jeszcze raz.';
        }
      }).catch(e => {
        this.clearPassword();
        if(e === 401) {
          this.error = 'Nieprawidłowy login i/lub hasło. Spróbuj jeszcze raz.';
        } else if(e === 403) {
          this.inactiveAccount = true;
          this.error = 'Konto użytkownika jest nieaktywne. Aktywuj konto za pomocą maila, aby móc się zalogować.';
        } else {
          this.error = 'Błąd wewnętrzny. Skontaktuj się z administratorem. Bądź spróbuj zalogować się ponownie za kilka minut.';
        }
      });
    }

    sendActivationMail(): void {
      this.httpService.post('registration/sendActivationEmailAgain', {unblockUsername : this.user.username})
        .then(result => {
          this.inactiveAccount = false;
          this.error = 'Mail został wysłany ponownie.'
        });
    }

    private clearPassword(): void {
      this.user.password = '';
    }

  }
