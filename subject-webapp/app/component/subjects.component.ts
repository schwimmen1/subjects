import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'subjects',
  templateUrl: './html/subjects.component.html',
  styleUrls: ['./css/subjects.component.css']
  })
  export class SubjectsComponet implements OnInit {

    localStorage: string = 'subjects';
    fullImagePath: string = './assets/subjects.png';
    name: String;
    authority: string;
    startEnrolmentDay: Date;
    endEnrolmentDay: Date;

    constructor(private router: Router) {}

    ngOnInit(): void {
      if(!localStorage.getItem(this.localStorage)) {
        this.router.navigate(['/login']);
      } else {
        this.name = JSON.parse(localStorage.getItem(this.localStorage)).firstName + ' ' + JSON.parse(localStorage.getItem(this.localStorage)).lastName;
        this.startEnrolmentDay = new Date(JSON.parse(localStorage.getItem(this.localStorage)).startEnrolmentDay);
        this.endEnrolmentDay = new Date(JSON.parse(localStorage.getItem(this.localStorage)).endEnrolmentDay);
        this.authority = JSON.parse(localStorage.getItem(this.localStorage)).role;
      }
    }

    logout(): void {
      localStorage.removeItem(this.localStorage);
      this.router.navigate(['/login']);
    }

  }