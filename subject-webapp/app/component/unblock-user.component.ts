import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpRequestService } from '../service/http-request.service';

@Component({
  selector: 'unblockUser',
  templateUrl: './html/unblock-user.component.html',
  styleUrls: ['./css/unblock-user.component.css']
  })
  export class UnblockUserComponent implements OnInit {

    constructor(private route: ActivatedRoute, private httpService: HttpRequestService, private router: Router) {}

    ngOnInit(): void {
        this.route.params.subscribe((params: Params) => {
            let unblockToken = params['unblockToken'];
            this.httpService.post("registration/unblock", {unblockToken : unblockToken})
                .then(x => console.log('Account was unlocked.'))
                .catch(e => this.router.navigate(['/registration']))
          });
    }

  }