import { Component, Input } from '@angular/core';

import { CreateClassesDetails } from '../../class/createClassesDetails';


@Component({
  selector: 'create-course-details',
  templateUrl: '../html/create-course-details.component.html',
  styleUrls: ['../css/create-course.component.css']
  })
  export class CreateCourseDetailsSubjectsComponet { 

    @Input() classesDetails: CreateClassesDetails[];
    objects: any[];

    constructor() {
        this.classesDetails = [];
        this.objects = [{id: 'MONDAY', day: 'poniedziałek'},
                        {id: 'TUESDAY', day: 'wtorek'},
                        {id: 'WEDNESDAY', day: 'środa'},
                        {id: 'THURSDAY', day: 'czwartek'},
                        {id: 'FRIDAY', day: 'piątek'},
                        {id: 'SATURDAY', day: 'sobota'},
                        {id: 'SUNDAY', day: 'niedziela'}
        ];
    }

    addClassDetails(): void {
        let detail = new CreateClassesDetails();
        detail.day = this.objects[0].id;
        this.classesDetails.push(detail);
    }

    removeClassDetails(classes: CreateClassesDetails): void {
        if(this.classesDetails.length > 1) {
            const index: number = this.classesDetails.indexOf(classes);
            if (index !== -1) {
                this.classesDetails.splice(index, 1);
            }
        }
    }

    public restrictNumeric(event) {
        const pattern = /[a-zA-Z0-9-_ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
          event.preventDefault();
        }
       }

  }