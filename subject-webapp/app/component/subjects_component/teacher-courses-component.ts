import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { HttpRequestService } from '../../service/http-request.service';
import { CreateCourse } from '../../class/createCourse';
import 'rxjs/Rx' ;

@Component({
  selector: 'home',
  templateUrl: '../html/teacher-courses-component.html',
  styleUrls: ['../css/teacher-courses-component.css']
  })
  export class TeacherCoursesSubjectsComponet {

    courses: CreateCourse[];

    from: number;
    size: number;
    term: string;
    sizeCourse: number;
    pageNumber: number;
    pagesIndex: number[];

    minIndex: number;
    maxIndex: number;
    paginSize: number;

    showDialogConfirmation: boolean;
    showDialog: boolean;
    messageConfirmation: string;
    message: string;
    toRemoved: boolean;
    courseId: number;

    constructor(private httpService: HttpRequestService) {
      this.courses = [];
      this.from = 1;
      this.size = 10;
      this.term = '';
      this.sizeCourse = 0;
      this.pageNumber = 0;
      this.pagesIndex = [];
      
      this.minIndex = 1;
      this.maxIndex = 4;
      this.paginSize = 4;

      this.showDialogConfirmation = false;
      this.showDialog = false;
      this.messageConfirmation = '';
      this.message = '';
      this.courseId = 0;
    }

    ngOnInit(): void {
      this.httpService.authenticateGet(`teacher/classes?size=${this.size}&from=${this.from}&search=`)
        .then(result => {
          this.courses = result;
          this.httpService.authenticateGet('teacher/sizeAllClasses')
          .then(result => {
            this.sizeCourse = result.size;
            this.initSelected();
          })
          .catch(e => console.log(e));
        })
        .catch(e => console.log(e));
    }

    initSelected() {
      this.pageNumber = parseInt("" + (this.sizeCourse / this.size));
      if(this.sizeCourse % this.size != 0){
        this.pageNumber++;
      }
      if(this.pageNumber < this.paginSize) {
        this.paginSize = this.pageNumber;
      }
      this.pushPagesIndex();
    }

    setPage(page: number) {
      this.from = page;
      this.findCourses();
    }

    prevPage(): void {
      if(this.minIndex > 1) {
        this.maxIndex--;
        this.minIndex--;
        this.pushPagesIndex();
        if(this.from > this.maxIndex) {
          this.from = this.maxIndex;
          this.findCourses();
        }
      }
    }
    
    nextPage(): void {
      if(this.maxIndex < this.pageNumber) {
        this.maxIndex++;
        this.minIndex++;
        this.pushPagesIndex();
        if(this.from < this.minIndex) {
          this.from = this.minIndex;
          this.findCourses();
        }
      }
    }

    pushPagesIndex() {
      this.pagesIndex = [];
      for(var index = this.minIndex; index < this.minIndex + this.paginSize; index++) {
        this.pagesIndex.push(index);
      }
    }

    findCourses(): void {
      this.httpService.authenticateGet(`teacher/classes?size=${this.size}&from=${this.from}&search=${this.term}`)
      .then(result => {
        this.courses = result;
      })
      .catch(e => console.log(e));
    }

    filterByName(term: string): void {
      this.term = term;
      this.from = 1;
      this.findCourses();
      this.httpService.authenticateGet(`teacher/sizeClasses?size=${this.size}&from=${this.from}&search=${this.term}`)
        .then(result => {
          this.sizeCourse = result.size;
          this.minIndex = 1;
          this.maxIndex = 4;
          this.paginSize = 4;
          this.pageNumber = parseInt("" + (this.sizeCourse / this.size));
          if(this.sizeCourse % this.size != 0){
            this.pageNumber++;
          }
          if(this.pageNumber < this.paginSize) {
            this.paginSize = this.pageNumber;
          }
          if(this.maxIndex > this.pageNumber) {
            this.maxIndex = this.pageNumber;
          }
          this.pushPagesIndex();
        })
        .catch(e => console.log(e));
    }

    getCourseState(course: CreateCourse): string {
      const actualDate = new Date();
      let startDate = new Date(course.startEnrolmentDate);
      let endDate = new Date(course.endEnrolmentDate);
      if(actualDate > startDate && actualDate < endDate) {
        return 'Zapisy trwają';
      }
      if(startDate < endDate && endDate < actualDate) {
        return 'Zapisy zakończone';
      }
      if(startDate < endDate && startDate > actualDate) {
        return 'Zapisy nierozpoczęte';
      }
      return '';
    }

    canSeeList(course: CreateCourse): boolean {
      const actualDate = new Date();
      let startDate = new Date(course.startEnrolmentDate);
      let endDate = new Date(course.endEnrolmentDate);
      return startDate < endDate && endDate < actualDate;
    }

    showConfirmation(isDelete: boolean, courseId: number) {
      this.courseId = courseId;
      this.showDialog = true;
      if(isDelete) {
        this.toRemoved = true;
        this.message = "Czy na pewno chcesz usunąć ten przedmiot?";
      } else {
        this.toRemoved = false;
        this.message = "Czy na pewno chcesz wykasować wszystkie zapisane osoby?";
      }
    }

    deleteCourse(): void {
      this.httpService.authenticateDelete(`teacher/course/${this.courseId}`)
      .then(result => {
        this.filterByName(this.term);
        this.messageConfirmation = 'Akcja została wykonana poprawnie';
        this.showDialogConfirmation = true;
      })
      .catch(e => {
        this.messageConfirmation = 'Błąd. Spróbuj poniwnie za chwilę.';
        this.showDialogConfirmation = true;
      });
    }

    clearCourse(): void {
      this.httpService.authenticatePut(`teacher/course/${this.courseId}`, null)
        .then(result => {
          this.messageConfirmation = 'Akcja została wykonana poprawnie';
          this.showDialogConfirmation = true;
          this.courses.forEach(item => {
            if(item.id === this.courseId) {
              item.numberOfPeople = 0;
            }
          });
        })
        .catch(e => {
          this.messageConfirmation = 'Błąd. Spróbuj poniwnie za chwilę.';
          this.showDialogConfirmation = true;
        });
    }

    canEnrolment(course: CreateCourse): boolean {
      const actualDate = new Date();
      let startDate = new Date(course.startEnrolmentDate);
      let endDate = new Date(course.endEnrolmentDate);
      return actualDate > startDate && actualDate < endDate;
    }

    downloadfile(courseName: string, courseId: number){
      for (var i = 0; i < courseName.length; i++) {
        courseName = courseName.replace(' ', '_');
      }
      this.httpService.authenticateGet(`common/downloadListFile/${courseId}`)
        .then(data => this.downloadFileCsv(data, 'Lista_' + courseName + '.csv'))
        .catch(e => console.log(e));
    }

    downloadFileCsv(data: any, fileName: string) {
      let parsedResponse = data.text();
      if(parsedResponse === undefined || parsedResponse.length === 0) {
        parsedResponse = 'Brak osób, które zapisały się na zajecia lub brak stworzonych zajeć dla przedmiotu.';
      }
      let blob = new Blob([parsedResponse], { type: 'text/csv;charset=UTF-8;' });
      let url = window.URL.createObjectURL(blob);

      if(navigator.msSaveOrOpenBlob) {
          navigator.msSaveBlob(blob, fileName);
      } else {
          let a = document.createElement('a');
          a.href = url;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();        
          document.body.removeChild(a);
      }
      window.URL.revokeObjectURL(url);
    }

    public restrictNumeric(event) {
      const pattern = /[a-zA-Z0-9-_ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
     }

  }