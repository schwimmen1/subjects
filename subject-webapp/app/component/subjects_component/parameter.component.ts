import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { HttpRequestService } from '../../service/http-request.service';
import { Parameter } from '../../class/parameter';

@Component({
  selector: 'home',
  templateUrl: '../html/parameter.component.html',
  styleUrls: ['../css/parameter.component.css']
  })
  export class ParameterSubjectsComponent implements OnInit {

    tempVal: any[];
    parameters: Parameter[];
    showDialog: boolean;
    messageError: string;
    
    constructor(private router: Router, private httpService: HttpRequestService, private datePipe: DatePipe) {
        this.parameters = [];
        this.tempVal = [];
        this.showDialog = false;
        this.messageError = '';
    }

    ngOnInit(): void {
      this.httpService.authenticateGet('admin/parameters')
          .then(result => {
            result.map(item => this.parameters.push({
                id : item.id,
                name : item.name, 
                description : item.description, 
                value : item.type === 'D' ? new Date(item.value) : item.value, 
                type : item.type
              }))
          })
          .catch(e => this.router.navigate(['/login']));
    }

    saveTemp(id: number, value: any): void {
      this.tempVal.push({id : id, value : value});
    }

    returnChanges(id: number): void {
      let tempDate: Date;
      this.tempVal.forEach(item => {
        if(item.id === id) {
          tempDate = item.value;
        }
      })
      this.tempVal = this.tempVal.filter(item => item.id !== id);
      this.parameters.forEach(item => {
        if(item.id === id) {
          item.value = tempDate;
        }
      })
    }

    saveParameter(parameter: Parameter): void {
      this.httpService.authenticatePut('admin/parameters', {
        parameterName : parameter.name,  
        parameterValue : parameter.type === 'D' ? this.datePipe.transform(parameter.value, 'dd/MM/yyyy HH:mm:ss') : parameter.value, 
      })
        .then(result => {})
        .catch(e => {
          if(e.status === 401) {
            this.router.navigate(['/login']);
          }
          this.messageError = `Nie udało zapisać się parametru. Sprawdź poprawność dat.`;
          this.showDialog = true;
          this.returnChanges(parameter.id);
          }
        );
    }

  }