import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from '../../class/course';

import { HttpRequestService } from '../../service/http-request.service';

@Component({
  selector: 'courses',
  templateUrl: '../html/courses.component.html',
  styleUrls: ['../css/courses.component.css']
  })
  export class CoursesSubjectsComponet implements OnInit {

    courseToDelete: Course;
    showDialogSelection: boolean = false;
    showDialogError: boolean = false;
    errorMessage: string;

    /** Selected items */
    filteredItemsSelected : Course[];
    pagesSelected : number = 4;
    pageSizeSelected : number = 5;
    pageNumberSelected : number = 0;
    currentIndexSelected : number = 1;
    itemsSelected: Course[];
    pagesIndexSelected : Array<number>;
    pageStartSelected : number = 1;

    /** Unselected items */
    filteredItemsUnselected : Course[];
    pagesUnselected : number = 4;
    pageSizeUnselected : number = 5;
    pageNumberUnselected : number = 0;
    currentIndexUnselected : number = 1;
    itemsUnselected: Course[];
    pagesIndexUnselected : Array<number>;
    pageStartUnselected : number = 1;
    inputName : string = '';

    allUnselectedCourses: Course[];

      constructor(private router: Router, private httpService: HttpRequestService) {
        this.filteredItemsUnselected = [];
        this.filteredItemsSelected = [];
        this.allUnselectedCourses = [];
        this.errorMessage = '';
      }

      ngOnInit(): void {
        this.httpService.authenticateGet('student/getStudentCourses')
          .then(response => {
            this.filteredItemsUnselected = response['unselectedStudentCourses'];
            this.filteredItemsSelected = response['selectedStudentCourses'];
            this.filteredItemsUnselected.forEach(item => this.allUnselectedCourses.push({id : item.id, name : item.name, description : item.description}));
            this.init();
            this.initSelected();
          })
          .catch(e => {
            this.showDialogError = true;
          });
      }

      initSelected() {
        this.pagesSelected = 4;

        this.pageNumberSelected = parseInt("" + (this.filteredItemsSelected.length / this.pageSizeSelected));
        if(this.filteredItemsSelected.length % this.pageSizeSelected != 0){
           this.pageNumberSelected++;
        }
        if(this.pageNumberSelected < this.pagesSelected){
              this.pagesSelected = this.pageNumberSelected;
        }
        if(this.currentIndexSelected > this.pageNumberSelected) {
          this.currentIndexSelected = this.pageNumberSelected;
        }
        if(this.pageStartSelected + this.pagesSelected - 1 > this.pageNumberSelected) {
          this.pageStartSelected = this.pageNumberSelected -  this.pagesSelected + 1;
        }
        if(this.currentIndexSelected === 0 && this.filteredItemsSelected.length !== 0 ) {
          this.currentIndexSelected++;
        }
        this.refreshItemsSelected();
      }

      fillArraySelected(): any {
        var obj = new Array();
        for(var index = this.pageStartSelected; index < this.pageStartSelected + this.pagesSelected; index++) {
          obj.push(index);
        }
        return obj;
      }

      refreshItemsSelected() {
        this.itemsSelected = this.filteredItemsSelected.slice((this.currentIndexSelected - 1) * this.pageSizeSelected, (this.currentIndexSelected) * this.pageSizeSelected);
        this.pagesIndexSelected =  this.fillArraySelected();
      }

      prevPageSelected() {
        if(this.currentIndexSelected > 1){
          this.currentIndexSelected--;
        } 
        if(this.currentIndexSelected < this.pageStartSelected){
          this.pageStartSelected = this.currentIndexSelected;
        }
        this.refreshItemsSelected();
      }

      nextPageSelected() {
        if(this.currentIndexSelected < this.pageNumberSelected){
          this.currentIndexSelected++;
        }
        if(this.currentIndexSelected >= (this.pageStartSelected + this.pagesSelected)){
          this.pageStartSelected = this.currentIndexSelected - this.pagesSelected + 1;
        }
        this.refreshItemsSelected();
      }

      setPageSelected(index : number) {
        this.currentIndexSelected = index;
        this.refreshItemsSelected();
      }

    deleteCourse(): void {
        this.httpService.authenticateDelete(`student/studentCourses/${this.courseToDelete.id}`)
          .then(result => {
            this.filteredItemsSelected = this.filteredItemsSelected.filter(item => item.id !== this.courseToDelete.id);
            this.filteredItemsUnselected.push(this.courseToDelete);
            this.allUnselectedCourses.push({id : this.courseToDelete.id, name : this.courseToDelete.name, description : this.courseToDelete.description});
            this.init();
            this.initSelected();
            this.FilterByName();
          }).catch(e => {
            this.errorMessage = 'Nie można wypisać się z wybranego przedmiotu.';
            this.showDialogError = true;
          });
      }

      init() {
        this.pagesUnselected = 4;

        this.pageNumberUnselected = parseInt("" + (this.filteredItemsUnselected.length / this.pageSizeUnselected));
        if(this.filteredItemsUnselected.length % this.pageSizeUnselected != 0){
           this.pageNumberUnselected++;
        }
        if(this.pageNumberUnselected < this.pagesUnselected){
              this.pagesUnselected = this.pageNumberUnselected;
        }
        if(this.currentIndexUnselected > this.pageNumberUnselected) {
          this.currentIndexUnselected = this.pageNumberUnselected;
        }
        if(this.pageStartUnselected + this.pagesUnselected - 1 > this.pageNumberUnselected) {
          this.pageStartUnselected = this.pageNumberUnselected -  this.pagesUnselected + 1;
        }
        if(this.currentIndexUnselected === 0 && this.filteredItemsUnselected.length !== 0 ) {
          this.currentIndexUnselected++;
        }
        this.refreshItems();
      }

      FilterByName() {
        this.filteredItemsUnselected = [];
        if(this.inputName != "") {
          this.allUnselectedCourses.forEach(element => {
                  if(element.name.toUpperCase().indexOf(this.inputName.toUpperCase())>=0){
                    this.filteredItemsUnselected.push({id : element.id, name : element.name, description : element.description});
                  }
              });
         } else {
           this.allUnselectedCourses.forEach(element => this.filteredItemsUnselected.push({id : element.id, name : element.name, description : element.description}));
         }
        this.init();
      }

      fillArray(): any {
        var obj = new Array();
        for(var index = this.pageStartUnselected; index < this.pageStartUnselected + this.pagesUnselected; index++) {
          obj.push(index);
        }
        return obj;
      }

      refreshItems() {
        this.itemsUnselected = this.filteredItemsUnselected.slice((this.currentIndexUnselected - 1) * this.pageSizeUnselected, (this.currentIndexUnselected) * this.pageSizeUnselected);
        this.pagesIndexUnselected =  this.fillArray();
      }

      prevPage() {
        if(this.currentIndexUnselected > 1){
          this.currentIndexUnselected--;
        } 
        if(this.currentIndexUnselected < this.pageStartUnselected){
          this.pageStartUnselected = this.currentIndexUnselected;
        }
        this.refreshItems();
      }

      nextPage() {
        if(this.currentIndexUnselected < this.pageNumberUnselected){
          this.currentIndexUnselected++;
        }
        if(this.currentIndexUnselected >= (this.pageStartUnselected + this.pagesUnselected)){
          this.pageStartUnselected = this.currentIndexUnselected - this.pagesUnselected + 1;
        }
        this.refreshItems();
      }

      setPage(index : number) {
        this.currentIndexUnselected = index;
        this.refreshItems();
      }

      saveCourse(course: Course): void {
        this.httpService.authenticatePut(`student/studentCourses/${course.id}`, null)
          .then(result => {
            this.filteredItemsUnselected = this.filteredItemsUnselected.filter(item => item !== course);
            this.allUnselectedCourses = this.allUnselectedCourses.filter(index => index.id !== course.id);
            this.filteredItemsSelected.push(course);
            this.init();
            this.initSelected();
          })
          .catch(e => {
            this.errorMessage = 'Nie można zapisac się na wybrany przedmiot.';
            this.showDialogError = true;
          });
      }

  }
