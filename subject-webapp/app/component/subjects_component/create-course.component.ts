import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CreateUser } from '../../class/createUsers';
import { CreateCourse } from '../../class/createCourse';
import { TypeOfCourses } from '../../class/typeOfCourse';
import { CreateClasses } from '../../class/createClasses';
import { CreateClassesDetails } from '../../class/createClassesDetails';
import { HttpRequestService } from '../../service/http-request.service';

@Component({
  selector: 'create-course',
  templateUrl: '../html/create-course.component.html',
  styleUrls: ['../css/create-course.component.css']
  })
  export class CreateCourseSubjectsComponet implements OnInit {

    number: number;
    unModifiedUserId: number;
    createUser: CreateUser[];
    createCourse: CreateCourse;
    classes: Map<TypeOfCourses, CreateClasses[]>;
    showDialogSuccess:boolean;
    showDialogError: boolean;
    errorMessage: string;

    constructor(private router: Router, private httpService: HttpRequestService) {
      this.number = 0;
      this.unModifiedUserId = 0;
      this.createCourse = new CreateCourse();
      this.showDialogError = false;
      this.showDialogSuccess = false;
      this.errorMessage = 'Coś poszło nie tak. Spróbuj ponownie za chwilę.';
    }

    ngOnInit(): void {
      this.httpService.authenticateGet('common/typeOfCourses')
        .then(result => {
          this.classes = new Map<TypeOfCourses, CreateClasses[]>();
          for(var i = 0; i < result.length; i++) {
            this.classes.set(result[i], []);
          }
        }).catch(e => this.showDialogError = true);
      this.httpService.authenticateGet('common/loggedUser')
      .then(result => {
        this.createUser = [];
        this.createUser.push(result);
        this.unModifiedUserId = result.id;
      }).catch(e => this.showDialogError = true);
    }

    setNumber(number: number): void {
      this.number = number;
    }

    getClasses(key: any): any {
      return this.classes.get(key);
    }

    saveCourse(): void {
      let temp = {};
      Array.from(this.classes.keys()).forEach(item => temp[item.id] = this.classes.get(item));
      this.createCourse.classes = temp;
      let usersId = [];
      this.createUser.forEach(user => usersId.push(user.id))
      this.createCourse.teacherId = usersId;
      this.httpService.authenticatePut('teacher/course', this.createCourse)
        .then(result => this.showDialogSuccess = true)
        .catch(e => this.showDialogError = true);
    }

    validDetails(): boolean {
      let ok = true;
      if(this.classes) {
        Array.from(this.classes.values()).forEach(item => {
          item.forEach(item2 => {
            if(item2.maxGroupNumber) {} else {
              ok = false;
            } 
            item2.classesDetails.forEach(item3 => {
              if(item3.hourFrom && item3.hourTo) {} else {
                ok = false;
              }
            })
          })
        });
      }
      return ok;
    }

    createSucces(): void {
      this.router.navigate(['/subjects/teacherCourses']);
    }

    public restrictNumeric(event) {
      const pattern = /[a-zA-Z0-9-_ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
     }

    public restrictNumber(event) {
      const pattern = /[0-9]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
    }

  }