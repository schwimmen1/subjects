import { Component, ViewChild, ElementRef, HostListener, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequestService } from '../../service/http-request.service';
import { Course } from '../../class/course';

@Component({
  selector: 'student-classes',
  templateUrl: '../html/classes-student.component.html',
  styleUrls: ['../css/classes-student.component.css']
  })
  export class ClassesStudentSubjectsComponet implements OnInit {
  
    endEnrolmentDay: Date;
    errorMessage: string;
    showDialogError: boolean;

    course: any;
    courses: Course[];
    coursesToDisplay: Course[];
    currentIndexId: number;
    indexStart: number;
    indexEnd: number;
    pageNumberSelected: number;
    
    constructor(private router: Router, private httpService: HttpRequestService) {
      this.coursesToDisplay = [];
      this.courses = [];
      this.indexStart = 0;
      this.indexEnd = 2;
      this.errorMessage = '';
      this.showDialogError = false;
      this.endEnrolmentDay = new Date(JSON.parse(localStorage.getItem('subjects')).endEnrolmentDay);
    }

    ngOnInit(): void {
      this.httpService.authenticateGet('student/selectedCourses')
      .then(response => {
        this.courses = response;
        this.initSelected();
        if(this.courses && this.courses.length > 0) {
          this.httpService.authenticateGet(`student/studentClasses/${this.courses[0].id}`)
          .then(response => { 
            this.course = response;
          })
          .catch(e => {
            this.errorMessage = e.status + ': Coś poszło nie tak. Spróbuj ponownie później.';
            this.showDialogError = true;
          });
        }
      })
      .catch(e => {
        this.errorMessage = e.status + ': Coś poszło nie tak. Spróbuj ponownie później.';
        this.showDialogError = true;
      });
    }

    initSelected() {
      this.pageNumberSelected = this.courses.length;
      if(this.pageNumberSelected <= this.indexEnd){
        this.indexEnd = this.pageNumberSelected - 1;
      }
      if(this.pageNumberSelected !== 0) {
        this.currentIndexId = this.courses[0].id;
      }
      for(var index = this.indexStart; index <= this.indexEnd; index++) {
        this.coursesToDisplay.push(this.courses[index]);
      }
    }

    setPageSelected(id: number) {
      this.currentIndexId = id;
      this.findCourseDetails();
    }

    prevPageSelected(): void {
      if(this.indexStart - 1 >= 0) {
        this.indexStart -= 1;
        this.indexEnd -= 1;
        if(!this.fillCoursesToDisplay()) {
          this.currentIndexId = this.courses[this.indexEnd].id;
          this.findCourseDetails();
        }
      }
    }

    nextPageSelected(): void {
      if(this.indexEnd < this.pageNumberSelected - 1) {
        this.indexStart += 1;
        this.indexEnd += 1;
        if(!this.fillCoursesToDisplay()) {
          this.currentIndexId = this.courses[this.indexStart].id;
          this.findCourseDetails();
        }
      }
    }
    
    fillCoursesToDisplay(): boolean {
      let isVisible: boolean = false;
      this.coursesToDisplay = [];
      for(var index = this.indexStart; index <= this.indexEnd; index++) {
        this.coursesToDisplay.push(this.courses[index]);
        if(this.courses[index].id === this.currentIndexId) {
          isVisible = true;
        }
      }
      return isVisible;
    }

    findCourseDetails(): void {
      this.course = undefined;
      this.httpService.authenticateGet(`student/studentClasses/${this.currentIndexId}`)
      .then(response => {
        this.course = response;
      })
      .catch(e => {
        this.errorMessage = e.status + ': Coś poszło nie tak. Spróbuj ponownie później.';
        this.showDialogError = true;
      });
    }

    // @HostListener('window:resize', ['$event']) 
    // onResize(event) { 
    //  // this.resizeEvent$.next(event);
    //   this.resizeWorks();
    // }

    // private resizeWorks(): void {
    //  // console.log('gg');
    //  // console.log(this.paginCourse.nativeElement.getBoundingClientRect().width + "   :  " + this.main.nativeElement.getBoundingClientRect().width);
    // }

  }

 