import { Component, Input } from '@angular/core';

import { CreateClasses } from '../../class/createClasses';
import { CreateClassesDetails } from '../../class/createClassesDetails';

@Component({
  selector: 'create-type-of-course-details',
  templateUrl: '../html/create-type-of-course.componnet.html',
  styleUrls: ['../css/create-course.component.css']
  })
  export class CreateTypeOfCourseDetailsSubjectsComponet { 

    @Input() classes: CreateClasses[];
    @Input() typeOfCourse: string;

    addEmptyClasses(): void {
        const classes = new CreateClasses();
        const detail = new CreateClassesDetails();
        detail.day = 'MONDAY';
        classes.classesDetails = [detail];
        this.classes.push(classes);
    }

    removeClasses(classes: CreateClasses): void {
        const index: number = this.classes.indexOf(classes);
        if (index !== -1) {
            this.classes.splice(index, 1);
        }
    }

    public restrictNumeric(event) {
        const pattern = /[a-zA-Z0-9-_ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ.]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
          event.preventDefault();
        }
       }

    public restrictNumber(event) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

  }