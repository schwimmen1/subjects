import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router} from '@angular/router';
import { HttpRequestService } from '../../service/http-request.service';
import { TypeOfCourses } from '../../class/typeOfCourse';
import { Course } from '../../class/course';

@Component({
  selector: 'student-classes-results',
  templateUrl: '../html/classes-student-result-list.component.html',
  styleUrls: ['../css/classes-student.component.css']
  })
  export class ClassesStudentResultsListSubjectsComponet implements OnInit {
    
    errorMessage: string;
    showDialogError: boolean;
    courseId: number;
    course: Course;
    actualClasses: any[];

    typesOfCourses: TypeOfCourses[];
    typesOfCoursesToDisplay: TypeOfCourses[];
    currentIndexId: number;
    indexStart: number;
    indexEnd: number;
    pageNumberSelected: number;

    constructor(private httpService: HttpRequestService, private route: ActivatedRoute, private router: Router) {
      this.typesOfCourses = [];
      this.typesOfCoursesToDisplay = [];
      this.actualClasses = [];
      this.indexStart = 0;
      this.indexEnd = 2;
      this.errorMessage = '';
      this.showDialogError = false;
    
    }

    ngOnInit(): void {
      this.route.paramMap
        .switchMap((params: ParamMap) => {
          this.courseId = +params.get('id');
          return this.httpService.authenticateGet(`common/typeOfCourses/` + params.get('id'));
        })
        .subscribe(result => {
          this.typesOfCourses = result;
          this.initSelected();
          this.httpService.authenticateGet(`common/course/${this.courseId}`)
            .then(result => this.course = result)
            .catch(e => {
              this.errorMessage =  e.status + ': coś poszło nie tak. Spróbuj ponownie za chwilę.';
              this.showDialogError = true;
            });
        });
    }

    initSelected() {
      this.pageNumberSelected = this.typesOfCourses.length;
      if(this.pageNumberSelected <= this.indexEnd){
        this.indexEnd = this.pageNumberSelected - 1;
      }
      if(this.pageNumberSelected !== 0) {
        this.currentIndexId = this.typesOfCourses[0].id;
      }
      for(var index = this.indexStart; index <= this.indexEnd; index++) {
        this.typesOfCoursesToDisplay.push(this.typesOfCourses[index]);
      }
      this.findClassesDetais();
    }

    setPageSelected(id: number) {
      this.currentIndexId = id;
      this.findClassesDetais();
    }

    prevPageSelected(): void {
      if(this.indexStart - 1 >= 0) {
        this.indexStart -= 1;
        this.indexEnd -= 1;
        if(!this.fillCoursesToDisplay()) {
          this.currentIndexId = this.typesOfCourses[this.indexEnd].id;
          this.findClassesDetais();
        }
      }
    }

    nextPageSelected(): void {
      if(this.indexEnd < this.pageNumberSelected - 1) {
        this.indexStart += 1;
        this.indexEnd += 1;
        if(!this.fillCoursesToDisplay()) {
          this.currentIndexId = this.typesOfCourses[this.indexStart].id;
          this.findClassesDetais();
        }
      }
    }
    
    fillCoursesToDisplay(): boolean {
      let isVisible: boolean = false;
      this.typesOfCoursesToDisplay = [];
      for(var index = this.indexStart; index <= this.indexEnd; index++) {
        this.typesOfCoursesToDisplay.push(this.typesOfCourses[index]);
        if(this.typesOfCourses[index].id === this.currentIndexId) {
          isVisible = true;
        }
      }
      return isVisible;
    }

    findClassesDetais(): void {
      if(this.currentIndexId != undefined) {
        this.httpService.authenticateGet(`common/selectedClasses/${this.courseId}/${this.currentIndexId}`)
          .then(result => {
            this.actualClasses = result;
          })
          .catch(e => {
            this.errorMessage =  e.status + ': coś poszło nie tak. Spróbuj ponownie za chwilę.';
            this.showDialogError = true;
          });
      }
    }

    navigate(): void {
      this.router.navigate(['/subjects/classesResult']);
    }

  }
 