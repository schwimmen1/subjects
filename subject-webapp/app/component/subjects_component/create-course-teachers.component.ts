import { Component, Input, Pipe } from '@angular/core';

import { CreateUser } from '../../class/createUsers';
import { HttpRequestService } from '../../service/http-request.service';

@Component({
  selector: 'create-course-teacher',
  templateUrl: '../html/create-course-teachers.component.html',
  styleUrls: ['../css/create-course.component.css']
  })
  export class CreateCourseTeacherSubjectsComponet{ 

    @Input()
    public set user(val: CreateUser[]) {
      if(val !== undefined) {
        this.users = val;
        this.httpService.authenticateGet(`common/usersByRole/ROLE_TEACHER?size=${this.size}&from=${this.from}&search=${this.getExcludeUserId()}`)
        .then(result => {
          this.toAddUsers = result.result;
          this.sizeUsersToAdd = result.maxSizeList
          this.initSelected();
        })
        .catch(e => console.log(e));
      }
    }
    @Input() unModifiedUserId: number;
    users: CreateUser[];
    toAddUsers: CreateUser[];

    from: number;
    size: number;
    term: string;
    sizeUsersToAdd: number;
    pageNumber: number;
    pagesIndex: number[];

    minIndex: number;
    maxIndex: number;
    paginSize: number;

    constructor(private httpService: HttpRequestService) {
        this.users = [];
        this.toAddUsers = [];
        this.unModifiedUserId = 0;

        this.from = 1;
        this.size = 10;
        this.term = '';
        this.sizeUsersToAdd = 0;
        this.pageNumber = 0;
        this.pagesIndex = [];
        
        this.minIndex = 1;
        this.maxIndex = 4;
        this.paginSize = 4;
    }

    initSelected() {
      this.pageNumber = parseInt("" + (this.sizeUsersToAdd / this.size));
      if(this.sizeUsersToAdd % this.size != 0){
        this.pageNumber++;
      }
      if(this.pageNumber < this.paginSize) {
        this.paginSize = this.pageNumber;
      }
      this.pushPagesIndex();
    }

    setPage(page: number) {
      this.from = page;
      this.findUsers();
    }

    prevPage(): void {
      if(this.minIndex > 1) {
        this.maxIndex--;
        this.minIndex--;
        this.pushPagesIndex();
        if(this.from > this.maxIndex) {
          this.from = this.maxIndex;
          this.findUsers();
        }
      }
    }
    
    nextPage(): void {
      if(this.maxIndex < this.pageNumber) {
        this.maxIndex++;
        this.minIndex++;
        this.pushPagesIndex();
        if(this.from < this.minIndex) {
          this.from = this.minIndex;
          this.findUsers();
        }
      }
    }

    pushPagesIndex() {
      this.pagesIndex = [];
      for(var index = this.minIndex; index < this.minIndex + this.paginSize; index++) {
        this.pagesIndex.push(index);
      }
    }

    findUsers(): void {
      this.httpService.authenticateGet(`common/usersByRole/ROLE_TEACHER?size=${this.size}&from=${this.from}&search=${this.term}${this.getExcludeUserId()}`)
      .then(result => {
        this.toAddUsers = result.result;
      })
      .catch(e => console.log(e));
    }

    filterByName(term: string): void {
      this.term = term;
      this.from = 1;
      this.httpService.authenticateGet(`common/usersByRole/ROLE_TEACHER?size=${this.size}&from=${this.from}&search=${this.term}${this.getExcludeUserId()}`)
        .then(result => {
          this.toAddUsers = result.result;
          this.sizeUsersToAdd = result.maxSizeList;
          this.minIndex = 1;
          this.maxIndex = 4;
          this.paginSize = 4;
          this.pageNumber = parseInt("" + (this.sizeUsersToAdd / this.size));
          if(this.sizeUsersToAdd % this.size != 0){
            this.pageNumber++;
          }
          if(this.pageNumber < this.paginSize) {
            this.paginSize = this.pageNumber;
          }
          if(this.maxIndex > this.pageNumber) {
            this.maxIndex = this.pageNumber;
          }
          this.pushPagesIndex();
        })
        .catch(e => console.log(e));
    }

    getExcludeUserId(): string {
      if(this.users == undefined) {
        return '&exclude=';
      }
      let response = '';
      this.users.forEach(user => response += `&exclude=${user.id}`);
      return response == '' ? '&exclude=' : response;
    }

    addUser(user: CreateUser): void {
      this.users.push(user);
      const index2: number = this.toAddUsers.indexOf(user);
      if (index2 !== -1) {
          this.toAddUsers.splice(index2, 1);
      }
      this.filterByName(this.term);
    }

    removeUser(user: CreateUser): void {
      const index: number = this.users.indexOf(user);
      if (index !== -1) {
          this.users.splice(index, 1);
      }
      this.filterByName(this.term);
    }

    public restrictNumeric(event) {
      const pattern = /[a-zA-Z ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
     }

  }