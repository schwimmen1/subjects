import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { HttpRequestService } from '../../service/http-request.service';

@Component({
  selector: 'student-classes-results',
  templateUrl: '../html/classes-student-result.component.html',
  styleUrls: ['../css/classes-student-result.component.css']
  })
  export class ClassesStudentResultsSubjectsComponet implements OnInit {
    
    errorMessage: string;
    showDialogError: boolean;
    classes: any[];

    constructor(private httpService: HttpRequestService, private router: Router) {
      this.classes = [];
      this.errorMessage = '';
      this.showDialogError = false;
    }

    ngOnInit(): void {
      this.httpService.authenticateGet('student/selectedClasses')
        .then(result => {
          this.classes = result;
        })
        .catch(e => {
          this.errorMessage = e.status + ': coś poszło nie tak. Spróbuj ponownie za chwilę.';
          this.showDialogError = true;
        });
    }

    navigate(course: any[]): void {
      if(course.length > 0) {
        this.router.navigate([`/subjects/classesResult/${course[0].courseId}`]);
      }
    }

  }
 