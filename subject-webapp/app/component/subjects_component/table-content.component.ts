import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequestService } from '../../service/http-request.service';
import { Classes } from '../../class/classes';

@Component({
  selector: 'classes-table-content',
  templateUrl: '../html/table-content.component.html',
  styleUrls: ['../css/table-content.component.css']
  })
  export class TableContentComponent {

    @Input() classesList: Classes[];
    hasChoosen: boolean;

    constructor(private httpService: HttpRequestService, private router: Router) {
      this.hasChoosen = false;
    }

    check(values: Classes[]): Classes[] {
      values.forEach(element => {
        if(element.chosen) {
          this.hasChoosen = true;
        }
      });
      return values;
    }
    
    chooseClasses(classes: Classes): void {
      this.httpService.authenticatePutEmpty(`student/classes/${classes.id}`)
        .then(response => {
          this.hasChoosen = true;
          classes.chosen = true;
          classes.actualGroupNumber += 1;
        })
        .catch(e => this.router.navigate(['/login']))
    }

    deleteClasses(classes: Classes): void {
      this.httpService.authenticateDelete(`student/classes/${classes.id}`)
      .then(response => {
        this.hasChoosen = false;
        classes.chosen = false;
        classes.actualGroupNumber -= 1;
      })
      .catch(e => this.router.navigate(['/login']))
    }

  }

 