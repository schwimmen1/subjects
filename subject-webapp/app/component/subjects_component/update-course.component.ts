import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { CreateUser } from '../../class/createUsers';
import { CreateCourse } from '../../class/createCourse';
import { TypeOfCourses } from '../../class/typeOfCourse';
import { CreateClasses } from '../../class/createClasses';
import { CreateClassesDetails } from '../../class/createClassesDetails';
import { HttpRequestService } from '../../service/http-request.service';

@Component({
  selector: 'update-course',
  templateUrl: '../html/update-course.component.html',
  styleUrls: ['../css/create-course.component.css']
  })
  export class UpdateCourseSubjectsComponet implements OnInit {

    courseId: number;
    number: number;
    unModifiedUserId: number;
    createUser: CreateUser[];
    createCourse: CreateCourse;
    classes: Map<TypeOfCourses, CreateClasses[]>;
    showDialogSuccess:boolean;
    showDialogError: boolean;
    errorMessage: string;

    days = {
      'poniedziałek' : 'MONDAY',
      'wtorek' : 'TUESDAY',
      'środa' : 'WEDNESDAY',
      'czwartek' : 'THURSDAY',
      'piątek' : 'FRIDAY',
      'sobota' : 'SATURDAY',
      'niedziela' : 'SUNDAY',
    }

    constructor(private router: Router, private httpService: HttpRequestService, private route: ActivatedRoute) {
      this.number = 0;
      this.unModifiedUserId = 0;
      this.createCourse = new CreateCourse();
      this.showDialogError = false;
      this.showDialogSuccess = false;
      this.errorMessage = 'Coś poszło nie tak. Spróbuj ponownie za chwilę.';
    }

    ngOnInit(): void {
      this.route.paramMap
      .switchMap((params: ParamMap) => {
        this.courseId = +params.get('id');
        return this.httpService.authenticateGet(`teacher/teacherClasses/` + params.get('id'));
      })
       .subscribe(result => {
          this.createCourse = new CreateCourse();
          this.createCourse.id = result.id;
          this.createCourse.name = result.name;
          this.createCourse.description = result.description;
          this.createCourse.startEnrolmentDate = new Date(result.startEnrolmentDate);
          this.createCourse.endEnrolmentDate = new Date(result.endEnrolmentDate);
          this.httpService.authenticateGet('teacher/teachers/' + this.courseId)
            .then(result => {
              this.createUser = result;
          }).catch(e => this.showDialogError = true);
          this.httpService.authenticateGet('common/loggedUser')
            .then(result => {
              this.unModifiedUserId = result.id;
          }).catch(e => this.showDialogError = true);
          this.httpService.authenticateGet('common/typeOfCourses')
            .then(types => {
              this.classes = new Map<TypeOfCourses, CreateClasses[]>();
              for(var i = 0; i < types.length; i++) {
                const classes = [];
                if(result.classesWithTypes[types[i].name] != undefined) {
                  result.classesWithTypes[types[i].name].forEach(item => {
                    const oldClasses = new CreateClasses();
                    oldClasses.id = item.id;
                    oldClasses.maxGroupNumber = item.maxGroupNumber;
                    oldClasses.lecturer = item.lecturer;
                    oldClasses.description = item.description;
                    const detailsList: CreateClassesDetails[] = [];
                    oldClasses.classesDetails = item.classesDetails.forEach(details => {
                      const oldDetails = new CreateClassesDetails();
                      oldDetails.id = details.id;
                      oldDetails.day = this.days[details.day];
                      oldDetails.hourFrom = details.hoursFrom;
                      oldDetails.hourTo = details.hoursTo;
                      oldDetails.building = details.building;
                      oldDetails.roomNumber = details.roomNumber;
                      detailsList.push(oldDetails);
                    });
                    oldClasses.classesDetails = detailsList;
                    classes.push(oldClasses);
                  })
                }
                this.classes.set(types[i], classes);
              }
          }).catch(e => this.showDialogError = true);
        });
      
      
    }

    setNumber(number: number): void {
      this.number = number;
    }

    getClasses(key: any): any {
      return this.classes.get(key);
    }

    saveCourse(): void {
      let temp = {};
      Array.from(this.classes.keys()).forEach(item => temp[item.id] = this.classes.get(item));
      this.createCourse.classes = temp;
      let usersId = [];
      this.createUser.forEach(user => usersId.push(user.id))
      this.createCourse.teacherId = usersId;
      this.httpService.authenticatePost('teacher/course', this.createCourse)
        .then(result => this.showDialogSuccess = true)
        .catch(e => this.showDialogError = true);
    }

    validDetails(): boolean {
      let ok = true;
      if(this.classes) {
        Array.from(this.classes.values()).forEach(item => {
          item.forEach(item2 => {
            if(item2.maxGroupNumber) {} else {
              ok = false;
            } 
            item2.classesDetails.forEach(item3 => {
              if(item3.hourFrom && item3.hourTo) {} else {
                ok = false;
              }
            })
          })
        });
      }
      return ok;
    }

    createSucces(): void {
      this.router.navigate(['/subjects/teacherCourses']);
    }

    public restrictNumeric(event) {
      const pattern = /[a-zA-Z0-9-_ ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
     }

    public restrictNumber(event) {
      const pattern = /[0-9]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
    }

  }