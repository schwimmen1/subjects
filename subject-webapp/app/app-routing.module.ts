import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login.component';
import { RegistrationComponent } from './component/registration.component';
import { UnblockUserComponent } from './component/unblock-user.component';
import { SubjectsComponet } from './component/subjects.component';
import { HomeSubjectsComponet } from './component/subjects_component/home.component';
import { CoursesSubjectsComponet } from './component/subjects_component/courses.component';
import { ParameterSubjectsComponent } from './component/subjects_component/parameter.component';
import { ClassesStudentSubjectsComponet } from './component/subjects_component/classes-student.component';
import { ClassesStudentResultsSubjectsComponet } from './component/subjects_component/classes-student-result.component';
import { ClassesStudentResultsListSubjectsComponet } from './component/subjects_component/classes-student-result-list.component';
import { TeacherCoursesSubjectsComponet } from './component/subjects_component/teacher-courses-component';
import { CreateCourseSubjectsComponet } from './component/subjects_component/create-course.component';
import { UpdateCourseSubjectsComponet } from './component/subjects_component/update-course.component';
import { ClassesStudentTeacherListSubjectsComponet } from './component/subjects_component/classes-student-list.component';

import { CanActivateViaStudentUser } from './activator/can-activate-student';
import { CanActivateViaAdminUser } from './activator/can-activate-admin';
import { CanActivateViaTeacherUser } from './activator/can-activate-teacher';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'registration', component: RegistrationComponent},
  { path: 'registration/:unblockToken', component: UnblockUserComponent},
  { path: 'subjects', component: SubjectsComponet, children: [
    { path: 'home', component: HomeSubjectsComponet },
    /* STUDENT */
    { path: 'courses', component: CoursesSubjectsComponet, canActivate: [CanActivateViaStudentUser]},
    { path: 'classes', component: ClassesStudentSubjectsComponet, canActivate: [CanActivateViaStudentUser] },
    { path: 'classesResult/:id', component: ClassesStudentResultsListSubjectsComponet, canActivate: [CanActivateViaStudentUser] },
    { path: 'classesResult', component: ClassesStudentResultsSubjectsComponet, canActivate: [CanActivateViaStudentUser] },
    /* ADMIN*/
    { path: 'parameters', component: ParameterSubjectsComponent, canActivate: [CanActivateViaAdminUser] },
    /* TEACHER */
    { path: 'teacherCourses', component: TeacherCoursesSubjectsComponet, canActivate: [CanActivateViaTeacherUser] },
    { path: 'teacherCourses/create', component: CreateCourseSubjectsComponet, canActivate: [CanActivateViaTeacherUser] },  
    { path: 'teacherCourses/update/:id', component: UpdateCourseSubjectsComponet, canActivate: [CanActivateViaTeacherUser] },  
    { path: 'teacherCourses/:id', component: ClassesStudentTeacherListSubjectsComponet, canActivate: [CanActivateViaTeacherUser] }
  ]},
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}