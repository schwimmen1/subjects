import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mapToIterable'
  })
  export class MapToIterable {
    transform(dict: Map<any, any>): any {
        var a = [];
        for(let key of Array.from(dict.keys()) ) {
            a.push({key: key, val: dict.get(key)});
        }
        return a;
    }
  }