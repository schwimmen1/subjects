package com.schwimmen.subjects.service;

import com.schwimmen.subjects.service.mappers.PersonContextMapper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class LdapServiceTest {

    @Mock
    private LdapTemplate ldapTemplate;
    @Mock
    private PersonContextMapper personContextMapper;
    @InjectMocks
    private LdapServiceImpl ldapService;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void authenticateLdapUserTrueTest() {
        when(ldapTemplate.authenticate(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(true);
        boolean b = ldapService.authenticateLdapUser("user", "password");
        assertTrue(b);
    }

    @Test
    public void authenticateLdapUserFalseTest() {
        when(ldapTemplate.authenticate(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(false);
        boolean b = ldapService.authenticateLdapUser("user", "password");
        assertFalse(b);
    }

    @Test
    public void getLdapUserAttributeTest() {
        when(ldapTemplate.searchForObject(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(new HashMap<>());
        Map<String, String> attribute = ldapService.getLdapUserAttribute("user");
        assertNotNull(attribute);
    }

}
