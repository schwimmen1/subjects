package com.schwimmen.subjects.service;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.dao.ParameterDao;
import com.schwimmen.subjects.entity.Parameter;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class ParameterServiceTest {

    @Mock
    private ParameterDao parameterDao;
    @InjectMocks
    private ParameterServiceImpl parameterService;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private Parameter parameter;

    @Before
    public void before() {
        parameter = new Parameter();
    }

    @Test
    public void getAllParametersEmptyListTest() {
        when(parameterDao.getAllParameters()).thenReturn(new ArrayList<>());
        List<Parameter> allParameters = parameterService.getAllParameters();
        assertNotNull(allParameters);
        assertTrue(allParameters.isEmpty());
    }

    @Test
    public void getAllParametersTest() {
        when(parameterDao.getAllParameters()).thenReturn(Arrays.asList(parameter));
        List<Parameter> allParameters = parameterService.getAllParameters();
        assertNotNull(allParameters);
        assertTrue(allParameters.size() == 1);
    }

    @Test
    public void saveParameterNotExistsInDbTest() throws ApplicationException {
        when(parameterDao.getParameter(Parameters.REGISTRATION_AVAILABLE)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.PARAMETER_NAME_THIS_NAME_DOES_NOT_EXIST.getCause());
        parameterService.saveParameter(Parameters.REGISTRATION_AVAILABLE.name(), "0");
    }

    @Test
    public void saveParameterWrongNameTest() throws ApplicationException {
        when(parameterDao.getParameter(Parameters.REGISTRATION_AVAILABLE)).thenReturn(parameter);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.PARAMETER_NAME_THIS_NAME_DOES_NOT_EXIST.getCause());
        parameterService.saveParameter("wrong name", "0");
    }

    @Test
    public void saveParameterTest() throws ApplicationException {
        when(parameterDao.getParameter(Parameters.REGISTRATION_AVAILABLE)).thenReturn(parameter);
        parameterService.saveParameter(Parameters.REGISTRATION_AVAILABLE.name(), "0");
    }

    @Test
    public void getDateWrongPatternDateTest() throws ApplicationException {
        String value = "test";
        when(parameterDao.getParameterValue(Parameters.BEGIN_ENROLMENT_DAY)).thenReturn(value);
        Date date = parameterService.getDate(Parameters.BEGIN_ENROLMENT_DAY);
        assertNull(date);
    }

    @Test
    public void getDateTest() throws ApplicationException {
        String value = "10/10/1995 10:10:10";
        when(parameterDao.getParameterValue(Parameters.BEGIN_ENROLMENT_DAY)).thenReturn(value);
        Date date = parameterService.getDate(Parameters.BEGIN_ENROLMENT_DAY);
        assertNotNull(date);
        assertTrue(date instanceof Date);
    }

    @Test
    public void castValueToObjectWrongParameterNameTest() throws ApplicationException {
        String value = "test";
        parameter.setName("WrongName");
        parameter.setValue(value);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.PARAMETER_NAME_THIS_NAME_DOES_NOT_EXIST.getCause());
        parameterService.castValueToObject(parameter);
    }

    @Test
    public void castValueToObjectWrongPatternDateTest() throws ApplicationException {
        String value = "10/10 10:10:10";
        parameter.setName(Parameters.BEGIN_ENROLMENT_DAY.name());
        parameter.setValue(value);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.WRONG_PATTERN_DATE.getCause());
        parameterService.castValueToObject(parameter);
    }

    @Test
    public void castValueToObjectCastDateTest() throws ApplicationException {
        String value = "10/10/1995 10:10:10";
        parameter.setName(Parameters.BEGIN_ENROLMENT_DAY.name());
        parameter.setValue(value);
        Object object = parameterService.castValueToObject(parameter);
        assertNotNull(object);
    }

    @Test
    public void castValueToObjectTest() throws ApplicationException {
        String value = "test";
        parameter.setName(Parameters.REGISTRATION_AVAILABLE.name());
        parameter.setValue(value);
        Object object = parameterService.castValueToObject(parameter);
        assertNotNull(object);
        assertEquals(object, value);
    }

}
