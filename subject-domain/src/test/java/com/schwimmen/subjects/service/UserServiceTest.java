package com.schwimmen.subjects.service;

import com.schwimmen.subjects.dao.ApplicationUserDao;
import com.schwimmen.subjects.dao.AuthorityDao;
import com.schwimmen.subjects.dao.UserDao;
import com.schwimmen.subjects.entity.security.ApplicationUser;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @Mock
    private ApplicationUserDao applicationUserDao;
    @Mock
    private UserDao userDao;
    @Mock
    private AuthorityDao authorityDao;
    @InjectMocks
    private UserServiceImpl userService;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private ApplicationUser applicationUser;
    private User user;

    @Before
    public void before() {
        applicationUser = new ApplicationUser();
        user = new User();
    }

    @Test
    public void findUserByUsernameNullTest() {
        when(applicationUserDao.findUserByUsername("")).thenReturn(null);
        ApplicationUser userByUsername = userService.findUserByUsername(null);
        assertNull(userByUsername);
    }

    @Test
    public void findUserByUsernameBlankTest() {
        when(applicationUserDao.findUserByUsername("")).thenReturn(null);
        ApplicationUser userByUsername = userService.findUserByUsername("");
        assertNull(userByUsername);
    }

    @Test
    public void findUserByUsernameDifferentLetterTest() {
        when(applicationUserDao.findUserByUsername("test")).thenReturn(applicationUser);
        ApplicationUser userByUsername = userService.findUserByUsername("TEST");
        assertNotNull(userByUsername);
        assertEquals(userByUsername, applicationUser);
    }

    @Test
    public void findUserByUsernameTest() {
        when(applicationUserDao.findUserByUsername("test")).thenReturn(applicationUser);
        ApplicationUser userByUsername = userService.findUserByUsername("test");
        assertNotNull(userByUsername);
        assertEquals(userByUsername, applicationUser);
    }

    @Test
    public void findUserByEmailNullTest() {
        when(applicationUserDao.findUserByEmail("")).thenReturn(null);
        ApplicationUser userByUsername = userService.findUserByEmail("");
        assertNull(userByUsername);
    }

    @Test
    public void findUserByEmailEmptyTest() {
        when(applicationUserDao.findUserByEmail("")).thenReturn(null);
        ApplicationUser userByUsername = userService.findUserByEmail("");
        assertNull(userByUsername);
    }

    @Test
    public void findUserByEmailTest() {
        when(applicationUserDao.findUserByEmail("ad@op.pl")).thenReturn(applicationUser);
        ApplicationUser userByUsername = userService.findUserByEmail("ad@op.pl");
        assertNotNull(userByUsername);
        assertEquals(userByUsername, applicationUser);
    }

    @Test
    public void unblockUserNullTest() {
        String token = "unblockToken";
        userService.unblockUser(null, token);
    }

    @Test
    public void unblockUserEmptyTest() {
        String token = "unblockToken";
        userService.unblockUser("", token);
    }

    @Test
    public void unblockUserTest() {
        String token = "unblockToken";
        userService.unblockUser("test", token);
    }


    @Test
    public void findUserByUsernameNullLDAPTest() {
        when(userDao.findUserByUsername("", true)).thenReturn(null);
        User userByUsername = userService.findUserByUsername(null, true);
        assertNull(userByUsername);
    }

    @Test
    public void findUserByUsernameBlankLDAPTest() {
        when(userDao.findUserByUsername("", true)).thenReturn(null);
        User userByUsername = userService.findUserByUsername("", true);
        assertNull(userByUsername);
    }

    @Test
    public void findUserByUsernameDifferentLetterLDAPTest() {
        when(userDao.findUserByUsername("test", true)).thenReturn(user);
        User userByUsername = userService.findUserByUsername("TeSt", true);
        assertNotNull(userByUsername);
        assertEquals(userByUsername, user);
    }

    @Test
    public void findUserByUsernameLDAPTest() {
        when(userDao.findUserByUsername("test", true)).thenReturn(user);
        User userByUsername = userService.findUserByUsername("test", true);
        assertNotNull(userByUsername);
        assertEquals(userByUsername, user);
    }

    @Test
    public void findUserByRoleEmptyTest() {
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        when(userDao.findUserByRole(name)).thenReturn(new ArrayList<>());
        List<User> userByRole = userService.findUserByRole(name);
        assertNotNull(userByRole);
        assertTrue(userByRole.isEmpty());
    }

    @Test
    public void findUserByRoleListNotEmptyTest() {
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        when(userDao.findUserByRole(name)).thenReturn(Arrays.asList(user));
        List<User> userByRole = userService.findUserByRole(name);
        assertNotNull(userByRole);
        assertTrue(userByRole.size() == 1);
    }

    @Test
    public void findUserByRoleListUserNotEnabledTest() {
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        applicationUser.setEnabled(false);
        when(userDao.findUserByRole(name)).thenReturn(Arrays.asList(applicationUser));
        List<User> userByRole = userService.findUserByRole(name);
        assertNotNull(userByRole);
        assertTrue(userByRole.isEmpty());
    }

    @Test
    public void findUserByRoleEmptyExcludeTest() {
        List<Long> excludeUserId = Arrays.asList(10L, 2L);
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        when(userDao.findUserByRole(name, excludeUserId)).thenReturn(new ArrayList<>());
        List<User> userByRole = userService.findUserByRole(name, excludeUserId);
        assertNotNull(userByRole);
        assertTrue(userByRole.isEmpty());
    }

    @Test
    public void findUserByRoleListNotEmptyExcludeTest() {
        List<Long> excludeUserId = Arrays.asList(10L, 2L);
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        when(userDao.findUserByRole(name, excludeUserId)).thenReturn(Arrays.asList(user));
        List<User> userByRole = userService.findUserByRole(name, excludeUserId);
        assertNotNull(userByRole);
        assertTrue(userByRole.size() == 1);
    }

    @Test
    public void findUserByRoleListUserNotEnabledExcludeTest() {
        List<Long> excludeUserId = Arrays.asList(10L, 2L);
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        applicationUser.setEnabled(false);
        when(userDao.findUserByRole(name, excludeUserId)).thenReturn(Arrays.asList(applicationUser));
        List<User> userByRole = userService.findUserByRole(name, excludeUserId);
        assertNotNull(userByRole);
        assertTrue(userByRole.isEmpty());
    }


    @Test
    public void findUserByRoleAndIdEmptyTest() {
        List<Long> usersId = Arrays.asList(10L, 2L);
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        when(userDao.findUserByRoleAndId(name, usersId)).thenReturn(new ArrayList<>());
        List<User> userByRole = userService.findUserByRoleAndId(name, usersId);
        assertNotNull(userByRole);
        assertTrue(userByRole.isEmpty());
    }

    @Test
    public void findUserByRoleAndIdListNotEmptyTest() {
        List<Long> usersId = Arrays.asList(10L, 2L);
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        when(userDao.findUserByRoleAndId(name, usersId)).thenReturn(Arrays.asList(user));
        List<User> userByRole = userService.findUserByRoleAndId(name, usersId);
        assertNotNull(userByRole);
        assertTrue(userByRole.size() == 1);
    }

    @Test
    public void findUserByRoleAndIdListUserNotEnabledTest() {
        List<Long> usersId = Arrays.asList(10L, 2L);
        AuthorityName name = AuthorityName.ROLE_TEACHER;
        applicationUser.setEnabled(false);
        when(userDao.findUserByRoleAndId(name, usersId)).thenReturn(Arrays.asList(applicationUser));
        List<User> userByRole = userService.findUserByRoleAndId(name, usersId);
        assertNotNull(userByRole);
        assertTrue(userByRole.isEmpty());
    }

    @Test
    public void checkIfUserIsSavedNullTest() {
        user.setLdap(true);
        when(userDao.findUserByUsername("")).thenReturn(new ArrayList<>());
        boolean bool = userService.checkIfUserIsSaved(null, true);
        assertFalse(bool);
    }

    @Test
    public void checkIfUserIsSavedEmptyTest() {
        user.setLdap(true);
        when(userDao.findUserByUsername("")).thenReturn(new ArrayList<>());
        boolean bool = userService.checkIfUserIsSaved("", true);
        assertFalse(bool);
    }

    @Test
    public void checkIfUserIsSavedDifferentLetterTest() {
        user.setLdap(true);
        when(userDao.findUserByUsername("test")).thenReturn(Arrays.asList(user));
        boolean bool = userService.checkIfUserIsSaved("tESt", true);
        assertTrue(bool);
    }

    @Test
    public void checkIfUserIsSavedTest() {
        user.setLdap(true);
        when(userDao.findUserByUsername("test")).thenReturn(Arrays.asList(user));
        boolean bool = userService.checkIfUserIsSaved("test", true);
        assertTrue(bool);
    }

    @Test
    public void checkIfUserIsSavedUserIsNotLDAPTest() {
        user.setLdap(false);
        when(userDao.findUserByUsername("test")).thenReturn(Arrays.asList(user));
        boolean bool = userService.checkIfUserIsSaved("test", true);
        assertFalse(bool);
    }

    @Test
    public void createUserApplicationLdapUserTest() throws ApplicationException {
        applicationUser.setLdap(true);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.WRONG_TYPE_USER_CLASS.getCause());
        userService.createUser(applicationUser);
    }

    @Test
    public void createUserNotLdapUserTest() throws ApplicationException {
        user.setLdap(false);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.WRONG_TYPE_USER_CLASS.getCause());
        userService.createUser(user);
    }

    @Test
    public void createUserUsernameExistTest() throws ApplicationException {
        String email = "op@op.pl";
        user.setUsername("test");
        user.setEmail(email);
        user.setLdap(true);
        when(userDao.findUserByUsername("test")).thenReturn(Arrays.asList(user));
        when(applicationUserDao.findUserByUsername(email)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.USER_WITH_USERNAME_EXISTS.getCause());
        userService.createUser(user);
    }

    @Test
    public void createUserEmailExistTest() throws ApplicationException {
        String email = "op@op.pl";
        user.setUsername("test");
        user.setEmail(email);
        user.setLdap(true);
        when(userDao.findUserByUsername("test")).thenReturn(new ArrayList<>());
        when(applicationUserDao.findUserByEmail(email)).thenReturn(applicationUser);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.USER_WITH_EMAIL_EXISTS.getCause());
        userService.createUser(user);
    }

    @Test
    public void createUserSuccessTest() throws ApplicationException {
        String email = "op@op.pl";
        user.setLdap(true);
        when(userDao.findUserByUsername("test")).thenReturn(new ArrayList<>());
        when(applicationUserDao.findUserByUsername(email)).thenReturn(null);
       userService.createUser(user);
    }

}
