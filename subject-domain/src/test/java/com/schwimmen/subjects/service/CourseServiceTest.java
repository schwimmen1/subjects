package com.schwimmen.subjects.service;

import com.schwimmen.subjects.dao.CourseDao;
import com.schwimmen.subjects.dao.CourseEnrolmentDao;
import com.schwimmen.subjects.dao.TypeOfCourseDao;
import com.schwimmen.subjects.entity.*;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class CourseServiceTest {

    @Mock
    private CourseDao courseDao;
    @Mock
    private UserService userService;
    @Mock
    private ClassesServiceImpl classesService;
    @Mock
    private CourseEnrolmentDao courseEnrolmentDao;
    @Mock
    private TypeOfCourseDao typeOfCourseDao;
    @InjectMocks
    private CourseServiceImpl courseService;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private Course course;
    private Classes classes;
    private User user;
    private ClassesDetails classesDetails;

    @Before
    public void before() {
        course = new Course();
        classes = new Classes();
        classesDetails = new ClassesDetails();
        user = new User();
    }

    @Test
    public void clearStudentCourseNotExistTest() throws ApplicationException {
        Long courseId = 1L;
        when(courseDao.getCourse(courseId)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DOES_NOT_EXIST.getCause());
        courseService.clearStudentCourse(courseId);
    }

    @Test
    public void clearStudentCourseClearStudentUserTest() throws ApplicationException {
        Long courseId = 1L;
        course.setClasses(new ArrayList<>());
        course.setStudentUsers(new ArrayList<>());
        course.getStudentUsers().add(user);
        when(courseDao.getCourse(courseId)).thenReturn(course);
        courseService.clearStudentCourse(courseId);
        assertNotNull(course.getStudentUsers());
        assertTrue(course.getStudentUsers().isEmpty());
    }

    @Test
    public void clearStudentCourseClearUserTest() throws ApplicationException {
        Long courseId = 1L;
        course.setClasses(new ArrayList<>());
        classes.setUsers(new ArrayList<>());
        classes.getUsers().add(user);
        course.getClasses().add(classes);
        course.setStudentUsers(new ArrayList<>());
        when(courseDao.getCourse(courseId)).thenReturn(course);
        courseService.clearStudentCourse(courseId);
        assertNotNull(course.getClasses());
        course.getClasses().forEach( item -> {
                assertNotNull(item.getUsers());
                assertTrue(item.getUsers().isEmpty());
            }
        );
    }

    @Test
    public void clearStudentCourseEmptyListTest() throws ApplicationException {
        Long courseId = 1L;
        course.setClasses(new ArrayList<>());
        course.setStudentUsers(new ArrayList<>());
        when(courseDao.getCourse(courseId)).thenReturn(course);
        courseService.clearStudentCourse(courseId);
    }

    @Test
    public void removeCourseNotExistTest() throws ApplicationException {
        Long courseId = 2L;
        when(courseDao.getCourse(courseId)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DOES_NOT_EXIST.getCause());
        courseService.removeCourse(courseId);
    }

    @Test
    public void removeCourseEmptyClassesDetailsTest() throws ApplicationException {
        Long courseId = 2L;
        course.setClasses(new ArrayList<>());
        classes.setClassesDetails(new ArrayList<>());
        course.getClasses().add(classes);
        course.setStudentUsers(new ArrayList<>());
        when(courseDao.getCourse(courseId)).thenReturn(course);
        courseService.removeCourse(courseId);
        assertNotNull(course.getClasses());
        course.getClasses().forEach( item -> {
                    assertNotNull(item.getUsers());
                    assertTrue(item.getUsers().isEmpty());
                }
        );
    }

    @Test
    public void removeCourseClaasesDetailsTest() throws ApplicationException {
        Long courseId = 2L;
        course.setClasses(new ArrayList<>());
        classes.setClassesDetails(new ArrayList<>());
        classes.getClassesDetails().add(classesDetails);
        course.getClasses().add(classes);
        course.setStudentUsers(new ArrayList<>());
        when(courseDao.getCourse(courseId)).thenReturn(course);
        courseService.removeCourse(courseId);
        assertNotNull(course.getClasses());
        course.getClasses().forEach( item -> {
                    assertNotNull(item.getUsers());
                    assertTrue(item.getUsers().isEmpty());
                }
        );
    }

    @Test
    public void removeCourseEmptyListTest() throws ApplicationException {
        Long courseId = 2L;
        course.setClasses(new ArrayList<>());
        course.setStudentUsers(new ArrayList<>());
        when(courseDao.getCourse(courseId)).thenReturn(course);
        courseService.removeCourse(courseId);
    }

    @Test
    public void getUnselectedStudentCoursesTest() {
        Long courseId = 1L;
        course.setId(courseId);
        user.setStudentCourses(new ArrayList<>());
        user.getStudentCourses().add(course);
        when(courseDao.getAllCoursesBetweenActualDate(Mockito.anyList())).thenReturn(new ArrayList());
        List<Course> unselectedStudentCourses = courseService.getUnselectedStudentCourses(user);
        assertNotNull(unselectedStudentCourses);
        assertTrue(unselectedStudentCourses.isEmpty());
    }

    @Test
    public void getSelectedStudentCoursesBetweenDateTest() {
        Long courseId = 1L;
        Date now = new Date();
        course.setId(courseId);
        course.setStartEnrolmentDate(new Date(now.getTime() - 1000000));
        course.setEndEnrolmentDate(new Date(now.getTime() + 1000000));
        user.setStudentCourses(new ArrayList<>());
        user.getStudentCourses().add(course);
        List<Course> selectedStudentCoursesBetweenDate = courseService.getSelectedStudentCoursesBetweenDate(user);
        assertNotNull(selectedStudentCoursesBetweenDate);
        assertTrue(selectedStudentCoursesBetweenDate.size() == 1);
    }

    @Test
    public void getSelectedStudentCoursesBetweenDateAfterNowTest() {
        Long courseId = 1L;
        Date now = new Date();
        course.setId(courseId);
        course.setStartEnrolmentDate(new Date(now.getTime() + 1000000));
        course.setEndEnrolmentDate(new Date(now.getTime() + 10000000));
        user.setStudentCourses(new ArrayList<>());
        user.getStudentCourses().add(course);
        List<Course> selectedStudentCoursesBetweenDate = courseService.getSelectedStudentCoursesBetweenDate(user);
        assertNotNull(selectedStudentCoursesBetweenDate);
        assertTrue(selectedStudentCoursesBetweenDate.isEmpty());
    }

    @Test
    public void getSelectedStudentCoursesBetweenDateBeforeNowTest() {
        Long courseId = 1L;
        Date now = new Date();
        course.setId(courseId);
        course.setStartEnrolmentDate(new Date(now.getTime() - 10000000));
        course.setEndEnrolmentDate(new Date(now.getTime() - 1000000));
        user.setStudentCourses(new ArrayList<>());
        user.getStudentCourses().add(course);
        List<Course> selectedStudentCoursesBetweenDate = courseService.getSelectedStudentCoursesBetweenDate(user);
        assertNotNull(selectedStudentCoursesBetweenDate);
        assertTrue(selectedStudentCoursesBetweenDate.isEmpty());
    }

    @Test
    public void addStudentCourseNotExistTest() throws ApplicationException {
        Long courseId = 1L;
        when(courseDao.getCourse(courseId)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DOES_NOT_EXIST.getCause());
        courseService.addStudentCourse(null, courseId);
    }

    @Test
    public void addStudentCourseEndDateAfterTodayTest() throws ApplicationException {
        prepareCourseEndDateAfterToday();
        when(courseDao.getCourse(course.getId())).thenReturn(course);

        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        courseService.addStudentCourse(null, course.getId());
    }

    @Test
    public void addStudentCourseStartDateAfterTodayTest() throws ApplicationException {
        prepareCourseStartDateAfterToday();
        when(courseDao.getCourse(course.getId())).thenReturn(course);

        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        courseService.addStudentCourse(null, course.getId());
    }

    @Test
    public void getAllCourseHistoryTest() {
        when(courseEnrolmentDao.findAllCourseHistory()).thenReturn(new ArrayList<>());
        List<CourseEnrolmentHistory> allCourseHistory = courseService.getAllCourseHistory();
        assertNotNull(allCourseHistory);
        assertTrue(allCourseHistory.isEmpty());
    }

    @Test
    public void updateCourseHistoryHistoryNotExistTest() {
        when(courseEnrolmentDao.findCourseHistory(Mockito.anyLong(), Mockito.anyLong())).thenReturn(null);
        courseService.updateCourseHistory(user, course, CourseEnrolmentAction.ADD);
    }

    @Test
    public void updateCourseHistoryHistoryExistTest() {
        when(courseEnrolmentDao.findCourseHistory(Mockito.anyLong(), Mockito.anyLong())).thenReturn(new CourseEnrolmentHistory());
        courseService.updateCourseHistory(user, course, CourseEnrolmentAction.ADD);
    }

    @Test
    public void getAllTypeOfCourseDescriptionIsEmptyTest() {
        TypeOfCourse typeOfCourse = new TypeOfCourse();
        when(typeOfCourseDao.getAllTypeOfCourse()).thenReturn(Arrays.asList(typeOfCourse));
        List<TypeOfCourse> allTypeOfCourse = courseService.getAllTypeOfCourse();
        assertNotNull(allTypeOfCourse);
        assertTrue(allTypeOfCourse.size() == 1);
        assertTrue(allTypeOfCourse.get(0).getDescription().equals(""));
    }

    @Test
    public void getAllTypeOfCourseDescriptionIsNotEmptyTest() {
        TypeOfCourse typeOfCourse = new TypeOfCourse();
        typeOfCourse.setDescription("test");
        when(typeOfCourseDao.getAllTypeOfCourse()).thenReturn(Arrays.asList(typeOfCourse));
        List<TypeOfCourse> allTypeOfCourse = courseService.getAllTypeOfCourse();
        assertNotNull(allTypeOfCourse);
        assertTrue(allTypeOfCourse.size() == 1);
        assertTrue(allTypeOfCourse.get(0).getDescription().equals("test"));
    }

    @Test
    public void getAllTypeOfCourseListIsEmptyTest() {
        TypeOfCourse typeOfCourse = new TypeOfCourse();
        typeOfCourse.setDescription("test");
        when(typeOfCourseDao.getAllTypeOfCourse()).thenReturn(new ArrayList<>());
        List<TypeOfCourse> allTypeOfCourse = courseService.getAllTypeOfCourse();
        assertNotNull(allTypeOfCourse);
        assertTrue(allTypeOfCourse.isEmpty());
    }

    @Test
    public void getTypeOfCourseNotExistTest() throws ApplicationException {
        Long typeId = 1L;
        TypeOfCourse typeOfCourse = new TypeOfCourse();
        typeOfCourse.setDescription(null);
        when(typeOfCourseDao.getTypeOfCourse(typeId)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.TYPE_OF_COURSE_DOES_NOT_EXIST.getCause());
        courseService.getTypeOfCourse(typeId);
    }

    @Test
    public void getTypeOfCourseDescriptionIsNullTest() throws ApplicationException {
        Long typeId = 1L;
        TypeOfCourse typeOfCourse = new TypeOfCourse();
        typeOfCourse.setDescription(null);
        when(typeOfCourseDao.getTypeOfCourse(typeId)).thenReturn(typeOfCourse);
        TypeOfCourse type = courseService.getTypeOfCourse(typeId);
        assertNotNull(type);
    }

    @Test
    public void getTypeOfCourseTest() throws ApplicationException {
        Long typeId = 1L;
        TypeOfCourse typeOfCourse = new TypeOfCourse();
        typeOfCourse.setDescription("test");
        when(typeOfCourseDao.getTypeOfCourse(typeId)).thenReturn(typeOfCourse);
        TypeOfCourse type = courseService.getTypeOfCourse(typeId);
        assertNotNull(type);
        assertTrue(type.getDescription().equals("test"));
    }

    @Test
    public void getCourseNullResultTest() {
        Long courseId = 1L;
        when(courseDao.getCourse(courseId)).thenReturn(null);
        assertNull(courseService.getCourse(courseId));
    }

    @Test
    public void getCourseNotNullResultTest() {
        Long courseId = 2L;
        when(courseDao.getCourse(courseId)).thenReturn(course);
        assertNotNull(courseService.getCourse(courseId));
    }

    @Test
    public void removeStudentCourseNotExistTest() throws ApplicationException {
        Long courseId = 1L;
        when(courseDao.getCourse(courseId)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DOES_NOT_EXIST.getCause());
        courseService.removeStudentCourse(null, courseId);
    }

    @Test
    public void removeStudentCourseEndDateAfterTodayTest() throws ApplicationException {
        prepareCourseEndDateAfterToday();
        when(courseDao.getCourse(course.getId())).thenReturn(course);

        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        courseService.removeStudentCourse(null, course.getId());
    }


    private void prepareCourseEndDateAfterToday() {
        course.setId(1L);
        Date date = new Date();
        course.setStartEnrolmentDate(new Date(date.getTime() - 36000));
        course.setEndEnrolmentDate(date);
    }

    @Test
    public void removeStudentCourseStartDateAfterTodayTest() throws ApplicationException {
        prepareCourseStartDateAfterToday();
        when(courseDao.getCourse(course.getId())).thenReturn(course);

        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        courseService.removeStudentCourse(null, course.getId());
    }

    private void prepareCourseStartDateAfterToday() {
        course.setId(1L);
        Date date = new Date();
        course.setStartEnrolmentDate(new Date(date.getTime() + 35000));
        course.setEndEnrolmentDate(new Date(date.getTime() + 36000));
    }

    private void prepareRightCourse() {
        course.setId(1L);
        Date date = new Date();
        course.setStartEnrolmentDate(new Date(date.getTime() - 36000));
        course.setEndEnrolmentDate(new Date(date.getTime() + 36000));
    }

    private void prepareUserWithClasses() {
        Classes classes = new Classes();
        classes.setCourse(course);
        user.setSelectedStudentClasses(Arrays.asList(classes));
        user.setStudentCourses(Arrays.asList(course));
    }

}
