package com.schwimmen.subjects.service;

import com.schwimmen.subjects.dao.ClassesDao;
import com.schwimmen.subjects.dao.DayDao;
import com.schwimmen.subjects.entity.*;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class ClassesServiceTest {

    @Mock
    private ClassesDao classesDao;
    @Mock
    private DayDao dayDao;
    @InjectMocks
    private ClassesServiceImpl classesService;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private Classes classes;
    private User user;
    private ClassesDetails classesDetails;
    private Course course;
    private Day day;

    @Before
    public void beforeTest() {
        classes = new Classes();
        classesDetails = new ClassesDetails();
        user = new User();
        course = new Course();
        day = new Day();
    }

    @Test
    public void removeClassesTest() {
        classesService.removeClasses(classes);
    }

    @Test
    public void removeClassesNullTest() {
        classesService.removeClasses(null);
    }

    @Test
    public void findClassesTest() {
        Long classesId = 1L;
        classes.setId(classesId);
        when(classesDao.findClasses(classesId)).thenReturn(classes);
        Classes returnedClasses = classesService.findClasses(classesId);
        assertNotNull(returnedClasses);
        assertEquals(returnedClasses, classes);
    }

    @Test
    public void findClassesNullTest() {
        Long classesId = 1L;
        when(classesDao.findClasses(classesId)).thenReturn(null);
        Classes returnedClasses = classesService.findClasses(classesId);
        assertNull(returnedClasses);
    }

    @Test
    public void modifyClassesTest() {
        classesService.modifyClasses(classes);
    }

    @Test
    public void modifyClassesNullTest() {
        classesService.modifyClasses(null);
    }

    @Test
    public void removeClassesDetailsTest() {
        classesService.removeClassesDetails(classesDetails);
    }

    @Test
    public void removeClassesDetailsNullTest() {
        classesService.removeClassesDetails(null);
    }

    @Test
    public void findClassesDetailsTest() {
        Long classesId = 1L;
        classesDetails.setId(classesId);
        when(classesDao.findClassesDetails(classesId)).thenReturn(classesDetails);
        ClassesDetails returnedClasses = classesService.findClassesDetails(classesId);
        assertNotNull(returnedClasses);
        assertEquals(returnedClasses, classesDetails);
    }

    @Test
    public void findClassesDetailsNullTest() {
        Long classesId = 1L;
        when(classesDao.findClassesDetails(classesId)).thenReturn(null);
        ClassesDetails returnedClasses = classesService.findClassesDetails(classesId);
        assertNull(returnedClasses);
    }

    @Test
    public void modifyClassesDetailsTest() {
        classesService.modifyClassesDetails(classesDetails);
    }

    @Test
    public void modifyClassesDetailsNullTest() {
        classesService.modifyClassesDetails(null);
    }

    @Test
    public void addStudentClassesNullTest() throws ApplicationException {
        Long classesId = 1L;
        when(classesDao.findClasses(classesId)).thenReturn(null);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        classesService.addStudentClasses(classesId, user);
    }

    @Test
    public void addStudentClassesNotBeetwenDateTest() throws ApplicationException {
        Long classesId = 1L;
        Date date = new Date();
        course.setStartEnrolmentDate(new Date(date.getTime() - 100000000));
        course.setEndEnrolmentDate(new Date(date.getTime() - 10000000));
        classes.setCourse(course);
        when(classesDao.findClasses(classesId)).thenReturn(classes);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        classesService.addStudentClasses(classesId, user);
    }

    @Test
    public void addStudentClassesNotTest() throws ApplicationException {
        Long classesId = 1L;
        Date date = new Date();
        course.setStartEnrolmentDate(new Date(date.getTime() - 100000000));
        course.setEndEnrolmentDate(new Date(date.getTime() + 10000000));
        classes.setCourse(course);
        when(classesDao.findClasses(classesId)).thenReturn(classes);
        exception.expect(ApplicationException.class);
        exception.expectMessage(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE.getCause());
        classesService.addStudentClasses(classesId, user);
    }

    @Test
    public void getClassesWithTypesEmptyLsitTest() {
        Long courseId = 1L;
        Long typeClassId = 1L;
        when(classesDao.getClassesWithType(courseId, typeClassId)).thenReturn(new ArrayList<>());
        List<Classes> classesWithTypes = classesService.getClassesWithTypes(courseId, typeClassId);
        assertNotNull(classesWithTypes);
        assertTrue(classesWithTypes.isEmpty());
    }

    @Test
    public void getClassesWithTypesNotEmptyListTest() {
        Long courseId = 1L;
        Long typeClassId = 1L;
        when(classesDao.getClassesWithType(courseId, typeClassId)).thenReturn(Arrays.asList(classes));
        List<Classes> classesWithTypes = classesService.getClassesWithTypes(courseId, typeClassId);
        assertNotNull(classesWithTypes);
        assertFalse(classesWithTypes.isEmpty());
        assertTrue(classesWithTypes.size() == 1);
    }

    @Test
    public void getDayByNameNullTest() {
        when(dayDao.getDay(Days.MONDAY)).thenReturn(null);
        Day dayByName = classesService.getDayByName(Days.MONDAY);
        assertNull(dayByName);
    }

    @Test
    public void getDayByNameTest() {
        when(dayDao.getDay(Days.MONDAY)).thenReturn(day);
        Day dayByName = classesService.getDayByName(Days.MONDAY);
        assertNotNull(dayByName);
        assertEquals(day, dayByName);
    }

}
