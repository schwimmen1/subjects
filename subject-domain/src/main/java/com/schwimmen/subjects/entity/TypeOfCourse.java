package com.schwimmen.subjects.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "TYPE_OF_COURSES")
public class TypeOfCourse {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "type_of_courses_id_seq")
    @SequenceGenerator(name = "type_of_courses_id_seq", sequenceName = "type_of_courses_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", length = 50)
    @NotNull
    @Size(max = 50)
    private String name;

    @Column(name = "DESCRIPTION", length = 2000)
    @Size(max = 2000)
    private String description;

    @ManyToMany(mappedBy = "typeOfCourse", fetch = FetchType.LAZY)
    private List<Classes> classes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Classes> getClasses() {
        return classes;
    }

    public void setClasses(List<Classes> classes) {
        this.classes = classes;
    }

    @Override
    public boolean equals(Object typeOfCourse) {
        if(!(typeOfCourse instanceof TypeOfCourse)) {
            return false;
        }
        return Optional.ofNullable(name)
                .map(name -> name.equalsIgnoreCase(((TypeOfCourse) typeOfCourse).getName()))
                .orElse(false);
    }

}
