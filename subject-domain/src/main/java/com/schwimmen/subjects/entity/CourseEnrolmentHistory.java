package com.schwimmen.subjects.entity;

import com.schwimmen.subjects.entity.security.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "COURSE_ENROLMENT_HISTORY")
public class CourseEnrolmentHistory {

    @EmbeddedId
    private CourseEnrolmentHistoryId id;

    @Column(name = "ACTION", length = 50)
    @NotNull
    @Enumerated(EnumType.STRING)
    private CourseEnrolmentAction action;

    public CourseEnrolmentHistoryId getId() {
        return id;
    }

    public void setId(CourseEnrolmentHistoryId id) {
        this.id = id;
    }

    public CourseEnrolmentAction getAction() {
        return action;
    }

    public void setAction(CourseEnrolmentAction action) {
        this.action = action;
    }

    @Transient
    public User getUser() {
        if(Objects.isNull(this.getId())) {
            this.setId(new CourseEnrolmentHistoryId());
        }
        return this.getId().getUser();
    }

    @Transient
    public void setUser(User user) {
        if(Objects.isNull(this.getId())) {
            this.setId(new CourseEnrolmentHistoryId());
        }
        this.getId().setUser(user);
    }

    @Transient
    public Course getCourse() {
        if(Objects.isNull(this.getId())) {
            this.setId(new CourseEnrolmentHistoryId());
        }
        return this.getId().getCourse();
    }

    @Transient
    public void setCourse(Course course) {
        if(Objects.isNull(this.getId())) {
            this.setId(new CourseEnrolmentHistoryId());
        }
        this.getId().setCourse(course);
    }

}
