package com.schwimmen.subjects.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "CLASSES_DETAILS")
public class ClassesDetails implements Comparable<ClassesDetails> {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "classes_details_id_seq")
    @SequenceGenerator(name = "classes_details_id_seq", sequenceName = "classes_details_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="DAY_ID")
    private Day day;

    @Column(name = "HOUR_FROM")
    @Temporal(TemporalType.TIME)
    @NotNull
    private Date hoursFrom;

    @Column(name = "HOUR_TO")
    @Temporal(TemporalType.TIME)
    @NotNull
    private Date hoursTo;

    @Column(name = "BUILDING", length = 50)
    @Size(max = 50)
    private String building;

    @Column(name = "ROOM_NUMBER", length = 50)
    @Size(max = 50)
    private String roomNumber;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="CLASSES_ID")
    private Classes classes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Date getHoursFrom() {
        return hoursFrom;
    }

    public void setHoursFrom(Date hoursFrom) {
        this.hoursFrom = hoursFrom;
    }

    public Date getHoursTo() {
        return hoursTo;
    }

    public void setHoursTo(Date hoursTo) {
        this.hoursTo = hoursTo;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    @Override
    public int compareTo(ClassesDetails classesDetails) {
        if(classesDetails.getDay().getName().ordinal() < this.getDay().getName().ordinal()) {
            return 1;
        } else if(classesDetails.getDay().getName().ordinal() > this.getDay().getName().ordinal()) {
            return -1;
        }
        return this.getHoursFrom().compareTo(classesDetails.getHoursFrom());
    }

}
