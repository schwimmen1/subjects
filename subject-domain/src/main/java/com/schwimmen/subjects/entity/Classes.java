package com.schwimmen.subjects.entity;


import com.schwimmen.subjects.entity.security.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CLASSES")
public class Classes implements Serializable, Comparable<Classes> {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "classes_id_seq")
    @SequenceGenerator(name = "classes_id_seq", sequenceName = "classes_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="COURSE_TYPE_ID")
    private TypeOfCourse typeOfCourse;

    @Column(name = "MAX_GROUP_NUMBER")
    @NotNull
    private Integer maxGroupNumber;

    @Column(name = "LECTURER", length = 50)
    @Size(max = 50)
    private String lecturer;

    @Column(name = "DESCRIPTION", length = 2000)
    @Size(max = 2000)
    private String description;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="COURSE_ID")
    private Course course;

    @OneToMany(mappedBy="classes", fetch = FetchType.LAZY)
    private List<ClassesDetails> classesDetails;

    //@ManyToMany(mappedBy = "selectedStudentClasses", fetch = FetchType.LAZY)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "CHOSEN_GROUP_STUDENTS_COURSES",
            joinColumns = {@JoinColumn(name = "COURSE_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "ID", referencedColumnName = "ID")})
    private List<User> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeOfCourse getTypeOfCourse() {
        return typeOfCourse;
    }

    public void setTypeOfCourse(TypeOfCourse typeOfCourse) {
        this.typeOfCourse = typeOfCourse;
    }

    public Integer getMaxGroupNumber() {
        return maxGroupNumber;
    }

    public void setMaxGroupNumber(Integer maxGroupNumber) {
        this.maxGroupNumber = maxGroupNumber;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<ClassesDetails> getClassesDetails() {
        return classesDetails;
    }

    public void setClassesDetails(List<ClassesDetails> classesDetails) {
        this.classesDetails = classesDetails;
    }

    public List<User> getUsers() {
        if(Objects.isNull(users)) {
            users = new ArrayList<>();
        }
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Transient
    public Integer getActualGroupNumber() {
        return Objects.nonNull(getUsers()) ? getUsers().size() : 0;
    }

    @Override
    public int compareTo(Classes classes) {
        Collections.sort(this.getClassesDetails());
        Collections.sort(classes.getClassesDetails());
        ClassesDetails thisClassesDetails = this.getClassesDetails().get(0);
        ClassesDetails thatClassesDetails = classes.getClassesDetails().get(0);
        if(thatClassesDetails.getDay().getName().ordinal() < thisClassesDetails.getDay().getName().ordinal()) {
            return 1;
        } else if(thatClassesDetails.getDay().getName().ordinal() > thisClassesDetails.getDay().getName().ordinal()) {
            return -1;
        }
        return thisClassesDetails.getHoursFrom().compareTo(thatClassesDetails.getHoursFrom());
    }

}
