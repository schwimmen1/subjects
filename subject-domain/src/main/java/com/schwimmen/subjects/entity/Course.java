package com.schwimmen.subjects.entity;

import com.schwimmen.subjects.entity.security.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name  = "COURSES")
public class Course implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "courses_id_seq")
    @SequenceGenerator(name = "courses_id_seq", sequenceName = "courses_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", length = 50)
    @NotNull
    @Size(max = 50)
    private String name;

    @Column(name = "DESCRIPTION", length = 2000)
    @Size(max = 2000)
    private String description;

    @Column(name = "START_ENROLMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startEnrolmentDate;

    @Column(name = "END_ENROLMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endEnrolmentDate;

    @ManyToMany(mappedBy="teacherCourses", fetch = FetchType.LAZY)
    private List<User> teacherUsers;

    //@ManyToMany(mappedBy="studentCourses", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "STUDENT_COURSES",
            joinColumns = {@JoinColumn(name = "ID_COURSE", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "ID", referencedColumnName = "ID")})
    private List<User> studentUsers;

    @OneToMany(mappedBy="course", fetch = FetchType.LAZY)
    private List<Classes> classes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartEnrolmentDate() {
        return startEnrolmentDate;
    }

    public void setStartEnrolmentDate(Date startEnrolmentDate) {
        this.startEnrolmentDate = startEnrolmentDate;
    }

    public Date getEndEnrolmentDate() {
        return endEnrolmentDate;
    }

    public void setEndEnrolmentDate(Date endEnrolmentDate) {
        this.endEnrolmentDate = endEnrolmentDate;
    }

    public List<User> getTeacherUsers() {
        if(Objects.isNull(teacherUsers)) {
            teacherUsers = new ArrayList<>();
        }
        return teacherUsers;
    }

    public void setTeacherUsers(List<User> teacherUsers) {
        this.teacherUsers = teacherUsers;
    }

    public List<User> getStudentUsers() {
        if(Objects.isNull(studentUsers)) {
            studentUsers = new ArrayList<>();
        }
        return studentUsers;
    }

    public void setStudentUsers(List<User> studentUsers) {
        this.studentUsers = studentUsers;
    }

    public List<Classes> getClasses() {
        if(Objects.isNull(classes)) {
            classes = new ArrayList<>();
        }
        return classes;
    }

    public void setClasses(List<Classes> classes) {
        this.classes = classes;
    }

}
