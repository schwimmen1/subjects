package com.schwimmen.subjects.entity;

import com.schwimmen.subjects.entity.security.User;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class CourseEnrolmentHistoryId implements Serializable {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="USER_ID")
    private User user;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="COURSE_ID")
    private Course course;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
