package com.schwimmen.subjects.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "DAYS")
public class Day implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "day_id_seq")
    @SequenceGenerator(name = "day_id_seq", sequenceName = "day_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    @NotNull
    @Enumerated(EnumType.STRING)
    private Days name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Days getName() {
        return name;
    }

    public void setName(Days name) {
        this.name = name;
    }

}
