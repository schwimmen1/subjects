package com.schwimmen.subjects.entity.security;

import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.Course;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "USERS")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
    private Long id;

    @Column(name = "USERNAME", length = 50, unique = true)
    @NotNull
    @Size(min = 4, max = 50)
    private String username;

    @Column(name = "FIRST_NAME", length = 50)
    @NotNull
    @Size(min = 3, max = 50)
    @Pattern(regexp="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+")
    private String firstName;

    @Column(name = "LAST_NAME", length = 50)
    @NotNull
    @Size(min = 3, max = 50)
    @Pattern(regexp="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+")
    private String lastName;

    @Column(name = "EMAIL", length = 50)
    @NotNull
    @Size(min = 4, max = 50)
    @Pattern(regexp="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")
    private String email;

    @Column(name = "ldap")
    @NotNull
    private Boolean ldap;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "USERS_AUTHORITY",
            joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID")})
    private List<Authority> authorities;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "STUDENT_COURSES",
            joinColumns = {@JoinColumn(name = "ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "ID_COURSE", referencedColumnName = "ID")})
    @OrderBy("name ASC")
    private List<Course> studentCourses;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "TEACHER_COURSES",
            joinColumns = {@JoinColumn(name = "ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "ID_COURSE", referencedColumnName = "ID")})
    @OrderBy("name ASC")
    private List<Course> teacherCourses;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "CHOSEN_GROUP_STUDENTS_COURSES",
            joinColumns = {@JoinColumn(name = "ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "COURSE_ID", referencedColumnName = "ID")})
    private List<Classes> selectedStudentClasses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = Optional.ofNullable(username).map(String::toLowerCase).orElse("");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getLdap() {
        return ldap;
    }

    public void setLdap(Boolean ldap) {
        this.ldap = ldap;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public List<Course> getStudentCourses() {
        return studentCourses;
    }

    public void setStudentCourses(List<Course> studentCourses) {
        this.studentCourses = studentCourses;
    }

    public List<Course> getTeacherCourses() {
        return teacherCourses;
    }

    public void setTeacherCourses(List<Course> teacherCourses) {
        this.teacherCourses = teacherCourses;
    }

    public List<Classes> getSelectedStudentClasses() {
        return selectedStudentClasses;
    }

    public void setSelectedStudentClasses(List<Classes> selectedStudentClasses) {
        this.selectedStudentClasses = selectedStudentClasses;
    }

}