package com.schwimmen.subjects.entity;

public enum CourseEnrolmentAction {
    ADD, REMOVE
}
