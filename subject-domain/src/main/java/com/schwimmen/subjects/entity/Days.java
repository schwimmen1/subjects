package com.schwimmen.subjects.entity;

import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;

import java.util.Arrays;

public enum Days {

    MONDAY("poniedziałek"),
    TUESDAY("wtorek"),
    WEDNESDAY("środa"),
    THURSDAY("czwartek"),
    FRIDAY("piątek"),
    SATURDAY("sobota"),
    SUNDAY("niedziela");

    String value;

    Days(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public static Days getDayByCode(String code) throws ApplicationException {
        return Arrays.stream(Days.values()).filter(value -> value.name().equalsIgnoreCase(code)).findAny()
                .orElseThrow(() -> new ApplicationException(ApplicationError.DAY_WITH_THIS_CODE_DOES_NOT_EXIST));
    }

}
