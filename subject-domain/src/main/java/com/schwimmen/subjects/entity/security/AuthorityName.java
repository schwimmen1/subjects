package com.schwimmen.subjects.entity.security;

import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;

import java.util.Arrays;

public enum AuthorityName {
    ROLE_STUDENT, ROLE_TEACHER, ROLE_ADMIN;

    public static AuthorityName getAuthorityByCode(String code) throws ApplicationException {
        return Arrays.stream(AuthorityName.values()).filter(value -> value.name().equalsIgnoreCase(code)).findAny()
                .orElseThrow(() -> new ApplicationException(ApplicationError.AUTHORITY_WITH_THIS_CODE_DOES_NOT_EXIST));
    }

}