package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.TypeOfCourse;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TypeOfCourseDao extends EntityDao<TypeOfCourse> {

    @SuppressWarnings("unchecked")
    public List<TypeOfCourse> getAllTypeOfCourse() {
        Criteria searchCriteria = super.getSearchCriteria(TypeOfCourse.class);
        return searchCriteria.list();
    }

    public TypeOfCourse getTypeOfCourse(Long typeId) {
        Criteria searchCriteria = super.getSearchCriteria(TypeOfCourse.class);
        searchCriteria.add(Restrictions.eq("id", typeId));
        return (TypeOfCourse) searchCriteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<TypeOfCourse> getChosenTypeOfCourse(Long courseId) {
        Criteria searchCriteria = super.getSearchCriteria(TypeOfCourse.class, "courseType");
        searchCriteria.createAlias("courseType.classes", "class");
        searchCriteria.add(Restrictions.eq("class.course.id", courseId));
        searchCriteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("courseType.id"), "id")
                .add(Projections.groupProperty("courseType.name"), "name"));
        searchCriteria.setResultTransformer(new AliasToBeanResultTransformer(TypeOfCourse.class));
        return searchCriteria.list();
    }

}
