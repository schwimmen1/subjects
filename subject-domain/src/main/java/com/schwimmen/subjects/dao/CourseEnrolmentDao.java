package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.CourseEnrolmentHistory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CourseEnrolmentDao extends EntityDao<CourseEnrolmentHistory> {

    @SuppressWarnings("unchecked")
    public List<CourseEnrolmentHistory> findAllCourseHistory() {
        Criteria searchCriteria = super.getSearchCriteria(CourseEnrolmentHistory.class);
        return searchCriteria.list();
    }

    public CourseEnrolmentHistory findCourseHistory(Long userId, Long courseId) {
        String hql = "SELECT history FROM com.schwimmen.subjects.entity.CourseEnrolmentHistory history WHERE " +
                "history.id.user.id = :userId AND history.id.course.id = :courseId";
        Query query = super.getSession().createQuery(hql);
        query.setParameter("userId", userId);
        query.setParameter("courseId", courseId);
        return (CourseEnrolmentHistory) query.uniqueResult();
    }

    public void saveCourseEnrolmentHistory(CourseEnrolmentHistory courseHistory) {
        super.getEntityManager().merge(courseHistory);
    }

    public void removeAllCourseHistory() {
        getSession().createQuery("DELETE FROM com.schwimmen.subjects.entity.CourseEnrolmentHistory").executeUpdate();
    }

}
