package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.ClassesDetails;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClassesDao extends EntityDao<Classes> {

    public Classes findClasses(Long classesId) {
        Criteria searchCriteria = super.getSearchCriteria(Classes.class);
        searchCriteria.add(Restrictions.eq("id", classesId));
        return (Classes) searchCriteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<Classes> getClassesWithType(Long courseId, Long typeClassId) {
        Criteria searchCriteria = super.getSearchCriteria(Classes.class, "classes");
        searchCriteria.createAlias("classes.course", "courseClasses");
        searchCriteria.createAlias("classes.typeOfCourse", "classesType");
        searchCriteria.add(Restrictions.eq("classesType.id", typeClassId));
        searchCriteria.add(Restrictions.eq("courseClasses.id", courseId));
        return searchCriteria.list();
    }

    public Classes updateClasses(Classes classes) {
        return super.getEntityManager().merge(classes);
    }

    public void removeClasses(Classes classes) {
        super.getEntityManager().remove(classes);
    }

    public ClassesDetails updateClassesDetails(ClassesDetails classesDetails) {
        return super.getEntityManager().merge(classesDetails);
    }

    public void removeClassesDetails(ClassesDetails classesDetails) {
        super.getEntityManager().remove(classesDetails);
    }

    public ClassesDetails findClassesDetails(Long classesDetailsId) {
        Criteria searchCriteria = super.getSession().createCriteria(ClassesDetails.class);
        searchCriteria.add(Restrictions.eq("id", classesDetailsId));
        return (ClassesDetails) searchCriteria.uniqueResult();
    }

}
