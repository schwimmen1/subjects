package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.Day;
import com.schwimmen.subjects.entity.Days;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class DayDao extends EntityDao<Day> {

    public Day getDay(Days day) {
        Criteria searchCriteria = super.getSearchCriteria(Day.class);
        searchCriteria.add(Restrictions.eq("name", day));
        return (Day) searchCriteria.uniqueResult();
    }

}
