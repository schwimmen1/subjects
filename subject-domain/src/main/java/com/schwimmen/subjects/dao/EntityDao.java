package com.schwimmen.subjects.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public abstract class EntityDao<T> {

    @PersistenceContext
    private EntityManager entityManager;

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected Criteria getSearchCriteria(Class<T> clazz) {
        return getSession().createCriteria(clazz);
    }

    protected Criteria getSearchCriteria(Class<T> clazz, String alias) {
        return getSession().createCriteria(clazz, alias);
    }

    protected Session getSession() {
        return entityManager.unwrap(Session.class);
    }

}
