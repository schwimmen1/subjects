package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.security.ApplicationUser;
import com.schwimmen.subjects.entity.security.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class ApplicationUserDao extends EntityDao<ApplicationUser>  {

    public ApplicationUser findUserByUsername(String username) {
        Criteria searchCriteria = super.getSearchCriteria(ApplicationUser.class);
        searchCriteria.add(Restrictions.eq("username", username));
        return (ApplicationUser) searchCriteria.uniqueResult();
    }

    public ApplicationUser findUserByEmail(String email) {
        Criteria searchCriteria = super.getSearchCriteria(ApplicationUser.class);
        searchCriteria.add(Restrictions.eq("email", email));
        return (ApplicationUser) searchCriteria.uniqueResult();
    }

    public void unblockUser(String username, String unblockToken) {
        String hql = "UPDATE com.schwimmen.subjects.entity.security.ApplicationUser user SET user.enabled = TRUE " +
                "WHERE user.username = :username AND user.unblockToken = :unblockToken AND user.ldap = false";
        Query query = super.getSession().createQuery(hql);
        query.setParameter("username", username);
        query.setParameter("unblockToken", unblockToken);
        query.executeUpdate();
    }

    public User saveUser(User user) {
        return super.getEntityManager().merge(user);
    }

}
