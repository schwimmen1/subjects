package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.security.Authority;
import com.schwimmen.subjects.entity.security.AuthorityName;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorityDao extends EntityDao<Authority> {

    public Authority getAuthority(AuthorityName authorityName) {
        Criteria searchCriteria = super.getSearchCriteria(Authority.class);
        searchCriteria.add(Restrictions.eq("name", authorityName));
        return (Authority) searchCriteria.uniqueResult();
    }

}
