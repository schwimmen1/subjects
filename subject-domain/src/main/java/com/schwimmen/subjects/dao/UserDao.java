package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class UserDao extends EntityDao<User> {

    @SuppressWarnings("unchecked")
    public List<User> findUserByRole(AuthorityName role) {
        Criteria searchCriteria = super.getSearchCriteria(User.class, "user");
        searchCriteria.createAlias("user.authorities", "role");
        searchCriteria.add(Restrictions.eq("role.name", role));
        searchCriteria.addOrder(Order.asc("user.lastName"));
        searchCriteria.addOrder(Order.asc("user.firstName"));
        return searchCriteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<User> findUserByRole(AuthorityName role, List<Long> excludeUserId) {
        if(Objects.isNull(excludeUserId) || excludeUserId.isEmpty()) {
            return this.findUserByRole(role);
        }
        Criteria searchCriteria = super.getSearchCriteria(User.class, "user");
        searchCriteria.add(Restrictions.not(Restrictions.in("user.id", excludeUserId)));
        searchCriteria.createAlias("user.authorities", "role");
        searchCriteria.add(Restrictions.eq("role.name", role));
        searchCriteria.addOrder(Order.asc("user.lastName"));
        searchCriteria.addOrder(Order.asc("user.firstName"));
        return searchCriteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<User> findUserByRoleAndId(AuthorityName role, List<Long> userId) {
        if(Objects.isNull(userId) || userId.isEmpty()) {
            return this.findUserByRole(role);
        }
        Criteria searchCriteria = super.getSearchCriteria(User.class, "user");
        searchCriteria.add(Restrictions.in("user.id", userId));
        searchCriteria.createAlias("user.authorities", "role");
        searchCriteria.add(Restrictions.eq("role.name", role));
        searchCriteria.addOrder(Order.asc("user.lastName"));
        searchCriteria.addOrder(Order.asc("user.firstName"));
        return searchCriteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<User> findUserByUsername(String username) {
        Criteria searchCriteria = super.getSearchCriteria(User.class);
        searchCriteria.add(Restrictions.eq("username", username));
        return searchCriteria.list();
    }

    public User findUserByUsername(String username, boolean isLdap) {
        Criteria searchCriteria = super.getSearchCriteria(User.class);
        searchCriteria.add(Restrictions.eq("username", username));
        searchCriteria.add(Restrictions.eq("ldap", isLdap));
        return (User) searchCriteria.uniqueResult();
    }

}
