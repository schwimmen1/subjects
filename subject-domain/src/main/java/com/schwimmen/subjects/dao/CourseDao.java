package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.entity.Course;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class CourseDao extends EntityDao<Course> {

    @SuppressWarnings("unchecked")
    public List<Course> getAllCourses() {
        Criteria searchCriteria = super.getSearchCriteria(Course.class);
        return searchCriteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Course> getAllCoursesBetweenActualDate() {
        Criteria searchCriteria = super.getSearchCriteria(Course.class);
        searchCriteria.add(Restrictions.ge("endEnrolmentDate", new Date()));
        searchCriteria.add(Restrictions.lt("startEnrolmentDate", new Date()));
        return searchCriteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Course> getAllCourses(List<Long> coursesId) {
        if(Objects.isNull(coursesId) || coursesId.isEmpty()) {
            return getAllCourses();
        }
        Criteria searchCriteria = super.getSearchCriteria(Course.class);
        searchCriteria.add(Restrictions.in("id", coursesId));
        return searchCriteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Course> getAllCoursesBetweenActualDate(List<Long> coursesId) {
        if(Objects.isNull(coursesId) || coursesId.isEmpty()) {
            return getAllCoursesBetweenActualDate();
        }
        Criteria searchCriteria = super.getSearchCriteria(Course.class);
        searchCriteria.add(Restrictions.not(Restrictions.in("id", coursesId)));
        searchCriteria.add(Restrictions.ge("endEnrolmentDate", new Date()));
        searchCriteria.add(Restrictions.lt("startEnrolmentDate", new Date()));
        return searchCriteria.list();
    }

    public Course getCourse(Long courseId) {
        Criteria searchCriteria = super.getSearchCriteria(Course.class);
        searchCriteria.add(Restrictions.eq("id",courseId));
        return (Course) searchCriteria.uniqueResult();
    }

    public Course updateCourse(Course course) {
        Course mergedCcourse = super.getEntityManager().merge(course);
        super.getEntityManager().flush();
        return mergedCcourse;
    }

    public void removeCourse(Course course) {
        super.getEntityManager().remove(course);
    }

}
