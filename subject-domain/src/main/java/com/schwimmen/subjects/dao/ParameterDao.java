package com.schwimmen.subjects.dao;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.entity.Parameter;
import com.schwimmen.subjects.entity.security.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ParameterDao extends EntityDao<Parameter> {

    public String getParameterValue(Parameters parameter) {
        String hql = "SELECT p.value FROM com.schwimmen.subjects.entity.Parameter p WHERE p.name = :name";
        Query query = super.getSession().createQuery(hql);
        query.setParameter("name",parameter.name());
        return (String) query.uniqueResult();
    }

    public Parameter getParameter(Parameters parameter) {
        Criteria searchCriteria = super.getSearchCriteria(Parameter.class);
        searchCriteria.add(Restrictions.eq("name", parameter.name()));
        return (Parameter) searchCriteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<Parameter> getAllParameters() {
        Criteria searchCriteria = super.getSearchCriteria(Parameter.class);
        return searchCriteria.list();
    }

    public void saveParameter(Parameter parameter) {
        super.getEntityManager().merge(parameter);
    }

}
