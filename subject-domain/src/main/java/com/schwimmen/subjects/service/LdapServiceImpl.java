package com.schwimmen.subjects.service;

import com.schwimmen.subjects.service.mappers.PersonContextMapper;
import com.schwimmen.subjects.service.interfaces.LdapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class LdapServiceImpl implements LdapService {

    private final static String OBJECT_CLASS = "objectclass";
    private final static String OBJECT_CLASS_VALUE = "Person";
    private final static String USERNAME = "uid";
    private final static  String BASE = "";

    private final LdapTemplate ldapTemplate;
    private final PersonContextMapper personContextMapper;

    @Autowired
    public LdapServiceImpl(PersonContextMapper personContextMapper, LdapTemplate ldapTemplate) {
        this.personContextMapper = personContextMapper;
        this.ldapTemplate = ldapTemplate;
    }

    @Override
    public boolean authenticateLdapUser(String username, String password) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter(OBJECT_CLASS, OBJECT_CLASS_VALUE)).and(new EqualsFilter(USERNAME, username));
        return ldapTemplate.authenticate(BASE, filter.toString(), password);
    }

    @Override
    public Map<String, String> getLdapUserAttribute(String username) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter(OBJECT_CLASS, OBJECT_CLASS_VALUE)).and(new EqualsFilter(USERNAME, username));
        return ldapTemplate.searchForObject(BASE, filter.toString(), personContextMapper);
    }

}
