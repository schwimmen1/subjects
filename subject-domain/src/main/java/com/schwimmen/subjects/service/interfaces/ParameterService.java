package com.schwimmen.subjects.service.interfaces;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.entity.Parameter;
import com.schwimmen.subjects.exception.ApplicationException;

import java.util.Date;
import java.util.List;

public interface ParameterService {

    List<Parameter> getAllParameters();
    String getParameterValue(Parameters parameter);
    void saveParameter(String nameParameter, String value) throws ApplicationException;
    Object castValueToObject(Parameter parameter) throws ApplicationException;
    Date getDate(Parameters parameter);

}
