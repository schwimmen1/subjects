package com.schwimmen.subjects.service.interfaces;

import com.schwimmen.subjects.entity.Classes;
import com.schwimmen.subjects.entity.ClassesDetails;
import com.schwimmen.subjects.entity.Day;
import com.schwimmen.subjects.entity.Days;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClassesService {

    void removeClasses(Classes classes);
    Classes findClasses(Long classesId);
    void modifyClasses(Classes classes);
    void removeClassesDetails(ClassesDetails classesDetails);
    ClassesDetails findClassesDetails(Long classesDetailsId);
    void modifyClassesDetails(ClassesDetails classesDetails);
    void addStudentClasses(Long classesId, User user) throws ApplicationException;
    void deleteStudentClasses(Long classesId, User user);
    List<Classes> getClassesWithTypes(Long courseId, Long typeClassId);
    Day getDayByName(Days day);

}
