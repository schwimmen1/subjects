package com.schwimmen.subjects.service.interfaces;

import com.schwimmen.subjects.entity.security.ApplicationUser;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationException;

import java.util.List;

public interface UserService {

    /** Only for application users */
    ApplicationUser findUserByUsername(String username);
    ApplicationUser findUserByEmail(String email);
    void unblockUser(String username, String unblockToken);
    /** For all types users - also ldap */
    User findUserByUsername(String username, boolean isLdap);
    List<User> findUserByRole(AuthorityName role);
    List<User> findUserByRole(AuthorityName role, List<Long> excludeUserId);
    List<User> findUserByRoleAndId(AuthorityName role, List<Long> usersId);
    void updateUser(User user);
    User createUser(User user) throws ApplicationException;
    User createUser(User user, AuthorityName authority) throws ApplicationException;
    boolean checkIfUserIsSaved(String username, boolean isLdapUser);

}
