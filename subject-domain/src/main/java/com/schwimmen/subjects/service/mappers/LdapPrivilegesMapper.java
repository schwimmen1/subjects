package com.schwimmen.subjects.service.mappers;

import com.schwimmen.subjects.entity.security.AuthorityName;

import java.util.List;

public class LdapPrivilegesMapper {

    private interface LdapPrivilege {
        String STUDENT = "Studenci";
    }

    static public String mapLdapPrivilege(List<String> values) {
        return values.stream()
                .filter(elem -> elem.contains(LdapPrivilege.STUDENT)).findAny()
                .map(elem -> AuthorityName.ROLE_STUDENT.name())
                .orElse(AuthorityName.ROLE_TEACHER.name());
    }

}
