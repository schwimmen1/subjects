package com.schwimmen.subjects.service.interfaces;

import com.schwimmen.subjects.entity.Course;
import com.schwimmen.subjects.entity.CourseEnrolmentAction;
import com.schwimmen.subjects.entity.CourseEnrolmentHistory;
import com.schwimmen.subjects.entity.TypeOfCourse;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationException;

import java.util.List;

public interface CourseService {

    void clearStudentCourse(Long courseId) throws ApplicationException;
    void removeCourse(Long courseId) throws ApplicationException;
    List<Course> getUnselectedStudentCourses(User user);
    List<Course> getSelectedStudentCoursesBetweenDate(User user);
    void addStudentCourse(User user, Long chosenCourseId) throws ApplicationException;
    void removeStudentCourse(User user, Long chosenCourseId) throws ApplicationException;
    void removeAllCourseHistory();
    List<CourseEnrolmentHistory> getAllCourseHistory();
    void updateCourseHistory(User user, Course course, CourseEnrolmentAction action);
    List<TypeOfCourse> getAllTypeOfCourse();
    TypeOfCourse getTypeOfCourse(Long typeId) throws ApplicationException;
    List<TypeOfCourse> getAllTypeOfCourse(Long courseId);
    Course getCourse(Long courseId);
    void createCourseWithClasses(Course course);
    void updateCourseWithClasses(Course course) throws ApplicationException;
}
