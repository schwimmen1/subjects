package com.schwimmen.subjects.service.mappers;

import com.schwimmen.subjects.LdapUserAttribute;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.stereotype.Component;

import javax.naming.NamingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
public class PersonContextMapper implements ContextMapper<Map<String, String>> {

    private static final String LDAP_LOGIN = "uid";
    private static final String LDAP_NAME = "givenname";
    private static final String LDAP_SURNAME = "sn";
    private static final String LDAP_EMAIL = "mail";
    private static final String PRIVILEGE = "groupmembership";

    @Override
    public Map<String, String> mapFromContext(Object o) throws NamingException {
        DirContextAdapter context = (DirContextAdapter)o;
        Map<String, String> attributes = new HashMap<>();
        attributes.put(LdapUserAttribute.LDAP_LOGIN.value(), (String)context.getObjectAttribute(LDAP_LOGIN));
        attributes.put(LdapUserAttribute.NAME.value(), (String)context.getObjectAttribute(LDAP_NAME));
        attributes.put(LdapUserAttribute.SURNAME.value(), (String)context.getObjectAttribute(LDAP_SURNAME));
        attributes.put(LdapUserAttribute.EMAIL.value(), (String)context.getObjectAttribute(LDAP_EMAIL));
        attributes.put(LdapUserAttribute.ROLE.value(), LdapPrivilegesMapper.mapLdapPrivilege
                (Arrays.asList(context.getStringAttributes(PRIVILEGE))));
        return attributes;
    }

}