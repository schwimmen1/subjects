package com.schwimmen.subjects.service.interfaces;

import java.util.Map;

public interface LdapService {

    boolean authenticateLdapUser(String username, String password);
    Map<String, String> getLdapUserAttribute(String username);

}
