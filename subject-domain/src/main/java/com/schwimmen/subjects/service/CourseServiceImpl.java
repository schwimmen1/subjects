package com.schwimmen.subjects.service;

import com.schwimmen.subjects.dao.ClassesDao;
import com.schwimmen.subjects.dao.CourseDao;
import com.schwimmen.subjects.dao.CourseEnrolmentDao;
import com.schwimmen.subjects.dao.TypeOfCourseDao;
import com.schwimmen.subjects.entity.*;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.service.interfaces.ClassesService;
import com.schwimmen.subjects.service.interfaces.CourseService;
import com.schwimmen.subjects.service.interfaces.UserService;
import com.schwimmen.subjects.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseDao courseDao;
    private final ClassesDao classesDao;
    private final UserService userService;
    private final TypeOfCourseDao typeOfCourseDao;
    private final CourseEnrolmentDao courseEnrolmentDao;
    private final ClassesService classesService;

    @Autowired
    public CourseServiceImpl(CourseDao courseDao, UserService userService, TypeOfCourseDao typeOfCourseDao
            , CourseEnrolmentDao courseEnrolmentDao, ClassesDao classesDao, ClassesService classesService) {
        this.courseDao = courseDao;
        this.userService = userService;
        this.typeOfCourseDao = typeOfCourseDao;
        this.courseEnrolmentDao = courseEnrolmentDao;
        this.classesDao = classesDao;
        this.classesService = classesService;
    }

    @Override
    @Transactional
    public void clearStudentCourse(Long courseId) throws ApplicationException {
        Optional<Course> course = Optional.ofNullable((courseDao.getCourse(courseId)));
        course.ifPresent(item -> {
            item.getClasses().forEach(classes -> {
                classes.getUsers().clear();
                classesService.modifyClasses(classes);
            });
            item.getStudentUsers().clear();
            courseDao.updateCourse(item);
        });
        if(!course.isPresent()) {
            throw new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST);
        }
    }

    @Override
    @Transactional
    public void removeCourse(Long courseId) throws ApplicationException{
        Optional<Course> course = Optional.ofNullable((courseDao.getCourse(courseId)));
        course.ifPresent(item -> {
            item.getClasses().forEach(classes -> {
                classes.getClassesDetails().forEach(classesService::removeClassesDetails);
                classesService.removeClasses(classes);
            });
            courseDao.removeCourse(item);
        });
        if(!course.isPresent()) {
            throw new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST);
        }
    }

    @Override
    @Transactional
    public List<Course> getUnselectedStudentCourses(User user) {
        return courseDao.getAllCoursesBetweenActualDate(user.getStudentCourses().stream()
                .map(Course::getId).collect(Collectors.toList()));
    }

    @Override
    @Transactional
    public List<Course> getSelectedStudentCoursesBetweenDate(User user) {
        return user.getStudentCourses().stream().filter(course -> DateUtils
                .betweenActualDate(course.getStartEnrolmentDate(), course.getEndEnrolmentDate()))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void addStudentCourse(User user, Long chosenCourseId) throws ApplicationException {
        Optional<Course> courseToAdd = Optional.ofNullable(courseDao.getCourse(chosenCourseId));
        if(!courseToAdd.isPresent()) {
            throw new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST);
        } else if(!checkCourseBetweenActualDate(courseToAdd.get())) {
            throw new ApplicationException(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE);
        } else if(user.getStudentCourses().stream().noneMatch(course -> courseToAdd.get().getId().equals(course.getId()))) {
            user.getStudentCourses().add(courseToAdd.get());
            userService.updateUser(user);
            updateCourseHistory(user, courseToAdd.get(), CourseEnrolmentAction.ADD);
        }
    }

    @Override
    @Transactional
    public void removeStudentCourse(User user, Long chosenCourseId) throws ApplicationException {
        Optional<Course> courseToRemove = Optional.ofNullable(courseDao.getCourse(chosenCourseId));
        if(!courseToRemove.isPresent()) {
            throw new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST);
        } else if(!checkCourseBetweenActualDate(courseToRemove.get())) {
            throw new ApplicationException(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE);
        }
        user.setSelectedStudentClasses(user.getSelectedStudentClasses().stream()
                .filter(classes -> !courseToRemove.get().getId().equals(classes.getCourse().getId()))
                .collect(Collectors.toList()));
        user.setStudentCourses(user.getStudentCourses().stream()
                .filter(course -> !courseToRemove.get().getId().equals(course.getId()))
                .collect(Collectors.toList()));
        userService.updateUser(user);
        updateCourseHistory(user, courseToRemove.get(), CourseEnrolmentAction.REMOVE);
    }

    @Override
    @Transactional
    public void removeAllCourseHistory() {
        courseEnrolmentDao.removeAllCourseHistory();
    }

    @Override
    @Transactional
    public List<CourseEnrolmentHistory> getAllCourseHistory() {
        return courseEnrolmentDao.findAllCourseHistory();
    }

    @Override
    @Transactional
    public void updateCourseHistory(User user, Course course, CourseEnrolmentAction action) {
        Optional<CourseEnrolmentHistory> courseHistory =
                Optional.ofNullable(courseEnrolmentDao.findCourseHistory(user.getId(), course.getId()));
        if(courseHistory.isPresent()) {
            courseHistory.get().setAction(action);
            courseEnrolmentDao.saveCourseEnrolmentHistory(courseHistory.get());
        } else {
            addNewHistory(user, course, action);
        }
    }

    private void addNewHistory(User user, Course course, CourseEnrolmentAction action) {
        CourseEnrolmentHistory courseEnrolmentHistory = new CourseEnrolmentHistory();
        courseEnrolmentHistory.setUser(user);
        courseEnrolmentHistory.setCourse(course);
        courseEnrolmentHistory.setAction(action);
        courseEnrolmentDao.saveCourseEnrolmentHistory(courseEnrolmentHistory);
    }

    @Override
    @Transactional
    public List<TypeOfCourse> getAllTypeOfCourse() {
        List<TypeOfCourse> typesOfCourse = typeOfCourseDao.getAllTypeOfCourse();
        typesOfCourse.forEach(type -> type.setDescription(Optional.ofNullable(type.getDescription()).orElse("")));
        return typesOfCourse;
    }

    @Override
    @Transactional
    public TypeOfCourse getTypeOfCourse(Long typeId) throws ApplicationException {
        Optional<TypeOfCourse> typesOfCourse = Optional.ofNullable(typeOfCourseDao.getTypeOfCourse(typeId));
        return typesOfCourse.orElseThrow(() -> new ApplicationException(ApplicationError.TYPE_OF_COURSE_DOES_NOT_EXIST));
    }

    @Override
    @Transactional
    public List<TypeOfCourse> getAllTypeOfCourse(Long courseId) {
        List<TypeOfCourse> typesOfCourse = typeOfCourseDao.getChosenTypeOfCourse(courseId);
        typesOfCourse.forEach(type -> type.setDescription(Optional.ofNullable(type.getDescription()).orElse("")));
        return typesOfCourse;
    }

    @Override
    @Transactional
    public Course getCourse(Long courseId) {
        return courseDao.getCourse(courseId);
    }

    private boolean checkCourseBetweenActualDate(Course course) {
        return DateUtils.betweenActualDate(course.getStartEnrolmentDate(), course.getEndEnrolmentDate());
    }

    @Override
    @Transactional
    public void createCourseWithClasses(Course course) {
        Course courseMerged = courseDao.updateCourse(course);
        course.getTeacherUsers().forEach(user -> {
            if(user.getAuthorities().stream().anyMatch(role -> AuthorityName.ROLE_TEACHER == role.getName())) {
                user.getTeacherCourses().add(courseMerged);
                userService.updateUser(user);
            }
        });
        course.getClasses().forEach(classes -> {
            classes.setCourse(courseMerged);
            Classes classesMerged = classesDao.updateClasses(classes);
            classes.getClassesDetails().forEach(details -> {
                details.setClasses(classesMerged);
                classesDao.updateClassesDetails(details);
            });
        });
    }

    @Override
    @Transactional
    public void updateCourseWithClasses(Course course) throws ApplicationException {
        Optional<Course> modifiedCourse = Optional.ofNullable(courseDao.getCourse(course.getId()));
        if(!modifiedCourse.isPresent()) {
            throw new ApplicationException(ApplicationError.COURSE_DOES_NOT_EXIST);
        }
        updateCourse(modifiedCourse.get(), course);
        List<Classes> classesToUpdate = course.getClasses().stream().filter(classes -> Objects.nonNull(classes.getId()))
                .collect(Collectors.toList());
        for(Classes classes : classesToUpdate) {
            updateClasses(classes);
        }
        createNewClasses(modifiedCourse.get(), course.getClasses());
    }

    private void updateCourse(Course existingCourse, Course course) throws ApplicationException {
        existingCourse.setName(course.getName());
        existingCourse.setDescription(course.getDescription());
        existingCourse.setStartEnrolmentDate(course.getStartEnrolmentDate());
        existingCourse.setEndEnrolmentDate(course.getEndEnrolmentDate());
        List<Long> classesId = course.getClasses().stream().filter(classes -> Objects.nonNull(classes.getId()))
                .map(Classes::getId).collect(Collectors.toList());
        existingCourse.getClasses().forEach(classes -> {
            if(!classesId.contains(classes.getId())) {
                classesService.removeClasses(classes);
            }
        });
        existingCourse.setClasses(existingCourse.getClasses().stream().filter(classes -> classesId.contains(classes.getId()))
                .collect(Collectors.toList()));
        courseDao.updateCourse(existingCourse);
    }

    private void updateClasses(Classes classes) throws ApplicationException {
        Optional<Classes> modifiedClasses = Optional.ofNullable(classesService.findClasses(classes.getId()));
        if(!modifiedClasses.isPresent()) {
            throw new ApplicationException(ApplicationError.CLASSES_DOES_NOT_EXIST);
        }
        modifiedClasses.get().setLecturer(classes.getLecturer());
        modifiedClasses.get().setDescription(classes.getDescription());
        if(checkMaxGroupNumber(classes, modifiedClasses.get())) {
            modifiedClasses.get().setUsers(modifiedClasses.get().getUsers().subList(0, classes.getMaxGroupNumber()));
        }
        modifiedClasses.get().setMaxGroupNumber(classes.getMaxGroupNumber());

        List<Long> detailsId = classes.getClassesDetails().stream().filter(details -> Objects.nonNull(details.getId()))
                .map(ClassesDetails::getId).collect(Collectors.toList());
        modifiedClasses.get().getClassesDetails().stream().filter(details -> !detailsId.contains(details.getId()))
                .forEach(classesService::removeClassesDetails);
        modifiedClasses.get().setClassesDetails(modifiedClasses.get().getClassesDetails().stream()
                .filter(details -> detailsId.contains(details.getId())).collect(Collectors.toList()));

        List<ClassesDetails> classesDetailsToUpdate = classes.getClassesDetails().stream()
                .filter(details -> Objects.nonNull(details.getId())).collect(Collectors.toList());
        for(ClassesDetails details : classesDetailsToUpdate) {
            updateClassesDetails(details);
        }
        classes.getClassesDetails().stream().filter(details -> Objects.isNull(details.getId())).forEach(details -> {
            details.setClasses(classes);
            classesService.modifyClassesDetails(details);
        });
        classesService.modifyClasses(modifiedClasses.get());
    }

    private boolean checkMaxGroupNumber(Classes newClasses, Classes existingClasses) {
        return newClasses.getMaxGroupNumber() < existingClasses.getMaxGroupNumber() &&
                existingClasses.getUsers().size() > newClasses.getMaxGroupNumber();
    }

    private void updateClassesDetails(ClassesDetails details) throws ApplicationException {
        Optional<ClassesDetails> modifiedDetails = Optional.ofNullable(classesService.findClassesDetails(details.getId()));
        if(!modifiedDetails.isPresent()) {
            throw new ApplicationException(ApplicationError.CLASSES_DETAILS_DOES_NOT_EXIST);
        }
        modifiedDetails.get().setDay(details.getDay());
        modifiedDetails.get().setHoursFrom(details.getHoursFrom());
        modifiedDetails.get().setHoursTo(details.getHoursTo());
        modifiedDetails.get().setBuilding(details.getBuilding());
        modifiedDetails.get().setRoomNumber(details.getRoomNumber());
        classesService.modifyClassesDetails(modifiedDetails.get());
    }

    private void createNewClasses(Course course, List<Classes> newClasses) {
        newClasses.forEach(classes -> {
            if(Objects.isNull(classes.getId())) {
                classes.setCourse(course);
                Classes classesMerged = classesDao.updateClasses(classes);
                classes.getClassesDetails().forEach(details -> {
                    details.setClasses(classesMerged);
                    classesDao.updateClassesDetails(details);
                });
            }
        });
    }

}
