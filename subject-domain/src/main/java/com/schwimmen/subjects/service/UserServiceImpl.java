package com.schwimmen.subjects.service;

import com.schwimmen.subjects.dao.AuthorityDao;
import com.schwimmen.subjects.dao.ApplicationUserDao;
import com.schwimmen.subjects.dao.UserDao;
import com.schwimmen.subjects.entity.security.ApplicationUser;
import com.schwimmen.subjects.entity.security.Authority;
import com.schwimmen.subjects.entity.security.AuthorityName;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final String BLANK = "";
    private final ApplicationUserDao applicationUserDao;
    private final UserDao userDao;
    private final AuthorityDao authorityDao;

    @Autowired
    public UserServiceImpl(ApplicationUserDao applicationUserDao, AuthorityDao authorityDao, UserDao userDao) {
        this.applicationUserDao = applicationUserDao;
        this.authorityDao = authorityDao;
        this.userDao = userDao;
    }

    /** Only for application users */
    @Override
    @Transactional
    public ApplicationUser findUserByUsername(String username) {
        return applicationUserDao.findUserByUsername(Optional.ofNullable(username).map(String::toLowerCase).orElse(BLANK));
    }

    @Override
    @Transactional
    public ApplicationUser findUserByEmail(String email) {
        return applicationUserDao.findUserByEmail(email);
    }

    @Override
    @Transactional
    public void unblockUser(String username, String unblockToken) {
        applicationUserDao.unblockUser(Optional.ofNullable(username).map(String::toLowerCase).orElse(BLANK), unblockToken);
    }

    /** For all types users - also ldap */
    @Override
    @Transactional
    public User findUserByUsername(String username, boolean isLdap) {
        return userDao.findUserByUsername(Optional.ofNullable(username).map(String::toLowerCase).orElse(BLANK), isLdap);
    }

    @Override
    @Transactional
    public List<User> findUserByRole(AuthorityName role) {
        return userDao.findUserByRole(role).stream().filter(user -> {
            if(user instanceof ApplicationUser) {
                return ((ApplicationUser) user).getEnabled();
            }
            return true;
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<User> findUserByRole(AuthorityName role, List<Long> excludeUserId) {
        return userDao.findUserByRole(role, excludeUserId).stream().filter(user -> {
            if(user instanceof ApplicationUser) {
                return ((ApplicationUser) user).getEnabled();
            }
            return true;
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<User> findUserByRoleAndId(AuthorityName role, List<Long> usersId) {
        return userDao.findUserByRoleAndId(role, usersId).stream().filter(user -> {
            if(user instanceof ApplicationUser) {
                return ((ApplicationUser) user).getEnabled();
            }
            return true;
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        applicationUserDao.saveUser(user);
    }

    @Override
    @Transactional
    public boolean checkIfUserIsSaved(String username, boolean isLdapUser) {
        return userDao.findUserByUsername(Optional.ofNullable(username).map(String::toLowerCase).orElse(BLANK)).stream()
                .anyMatch(user -> user.getLdap().equals(isLdapUser));
    }

    @Override
    @Transactional
    public User createUser(User user, AuthorityName authorityName) throws ApplicationException {
        Optional.ofNullable(authorityDao.getAuthority(authorityName))
                .ifPresent(authority -> user.setAuthorities(Arrays.asList(authority)));
        return this.createUser(user);
    }

    @Override
    @Transactional
    public User createUser(User user) throws ApplicationException {
        if((user instanceof ApplicationUser && user.getLdap()) || (!(user instanceof ApplicationUser) && !user.getLdap())) {
            throw new ApplicationException(ApplicationError.WRONG_TYPE_USER_CLASS);
        } else if(this.checkIfUserIsSaved(user.getUsername(), user.getLdap())) {
            throw new ApplicationException(ApplicationError.USER_WITH_USERNAME_EXISTS);
        } else if(Optional.ofNullable(this.findUserByEmail(user.getEmail())).isPresent()) {
            throw new ApplicationException(ApplicationError.USER_WITH_EMAIL_EXISTS);
        }
        return applicationUserDao.saveUser(user);
    }

}
