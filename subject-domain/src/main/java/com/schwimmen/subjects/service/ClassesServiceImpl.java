package com.schwimmen.subjects.service;

import com.schwimmen.subjects.dao.ClassesDao;
import com.schwimmen.subjects.dao.DayDao;
import com.schwimmen.subjects.entity.*;
import com.schwimmen.subjects.entity.security.User;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.service.interfaces.ClassesService;
import com.schwimmen.subjects.service.interfaces.UserService;
import com.schwimmen.subjects.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClassesServiceImpl implements ClassesService {

    private final ClassesDao classesDao;
    private final UserService userService;
    private final DayDao dayDao;

    @Autowired
    public ClassesServiceImpl(UserService userService, ClassesDao classesDao, DayDao dayDao) {
        this.userService = userService;
        this.classesDao = classesDao;
        this.dayDao = dayDao;
    }

    @Override
    @Transactional
    public void removeClasses(Classes classes) {
        classesDao.removeClasses(classes);
    }

    @Override
    @Transactional
    public Classes findClasses(Long classesId) {
        return classesDao.findClasses(classesId);
    }

    @Override
    @Transactional
    public void modifyClasses(Classes classes) {
        classesDao.updateClasses(classes);
    }

    @Override
    @Transactional
    public void removeClassesDetails(ClassesDetails classesDetails) {
        classesDao.removeClassesDetails(classesDetails);
    }

    @Override
    @Transactional
    public ClassesDetails findClassesDetails(Long classesDetailsId) {
        return classesDao.findClassesDetails(classesDetailsId);
    }

    @Override
    @Transactional
    public void modifyClassesDetails(ClassesDetails classesDetails) {
        classesDao.updateClassesDetails(classesDetails);
    }

    @Override
    @Transactional
    public void addStudentClasses(Long classesId, User user) throws ApplicationException {
        Optional<Classes> classesToAdd = Optional.ofNullable(classesDao.findClasses(classesId));
        if(!classesToAdd.isPresent() || !checkCourseBetweenActualDate(classesToAdd.get().getCourse())) {
            throw new ApplicationException(ApplicationError.COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE);
        } else if(classesToAdd.map(toAdd -> user.getSelectedStudentClasses().stream()
                .noneMatch(classes -> equalsClasses(toAdd, classes))).orElse(false)) {
            user.getSelectedStudentClasses().add(classesToAdd.get());
            userService.updateUser(user);
        }
    }

    private boolean equalsClasses(Classes classes1, Classes classes2) {
        return classes1.getCourse().getId().equals(classes2.getCourse().getId()) &&
                classes1.getTypeOfCourse().equals(classes2.getTypeOfCourse());
    }

    @Override
    @Transactional
    public void deleteStudentClasses(Long courseId, User user) {
        user.setSelectedStudentClasses(user.getSelectedStudentClasses().stream()
                .filter(classes -> !classes.getId().equals(courseId))
                .collect(Collectors.toList()));
        userService.updateUser(user);
    }

    @Override
    @Transactional
    public List<Classes> getClassesWithTypes(Long courseId, Long typeClassId) {
       return  classesDao.getClassesWithType(courseId, typeClassId);
    }

    private boolean checkCourseBetweenActualDate(Course course) {
        return DateUtils.betweenActualDate(course.getStartEnrolmentDate(), course.getEndEnrolmentDate());
    }

    @Override
    @Transactional
    public Day getDayByName(Days day) {
        return dayDao.getDay(day);
    }

}
