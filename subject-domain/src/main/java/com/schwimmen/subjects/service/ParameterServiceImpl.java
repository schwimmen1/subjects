package com.schwimmen.subjects.service;

import com.schwimmen.subjects.Parameters;
import com.schwimmen.subjects.dao.ParameterDao;
import com.schwimmen.subjects.entity.Parameter;
import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;
import com.schwimmen.subjects.service.interfaces.ParameterService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ParameterServiceImpl implements ParameterService {

    private final ParameterDao parameterDao;
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Autowired
    public ParameterServiceImpl(ParameterDao parameterDao) {
        this.parameterDao = parameterDao;
    }

    @Override
    @Transactional
    public List<Parameter> getAllParameters() {
        return parameterDao.getAllParameters();
    }

    @Override
    @Transactional
    public String getParameterValue(Parameters parameter) {
        return parameterDao.getParameterValue(parameter);
    }

    @Override
    @Transactional
    public void saveParameter(String parameterName, String value) throws ApplicationException {
        Optional<Parameter> parameter =
                Optional.ofNullable(parameterDao.getParameter(Parameters.getParameterByName(parameterName)));
        if(!parameter.isPresent()) {
            throw new ApplicationException(ApplicationError.PARAMETER_NAME_THIS_NAME_DOES_NOT_EXIST);
        } else if(Parameters.BEGIN_ENROLMENT_DAY.name().equalsIgnoreCase(parameterName)){
            Date beginDate = this.castToDate(value);
            Date endDate = this.getDate(Parameters.END_ENROLMENT_DAY);
            if(beginDate.after(endDate)) {
                throw new ApplicationException(ApplicationError.START_ENROLMENT_DATE_IS_AFTER_END_DATE);
            }
        } else if(Parameters.END_ENROLMENT_DAY.name().equalsIgnoreCase(parameterName)) {
            Date endDate = this.castToDate(value);
            Date beginDate = this.getDate(Parameters.BEGIN_ENROLMENT_DAY);
            if(endDate.before(beginDate)) {
                throw new ApplicationException(ApplicationError.END_ENROLMENT_DATE_IS_BEGORE_START_DATE);
            }
        }
        parameter.get().setValue(value);
        parameterDao.saveParameter(parameter.get());
    }

    private final Log logger = LogFactory.getLog(this.getClass());

    @Override
    @Transactional
    public Date getDate(Parameters parameter) {
        String parameterValue = this.getParameterValue(parameter);
        logger.info("Getting string date: " + parameterValue + ", length: " + parameterValue.length());
        Date value = null;
        try {
            if (Date.class.equals(parameter.getValueClass())) {
                value = dateFormatter.parse(parameterValue);
            }
        } catch(ParseException | NumberFormatException e) {
            logger.fatal("First exception - fatal with parse Date: " + parameterValue + ", length: " + parameterValue.length());
        }
        return value;
    }

    @Override
    @Transactional
    public Object castValueToObject(Parameter parameter) throws ApplicationException {
        Parameters parameterEnum = Parameters.getParameterByName(parameter.getName());
        if(Date.class.equals(parameterEnum.getValueClass())) {
            return this.castToDate(parameter.getValue());
        }
        return parameter.getValue();
    }

    private Date castToDate(String stringToDate) throws ApplicationException{
        try {
            return dateFormatter.parse(stringToDate);
        } catch(ParseException e) {
            throw new ApplicationException(ApplicationError.WRONG_PATTERN_DATE);
        }
    }

}
