package com.schwimmen.subjects.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class DateUtils {

    public static final String PATTERN_HOUR = "HH:mm";
    public static final String PATTERN_DATE = "yyyy-MM-dd'T'HH:mm";

    public static Date actualDate() {
        return new Date();
    }

    public static boolean betweenActualDate(Date startDate, Date endDate) {
        Date actualDate = DateUtils.actualDate();
        return Optional.ofNullable(startDate).map(actualDate::after).orElse(false) &&
                Optional.ofNullable(endDate).map(actualDate::before).orElse(false);
    }

    public static Date parseDate(String pattern, String date) throws ParseException {
        SimpleDateFormat parser = new SimpleDateFormat(pattern);
        return parser.parse(date);
    }

    public static String parseToString(String pattern, Date date) throws ParseException {
        SimpleDateFormat parser = new SimpleDateFormat(pattern);
        return parser.format(date);
    }

}
