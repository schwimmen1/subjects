package com.schwimmen.subjects.exception;

public enum ApplicationError {

    COURSE_DOES_NOT_EXIST("Course with this id does not exist."),
    CLASSES_DOES_NOT_EXIST("Classes with this id does not exist"),
    CLASSES_DETAILS_DOES_NOT_EXIST("Classes details with this id does not exist"),
    TYPE_OF_COURSE_DOES_NOT_EXIST("Type of course with this id does not exist"),
    COURSE_DATE_NOT_BETWEEN_ACTUAL_DATE("Course enrolment dates are not between actual date."),

    USER_WITH_EMAIL_EXISTS("User with this email already exists."),
    USER_WITH_USERNAME_EXISTS("User with this username already exists"),
    AUTHORITY_WITH_THIS_CODE_DOES_NOT_EXIST("Authority with this code does not exist."),
    DAY_WITH_THIS_CODE_DOES_NOT_EXIST("Day with this code does not exist."),
    WRONG_TYPE_USER_CLASS("Ldap user have to be user type and application user have to be application user type"),
    ACTUAL_DATE_BEFORE_START_ENROLMENT("Actual date is before start enrolment date. This operation is impossible."),
    ACTUAL_DATE_AFTER_END_ENROLMENT("Actual date is after end enrolment date. This operation is impossible."),
    ACTUAL_DATE_AFTER_START_ENROLMENT("Actual date is after start enrolment date. This operation is impossible."),
    ACTUAL_DATE_BEFORE_END_ENROLMENT("Actual date is before end enrolment date. This operation is impossible."),
    PARAMETER_NAME_THIS_NAME_DOES_NOT_EXIST("Parameter with this name does not exist."),
    START_ENROLMENT_DATE_IS_AFTER_END_DATE("Start enrolment date cannot be after end enrolment date"),
    END_ENROLMENT_DATE_IS_BEGORE_START_DATE("End enrolment date cannot be before start enrolment date"),
    WRONG_PATTERN_DATE("Wrong pattern date. Mandatory pattern: dd/MM/yyyy HH:mm:ss");

    private String cause;

    ApplicationError(String cause) {
        this.cause = cause;
    }

    public String getCause() {
        return this.cause;
    }

}
