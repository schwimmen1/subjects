package com.schwimmen.subjects.exception;

public class ApplicationException extends Exception {

    private ApplicationError applicationError;

    public ApplicationException(ApplicationError applicationError) {
        super(applicationError.getCause());
        this.applicationError = applicationError;
    }

    public ApplicationException(ApplicationError applicationError, Throwable cause) {
        super(applicationError.getCause(), cause);
        this.applicationError = applicationError;
    }

    public ApplicationError getApplicationError() {
        return applicationError;
    }

}
