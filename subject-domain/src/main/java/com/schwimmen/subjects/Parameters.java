package com.schwimmen.subjects;

import com.schwimmen.subjects.exception.ApplicationError;
import com.schwimmen.subjects.exception.ApplicationException;

import java.util.Arrays;
import java.util.Date;

public enum Parameters {

    BEGIN_ENROLMENT_DAY(Date.class, "Begin enrolment day."),
    END_ENROLMENT_DAY(Date.class, "End enrolment day."),
    ONLY_LDAP_LOGIN(Integer.class, "Only LDAP."),
    REGISTRATION_AVAILABLE(Integer.class, "Registration available.");

    private Class<?> type;
    private String description;

    Parameters(Class<?> type, String description) {
        this.type = type;
        this.description = description;
    }

    public Class<?> getValueClass() {
        return this.type;
    }

    public String getDescription() {
        return this.description;
    }

    public static Parameters getParameterByName(String name) throws ApplicationException {
        return Arrays.stream(Parameters.values()).filter(value -> value.name().equalsIgnoreCase(name)).findAny()
                .orElseThrow(() -> new ApplicationException(ApplicationError.PARAMETER_NAME_THIS_NAME_DOES_NOT_EXIST));
    }

}
