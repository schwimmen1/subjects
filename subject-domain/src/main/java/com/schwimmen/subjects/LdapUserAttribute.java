package com.
        schwimmen.subjects;

public enum LdapUserAttribute {

    LDAP_LOGIN("ldapLogin"),
    NAME("name"),
    SURNAME("surname"),
    EMAIL("email"),
    ROLE("role");

    private String name;

    LdapUserAttribute(String name) {
        this.name = name;
    }

    public String value() {
        return this.name;
    }

}
